<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\Filters\BuilderFilter;
use App\Http\Controllers\API\V1\Logs\ActivityLogController;
use App\Http\Controllers\API\V1\Controller;
use App\Http\Controllers\API\V1\ImageController;
use Modules\Auth\Entities\User;
use Modules\Catalog\Entities\Item;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


//TODO - Must add A Throttle on this route
Route::get('/download', [Controller::class, 'downloadFile'])->withoutMiddleware(['set-lang', 'current-page']);


Route::group(['middleware' => ['auth:sanctum']], function () {

    Route::get('items/cache-all', function (Request $request) {
        if (!(User::find(auth()->user()->user_id)->roles->contains('name', 'admin'))) {
            abort(400, 'Only admin');
        }
        $skip = $request->skip ?? 0;
        $take = $request->take ?? 10;
        $items = QueryBuilder::for(Item::class)
            ->allowedFilters([AllowedFilter::custom('product_id', new BuilderFilter())])
            ->skip($skip)
            ->take($take)
            ->get();

        $filtered_not_cached_items = $items->filter(function ($item) {
            $cached_item = Cache::get('download-items-' . $item->id);
            return !isset($cached_item);
        });

        $filtered_not_cached_items_ids = $filtered_not_cached_items->pluck('id')->toArray();

        $items_to_cache = Item::whereIn('id', $filtered_not_cached_items_ids)->get();

        $items_to_cache->each(function ($item) {
            cacheItem($item);
        });

        return response()->json([
            "message" => count($items_to_cache) . " items cached successfully"
        ]);
    });
    //(Admins)
    Route::apiResource('logs/activity-logs', ActivityLogController::class)->only(['index', 'show']);

    Route::group(['middleware' => ['sessions']], function () {
        //upload multiple images
        Route::post('upload-images', [ImageController::class, 'uploadImages']);
        //delete image from the opened session
        Route::delete('delete-image', [ImageController::class, 'deleteImage']);
        //upload one image to a session (Doctors - Admins - User)
        Route::post('upload-image', [ImageController::class, 'uploadImage']);
    });
});
