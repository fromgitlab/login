<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Pharmaway</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            line-height: 1.6;
            color: #333;
            margin: 0;
            padding: 20px;
        }

        .container {
            max-width: 600px;
            margin: 0 auto;
            background-color: #f9f9f9;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }

        h1 {
            color: #333;
            margin-bottom: 10px;
        }

        .message {
            margin-bottom: 20px;
        }

        .signature {
            font-style: italic;
        }
    </style>
</head>

<body>
    <div class="container">
        <h1>Pharmaway</h1>
        <div>Dear {{ $data['name'] }},</div>
        <div>You sent a request to join us. Here are the details:</div>

        <div class="message">
            {{ $data['title'] }}<br>
            {{ $data['message'] }}
        </div>

        <div>Thank you for your interest.</div>

        <div class="signature">Sincerely,<br>Your Team at Pharmaway</div>
    </div>
</body>

</html>
