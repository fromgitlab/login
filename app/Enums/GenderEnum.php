<?php

namespace App\Enums;

enum GenderEnum: string
{
    case MALE = 'male';
    case FEMALE = 'female';
    case OTHER = 'other';


    public static function getTranslatedValues(): array
    {
        $values = [];
        foreach (self::cases() as $case) {
            $values[] =  __('gender.' . $case->value);
        }
        return $values;
    }
}
