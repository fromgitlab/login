<?php

namespace App\Enums;

enum ModuleName: string
{
    case Category = "category";
    case Catalog = "catalog";
    case Auth = "auth";
    case Announcement = "announcement";
    case Interaction = "interaction";
    case Loyalty = "loyalty";
    case Notification = "notification";
    case Operation = "operation";
    case Profile = "profile";
    case Setting = "setting";
    case Website = "website";
}
