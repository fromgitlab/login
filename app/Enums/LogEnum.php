<?php

namespace App\Enums;

enum LogEnum: string
{
    case EDIT = "EDIT";
    case ADD = "ADD";
    case DELETE = "DELETE";
    case ACTIVITY = "ACTIVITY";
    case GET = "GET";
}
