<?php

namespace App\Enums;

enum StoreInSessionKey :string{
    case IMAGE="image";
    case IMAGES="images";
    case FILE="file";
    case FILES="files";
}
