<?php

namespace App\Policies;

use Modules\Auth\Entities\User;

class ActivityLogPolicy 
{



    /**
     * Determine whether the user can view the model.
     *
     * @param  User  $user
     * @return mixed
     */
    public static function browse($user)
    {
        $allowed = $user?->roles->contains('name', 'admin');
        return $allowed;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  User  $user
     * @return mixed
     */
    public static function read($user)
    {
        $allowed = $user?->roles->contains('name', 'admin');
        return $allowed;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  User  $user
     * @return mixed
     */
    public static function add($user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  User $user
     * @return mixed
     */
    public static function edit($user)
    {
        return true;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  User  $user
     * @return mixed
     */
    public static function delete($user)
    {
        return true;
    }
}
