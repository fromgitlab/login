<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

/**
 * This scope will be used for remove suspended fields
 **/
class SuspensionScope implements Scope
{
     /**
     * Apply the scope to a given Eloquent query builder.
     */
    public function apply(Builder $builder, Model $model): void
    {
        $user = auth()->user();
        switch ($user) {
            case $user?->roles?->contains('name', 'admin') || $user?->roles?->contains('name', 'brand_operator'):
                break;
            default:
                if (method_exists($model, 'scopeOrWhereHasOwner') && $user) {
                    $builder->suspended(false)->orWhereHasOwner($user);
                } else {
                    $builder->suspended(false);
                }
                break;
        }
    }
}
