<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class FreeSampleScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     */
    public function apply(Builder $builder, Model $model): void
    {
        $user = auth()->user();
        switch ($user) {
            case auth()->user()?->roles->contains('name', 'admin'):
                break;
            case  auth()->user()?->roles->contains('name', 'brand_operator'):
                // //FIXME - name of the role must be from RoleEnum
                $builder->whereHas('item.product.brand', function ($query) {
                    return $query->where('id', authProfile()->brand?->id);
                });
                break;
            default:
                $builder->whereHas('profiles', function ($query) {
                    $query->whereIn('profile_id', auth()->user()->profiles->pluck('id'));
                });
                break;
        }
    }
}
