<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Closure;
use App\Enums\LogEnum;
use App\Interfaces\V1\ActivityLogRepositoryInterface;
use Illuminate\Support\Facades\Route;

class RetrieveLogs
{
    public function __construct(private ActivityLogRepositoryInterface  $activityLogRepositoryInterface)
    {
        //
    }

    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);
        if (request()->method() == "GET" && Route::current()->uri() != "api/v1/logs/activity-logs") {
            $this->activityLogRepositoryInterface->storeGet(
                LogEnum::GET,
                isset($response->original['data'])
                    ? $response->original['data'] :
                     $response->original
            );
        }
        return $response;
    }
}
