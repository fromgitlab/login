<?php

namespace App\Http\Resources\Logs\ActivityLogs;

use Illuminate\Http\Resources\Json\JsonResource;

class LogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray($request)
    {
        return  [
            'id' => $this->id,
            'event' => $this->event->template,
            'host' => $this->host,
            'ip' => $this->ip,
            'request' => $this->request,
            'current_page' => $this->current_page,
            'user_agent' => $this->user_agent,
            'created_at' => $this->created_at,
            "causer" => [
                'name' => $this->causer->name,
                'phoneNumber' => $this->causer->phone_number,
                'email' => $this->causer->email,
            ],
            "activity_log" =>new ActivityLogResource($this->activityLogs),

        ];
    }
}
