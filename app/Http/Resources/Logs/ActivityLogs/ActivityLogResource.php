<?php

namespace App\Http\Resources\Logs\ActivityLogs;

use Illuminate\Http\Resources\Json\JsonResource;

class ActivityLogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray($request)
    {
        return  [
            'id' => $this->id,
            "description" => $this->description,
            "subject_name" => class_basename($this->subject),
            "properties" => $this->when((request()->activity_log), json_decode($this->properties)),
            "created_at" => $this->created_at,
            "batch_uuid" => $this->batch_uuid,
            'log' => [
                'id' => $this->log->id,
                'event' => $this->log->event->template,
                'host' => $this->log->host,
                'ip' => $this->log->ip,
                'request' => $this->log->request,
                'current_page' => $this->log->current_page,
                'user_agent' => $this->log->user_agent,
                'created_at' => $this->log->created_at,
                "causer" => [
                    'name' => $this->log->causer->name,
                    'phoneNumber' => $this->log->causer->phone_number,
                    'email' => $this->log->causer->email,
                ],
            ]
        ];
    }
}
