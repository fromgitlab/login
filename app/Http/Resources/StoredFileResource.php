<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StoredFileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray($request)
    {
        return  [
            'url' => $this->url,
            'path' => $this->path,
            'name' => $this->name,
            'folder_path' => ($this->folder_path) ? $this->folder_path : null,
            'title' => ($this->title) ? $this->title : null,
        ];
    }
}
