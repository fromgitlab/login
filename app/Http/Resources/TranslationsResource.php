<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TranslationsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray($request)
    {
        return $this->resource->groupBy('locale')->map(function ($items) {
            return collect($items->first())->only([
                'text',
                'reason',
                'address',
                'description',
                'package_description',
            ]);
        });
    }
}
