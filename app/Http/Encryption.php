<?php

namespace App\Http;

use Error;

/**
 * Encryption class for encrypting and decrypting data.
 */
class Encryption
{
    /**
     * The encryption method to use.
     *
     * @var string
     */
    private string $encryptMethod = 'AES-256-CBC';

    /**
     * The encryption key.
     *
     * @var string
     */
    private string $key;

    /**
     * The encryption initialization vector (IV).
     *
     * @var string
     */
    private string $iv;


    /**
     * Create a new encryption instance.
     *
     * This constructor sets up the encryption key and IV.
     */
    public function __construct()
    {
        $mykey = 'ThisIspharmawaKey';
        $myiv = 'ThisIspharmawaBlock';
        $this->key = substr(hash('sha256', $mykey), 0, 32);
        $this->iv = substr(hash('sha256', $myiv), 0, 16);
    }

    /**
     * Encrypt a value.
     *
     * @param string $value The value to encrypt.
     *
     * @return string The encrypted value.
     */
    public function encrypt(string $value): string
    {
        return openssl_encrypt(
            $value,
            $this->encryptMethod,
            $this->key,
            0,
            $this->iv
        );
    }

    /**
     * Decrypt a value.
     *
     * @param string $base64Value The base64-encoded value to decrypt.
     *
     * @return string The decrypted value.
     *
     * @throws Error If the decryption fails.
     */
    public function decrypt(string $base64Value): string
    {
        try {

            return openssl_decrypt(
                $base64Value,
                $this->encryptMethod,
                $this->key,
                0,
                $this->iv
            );
        } catch (Error $e) {
            throw $e;
        }
    }
}
