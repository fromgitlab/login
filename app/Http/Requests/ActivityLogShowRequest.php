<?php

namespace App\Http\Requests;

use App\Policies\ActivityLogPolicy;
use App\Http\Requests\BaseRequest;
use App\Enums\ModuleName;

class ActivityLogShowRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return ActivityLogPolicy::read(auth()->user());
    }
}
