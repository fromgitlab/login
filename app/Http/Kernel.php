<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{

    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array<int, class-string|string>
     */
    protected $middleware = [
        // \App\Http\Middleware\TrustHosts::class,
        \App\Http\Middleware\TrustProxies::class,
        \Illuminate\Http\Middleware\HandleCors::class,
        \App\Http\Middleware\PreventRequestsDuringMaintenance::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
        // \Modules\Auth\Http\Middleware\AuthenticateProfileTokens::class,
        // \Modules\Profile\Http\Middleware\ProfilePendingSuspensionMiddleware::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array<string, array<int, class-string|string>>
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],
        
        'api' => [
            
            // \Laravel\Sanctum\Http\Middleware\EnsureFrontendRequestsAreStateful::class,
            \App\Http\Middleware\AcceptJson::class,
            \App\Http\Middleware\SetUlid::class,
            // \App\Http\Middleware\RetrieveLogs::class,
            \App\Http\Middleware\SetLangApp::class,
            \App\Http\Middleware\CurrentPage::class,
            \App\Http\Middleware\Pagination::class,
            'throttle:api',
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
            
        ],
        'tools' => [
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
            \App\Http\Middleware\Pagination::class,

        ],
        'sessions' => [
            \App\Http\Middleware\AddSession::class,
            \Illuminate\Session\Middleware\StartSession::class,
        ]

    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array<string, class-string|string>
     */
    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'auth.session' => \Illuminate\Session\Middleware\AuthenticateSession::class,
        'cache.headers' => \Illuminate\Http\Middleware\SetCacheHeaders::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'password.confirm' => \Illuminate\Auth\Middleware\RequirePassword::class,
        'signed' => \App\Http\Middleware\ValidateSignature::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'verified' => \Illuminate\Auth\Middleware\EnsureEmailIsVerified::class,
        'add-session' => \App\Http\Middleware\AddSession::class,
        'authenticate-profile-tokens' => \Modules\Auth\Http\Middleware\AuthenticateProfileTokens::class,
        'profile-pennding-suspension' => \Modules\Profile\Http\Middleware\ProfilePendingSuspensionMiddleware::class,
        'verify-phone-number' => \App\Http\Middleware\VerifyPhoneNumber::class,
        'access-limit' => \App\Http\Middleware\AccessLimit::class,
        'get-logs' => \App\Http\Middleware\RetrieveLogs::class,
        'set-lang'=>\App\Http\Middleware\SetLangApp::class,
        'current-page'=>\App\Http\Middleware\CurrentPage::class,
    ];

    /**
     * The priority-sorted list of middleware.
     *
     * This forces non-global middleware to always be in the given order.
     *
     * @var string[]
     */
    protected $middlewarePriority = [
        \Modules\Auth\Http\Middleware\AuthenticateProfileTokens::class,
        \Modules\Profile\Http\Middleware\ProfilePendingSuspensionMiddleware::class,
        \App\Http\Middleware\AddSession::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \App\Http\Middleware\VerifyPhoneNumber::class,
    ];
}
