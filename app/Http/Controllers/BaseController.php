<?php

namespace App\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

/**
 * Base controller for other controllers to extend from.
 */
class BaseController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * This constructor sets up middleware for user authentication and profile suspension checks.
     */
    public function __construct()
    { 
        if(Auth::guard('sanctum')->check()){
            $this->middleware('authenticate-profile-tokens')->except(Config::get('sanctum.except'));
            $this->middleware('auth:sanctum');
            $this->middleware('profile-pennding-suspension');
        }
        $this->middleware('get-logs');
    }

}
