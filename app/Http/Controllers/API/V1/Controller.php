<?php

namespace App\Http\Controllers\API\V1;


use App\Http\Controllers\BaseController;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Download a file from storage.
     *
     * @param Request $request The request object.
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse The file download response.
     */
    public function downloadFile(Request $request)
    {
        $request->validate([
            'url' => ['required', 'string', 'url', Rule::in([str_contains($request->url, '/storage') ? $request->url : null])],
        ]);
        $url = parse_url($request->url);
        $path = $url['path'];
        $path = str_replace('/storage', 'public', $path);

        return response()->download(storage_path('app/' . $path));
    }


  
}
