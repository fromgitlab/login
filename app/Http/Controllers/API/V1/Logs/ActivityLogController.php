<?php

namespace App\Http\Controllers\API\V1\Logs;

use App\Http\Requests\ActivityLogIndexRequest;
use App\Models\ActivityLog;
use App\Interfaces\V1\ActivityLogRepositoryInterface;
use App\Http\Controllers\BaseController;
use App\Http\Requests\ActivityLogShowRequest;

class ActivityLogController extends BaseController
{
    public function __construct(private ActivityLogRepositoryInterface  $activityLogRepositoryInterface)
    {
        parent::__construct();
    }
    
    /**
     * Display a listing of the logs resource.
     *
     * @param ActivityLogIndexRequest $request The request object.
     *
     * @return  The JSON response.
     */
    public function index(ActivityLogIndexRequest $request)
    {
        return apiResponse(
            'All Activity logs',
            transferDataWithPagination($this->activityLogRepositoryInterface->index($request)),
            200
        );
    }

    /**
     * Display the specified resource.
     *
     * @param ActivityLogShowRequest $request The request object.
     * @param Activity $activity The activity log to display.
     *
     * @return  The JSON response.
     */
    public function show(ActivityLogShowRequest $request, ActivityLog $activityLog)
    {
        return apiResponse(
            'Activity log',
            $this->activityLogRepositoryInterface->show($activityLog),
            200,
        );
    }

}
