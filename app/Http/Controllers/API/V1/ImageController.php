<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Actions\ImageDeleteAction;
use App\Helpers\Util;
use App\Http\Controllers\BaseController;
use Modules\Profile\Http\Requests\ImageDeleteRequest;
use Modules\Profile\Http\Requests\ImageStoreRequest;

class ImageController extends BaseController
{

    /**
     * upload an image to the current session
     * 
     * @param ImageStoreRequest $request
     * 
     * @return 
     */
    public function uploadImage(ImageStoreRequest $request)
    {
        session()->forget('image');
        $originalName = $request->file('image')->getClientOriginalName();
        $fileName = replaceSpacesWithUnderscores($originalName);
        $image = Util::imageStore('temp', $request->file('image'), $fileName);
        session()->put('image', $image);
        return apiResponse(
            __("image.uploadImage"),
            ['session_id' => session()->getId()],
            201,
            null,
        );
    }

    /**
     * Upload multiple images to the current session
     * 
     * @param ImageStoreRequest $request
     * 
     * @return 
     */
    public function uploadImages(ImageStoreRequest $request)
    {
        $originalName = $request->file('image')->getClientOriginalName();
        $fileName = replaceSpacesWithUnderscores($originalName);
        $image = Util::imageStore('temp', $request->file('image'), $fileName);
        session()->push('images', $image);
        return apiResponse(
            __("image.uploadImages"),
            [
                'path' => $image['path'],
                'session_id' => session()->getId()
            ],
            201,
            null,
        );
    }

    /**
     * Delete an image from the current session by giving the path of the temp disk
     * 
     * @param Request $request
     * 
     * @return 
     */
    public function deleteImage(ImageDeleteRequest $request)
    {
        $path = $request->path;

        ImageDeleteAction::execute('', $path, 'temp');
        $images = session()->get('images');
        session()->forget('images');
        $images = array_filter($images, function ($image) use ($path) {
            return $image['path'] != $path;
        });
        session()->put('images', $images);
        return apiResponse(
            __("image.deleteImage"),
            ['session_id' => session()->getId()],
            200,
            null,

        );
    }
}
