<?php

namespace App\Actions;

use Carbon\Carbon;

/**
 * Responsible for converting a date from the dd-mm-yyyy format to the yyyy-mm-dd format.
 */
class DMYToYMD
{
    /**
     *  Converts a date from the dd-mm-yyyy format to the yyyy-mm-dd format.
     *
     * @param string $date The date to be converted in the dd-mm-yyyy format.
     *
     * @return string The converted date in the yyyy-mm-dd format.
     */
    public static function execute($date)
    {      
        try {
            $date = Carbon::createFromTimestamp( $date)->format('Y-m-d H:i:s');
      
        } catch (\Exception $exception) {
            //do nothing
       
        }

        return $date;
    }
}
