<?php

namespace App\Actions;

use App\Models\UploadedVideo;

/**
 * Creates a new video and stores it in the database based on the provided data.
 */
class VideoStoreAction
{
    /**
     * Creates a new video and stores it in the database based on the provided data.
     *
     * @param array $data The data to be used to create the video.
     *
     * @return UploadedVideo The newly created video.
     */
    public static function execute(array $data)
    {
        $video = UploadedVideo::create($data);
        return $video;
    }
}
