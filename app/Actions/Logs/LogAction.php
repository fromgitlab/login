<?php

namespace App\Actions\Logs;

use App\Models\LogEvent;
use App\Models\Log;
use App\Enums\LogEnum;
use Illuminate\Support\Facades\DB;
use Modules\Auth\Helpers\AuthHelper;
use Illuminate\Database\Eloquent\Model;

/**
 * This action class used to create a log in the database based on the request function 
 */
class LogAction
{
    /**
     * Creates an log in the database based on the given parameters.
     *
     * @param LogEnum $action The log action to be performed.
     * @param Model $model The model that the log action is being performed on.
     * @return Log
     * @throws \InvalidArgumentException If an invalid `LogEnum` value is provided.
     */
    public static function execute(LogEnum $action):Log
    {
        $log = new Log();
        $event =  LogEvent::where('template', $action->name)->first();

        $log->event_id = $event->id;
        $log->host = request()->getHttpHost();
        $log->ip = request()->ip();
        $log->request = request()->url();
        $log->current_page = request()->header('current-page') ? request()->header('current-page') : 'Internal System Page';
        $log['User_Agent'] = request()->header('User-Agent');

        if (auth()->user()) {
            $log->causer()->associate(auth()->user());
        } else {
            $user = AuthHelper::getUserOrCreate('0900000000', 'anonymous_user@miroworld.com','anonymous user'); 
            $log->causer_id = $user->id;
            $log->causer_type = "Modules\Auth\Entities\User";
        }

        $log->save();

        return $log;
    }

}
