<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class GeneratePolicies extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:policies {--ignore-models-ends-with= : Comma-separated list of model suffixes to ignore}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will generate policies for an HMVC structure';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $ignoreSuffixes = explode(',', $this->option('ignore-models-ends-with'));

        $path = base_path('Modules');
        if (!is_dir($path)) {
            $this->error('Modules directory not found!');
            return;
        }

        $directories = array_diff(scandir($path), ['..', '.']);
        foreach ($directories as $directory) {
            $modelsPath = $path . '/' . $directory . '/Entities';
            if (!File::isDirectory($modelsPath)) {
                continue;
            }
            $models = array_filter(array_diff(scandir($modelsPath), ['..', '.']), function ($model) use ($ignoreSuffixes) {
                if ($model === '.gitkeep') {
                    return false;
                }

                foreach ($ignoreSuffixes as $suffix) {
                    if (!Str::endsWith($model, $suffix . '.php')) {
                        return false;
                    }
                }

                return true;
            });
            foreach ($models as $model) {
                $modelName = str_replace('.php', '', $model);
                $policyName = $modelName . 'Policy';
                $policyPath = base_path('Modules/' . $directory . '/' . 'Policies/' . $policyName . '.php');
                if (File::exists($policyPath)) {
                    $this->warn("Policy already exists for {$modelName} model.");
                    continue;
                }
                $policyContent = "<?php\n\nnamespace App\Modules\\{$directory}\Policies;\n\nuse App\Modules\\{$directory}\Entities\\{$modelName};\nuse Illuminate\Auth\Access\HandlesAuthorization;\n\nclass {$policyName}\n{\n use HandlesAuthorization;\n\n public function browse(User \$user)\n {\n return \$user->hasPermission('{$directory}.{$modelName}.browse');\n }\n\n public function read(User \$user, {$modelName} \${$modelName})\n {\n return \$user->hasPermission('{$directory}.{$modelName}.read');\n }\n\n public function add(User \$user)\n {\n return \$user->hasPermission('{$directory}.{$modelName}.add');\n }\n\n public function edit(User \$user, {$modelName} \${$modelName})\n {\n return \$user->hasPermission('{$directory}.{$modelName}.edit');\n }\n\n public function delete(User \$user, {$modelName} \${$modelName})\n {\n return \$user->hasPermission('{$directory}.{$modelName}.delete');\n }\n}";

                if (!File::isDirectory(dirname($policyPath))) {
                    File::makeDirectory(dirname($policyPath), 0755, true);
                }
                File::put($policyPath, $policyContent);
                $this->info("Policy generated for {$modelName} model.");
            }
        }
        $this->info('All policies generated successfully!');
    }
}
