<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Log extends BaseModel
{
    use HasFactory;

        /**
     * Get the  causer model.
     */
    public function causer(): MorphTo
    {
        return $this->morphTo();
    }


        /**
     * Get the activityLogs associated with the log.
     */
    public function activityLogs(): HasOne
    {
        return $this->hasOne(ActivityLog::class);
    }

            /**
     * Get the activityLogs associated with the user.
     */
    public function event(): BelongsTo
    {
        return $this->belongsTo(LogEvent::class);
    }

}
