<?php

namespace App\Models;

use App\Models\BaseModel;

class UploadedVideo  extends BaseModel
{
    protected $fillable = [
        'type',
        'local_path',
        'url'
    ];
}
