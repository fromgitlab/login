<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class ActivityLog extends BaseModel
{
    use HasFactory;

    protected $fillable = [
        'log_id',
        'properties',
        'description',
        'subject_id',
        'subject_type',
        'batch_uuid',
    ];

    /**
     * 
     */
    public function subject(): MorphTo
    {
        return $this->morphTo();
    }

    /**
     * Get the phone associated with the user.
     */
    public function log(): BelongsTo
    {
        return $this->belongsTo(Log::class);
    }

    public function scopeSearch($query, $search = null)
    {
        if (isset($search)) {

            return $query
                ->where('description', 'like', "%{$search}%")
                ->orWhere('created_at', 'like', "%{$search}%")
                ->orWhere('properties', 'like', "%{$search}%")

                ->orWhereHas('log', function ($query) use ($search) {
                    $query->where('logs.host', 'like', "%{$search}%")
                        ->orWhere('logs.ip', 'like', "%{$search}%")
                        ->orWhere('logs.request', 'like', "%{$search}%")
                        ->orWhere('logs.current_page', 'like', "%{$search}%")
                        ->orWhere('logs.user_agent', 'like', "%{$search}%")

                        ->orWhereHas('event', function ($query) use ($search) {
                            $query->where('log_events.template', 'like', "%{$search}%");
                        })

                        ->orWhereHas('causer', function ($query) use ($search) {
                            $query->where('users.name', 'like', "%{$search}%")
                                ->where('users.phone_number', 'like', "%{$search}%")
                                ->where('users.email', 'like', "%{$search}%");
                        });
                });
        }
        return $query;
    }
}
