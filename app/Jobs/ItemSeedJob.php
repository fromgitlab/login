<?php

namespace App\Jobs;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Spatie\QueryBuilder\AllowedFilter;
use Illuminate\Queue\SerializesModels;
use Spatie\QueryBuilder\QueryBuilder;
use Modules\Catalog\Entities\Item;

use Modules\Catalog\DTO\ItemData;

use Illuminate\Bus\Queueable;

use App\Imports\ProductImport;
use Cache;
use DB;
use Excel;
use Illuminate\Database\Eloquent\Model;
use Storage;

/**
 * This class is used to export the system items to a json file for the mobile app to have a data source built into the app
 * 
 */
class ItemSeedJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 9999999;

    /**
     * re-import the item data and cache them.
     *
     * @return void
     */
    public function handle()
    {
        Model::unguard();
        Excel::import(new ProductImport,  Storage::disk('local')->path('excel/importer_data.csv'));


        $items = QueryBuilder::for(Item::class)
            ->allowedFilters([AllowedFilter::scope('updated_after')])
            ->orderBy(DB::raw('ROW_NUMBER() OVER (PARTITION BY product_id ORDER BY id) - 1 MOD (SELECT COUNT(DISTINCT product_id) FROM items) + 1'), 'ASC')
            ->orderBy('id', 'ASC')
            ->get();

        $items =  $items->map(
            function ($item) {
                cacheItem($item);
            }
        );
    }
}
