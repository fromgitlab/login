<?php
namespace App\Interfaces\V1;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Database\Eloquent\Model;
use App\Models\ActivityLog;
use App\Enums\LogEnum;

interface ActivityLogRepositoryInterface
{
    public function index($request) :JsonResource;
    public function store(LogEnum $action,Model $model) :void;
    public function storeGet(LogEnum $action,$response) :void;
    public function show(ActivityLog $activityLog) :JsonResource;
} 