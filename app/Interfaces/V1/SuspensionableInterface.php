<?php 
namespace App\Interfaces\V1;
interface SuspensionableInterface
{
    public function approveConditions($request);
    public function approveAction($request);
}