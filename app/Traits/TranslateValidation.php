<?php

namespace App\Traits;

use App\Enums\ModuleName;

/**
 * TranslateValidation trait that used with translate validation messages request 
 */
trait TranslateValidation
{

    protected function getValuesByKeys(array $keys, ModuleName $module_name, string $folder_name): array
    {
        // dd($this->get_translated_call($module_name->value, $folder_name));
        return array_intersect_key($this->get_translated_call($module_name->value, $folder_name), array_flip($keys));
    }


    protected static  function get_translated_call(string $module_name, string $folder_name): array
    {
        $translated_call = [
            'category' => [
                'common' => [
                    'en' => __('category::key.common.en'),
                    'ar' => __('category::key.common.ar'),
                    'en.text' => __('category::key.common.en_text'),
                    'ar.text' => __('category::key.common.ar_text'),
                ],
                'province' => [],
                'city' => [
                    'province_id' => __('category::key.city.province_id'),
                    'file' => __('category::key.city.file'),
                ],
                'avatar' => [
                    'avatar' => __('category::key.avatar.avatar'),
                ],

                'region' => [
                    'city_id' => __('category::key.region.city_id'),
                ],
                'type' => [
                    'type' => __('category::key.type.type'),
                    'key' => __('category::key.type.key'),
                ],
                'unit' => [
                    'type' => __('category::key.unit.type'),
                ],
            ],
            'announcement' => [
                'common' => [
                    'en' => __('announcement::key.common.en'),
                    'ar' => __('announcement::key.common.ar'),
                    'en.text' => __('announcement::key.common.en_text'),
                    'ar.text' => __('announcement::key.common.ar_text'),
                    'free_sample' => __('announcement::key.common.free_sample'),
                    'profile_id' => __('announcement::key.common.profile_id'),
                    'text' => __('announcement::key.common.text'),
                    'survey' => __('announcement::key.common.survey'),
                    'when_to_send' => __('announcement::key.common.when_to_send'),
                ],
                'announcement' => [
                    'description' => __('announcement::key.announcement.description'),
                    'local_path' => __('announcement::key.announcement.local_path'),
                    'url' => __('announcement::key.announcement.url'),
                    'product_id' => __('announcement::key.announcement.product_id'),
                    'uploaded_video_id' => __('announcement::key.announcement.uploaded_video_id'),
                ],
                'free_sample' => [
                    'item_id' => __('announcement::key.free_sample.item_id'),
                    'number_of_samples' => __('announcement::key.free_sample.number_of_samples'),
                    'profile_ids' => __('announcement::key.free_sample.profile_ids'),
                    'profile_ids.*' => __('announcement::key.free_sample.profile_ids_*'),
                ],
                'free_sample_request' => [
                    'item_id' => __('announcement::key.free_sample_request.item_id'),
                    'description' => __('announcement::key.free_sample_request.description'),
                    'quantity' => __('announcement::key.free_sample_request.quantity'),
                ],
                'slider' => [
                    'image' => __('announcement::key.slider.image'),
                    'type_id' => __('announcement::key.slider.type_id'),
                    'slider_type_id'=> __('announcement::key.slider.slider_type_id'),
                ],
                'survey_answer' => [
                    'answer' => __('announcement::key.survey_answer.answer'),
                    'survey_id' => __('announcement::key.survey_answer.survey_id'),
                ],
                'survey' => [
                    'free_sample_id' => __('announcement::key.survey.free_sample_id'),
                ],
                'survey_template' => [],
                'slider_type' => [
                    'key' => __('announcement::key.slider_type.key'),
                ],
            ],
            'auth' => [
                'common' => [
                    'phone_number' => __('auth::key.common.phone_number'),
                    'password' => __('auth::key.common.password'),
                    'profile_type' => __('auth::key.common.profile_type'),
                    'national_number' => __('auth::key.common.national_number'),
                    'id_card_data' => __('auth::key.common.id_card_data'),
                    'otp' => __('auth::key.common.otp'),
                ],
                'auth' => [
                    'device_id' => __('auth::key.auth.device_id'),
                    'created_at' => __('auth::key.auth.created_at'),
                    'fcm_token' => __('auth::key.auth.fcm_token'),
                    'locale' => __('auth::key.auth.locale'),
                    'last_login_at' => __('auth::key.auth.last_login_at'),
                    'user' => __('auth::key.auth.user'),

                    'name' => __('auth::key.auth.name'),
                    'birth_date' => __('auth::key.auth.birth_date'),
                    'region_id' => __('auth::key.auth.region_id'),
                    'gender' => __('auth::key.auth.gender'),

                    'avatar' => __('auth::key.auth.avatar'),
                    'qr_code' => __('auth::key.auth.qr_code'),
                    'user_id' => __('auth::key.auth.user_id'),
                ],
                'citizen' => [],
                'otp' => [
                    'type_id' => __('auth::key.otp.type_id'),
                ],
                'profile_auth' => [],
                'user' => [
                    'old_password' => __('auth::key.user.old_password'),
                    'email' => __('auth::key.user.email'),
                ],
            ],
            'interaction' => [
                'common' => [],
                'profile_id' => __('interaction::key.profile_id'),
            ],
            'loyalty' => [
                'common' => [
                    'en' => __('loyalty::key.common.en'),
                    'ar' => __('loyalty::key.common.ar'),
                    'en.text' => __('loyalty::key.common.en_text'),
                    'ar.text' => __('loyalty::key.common.ar_text'),
                    'image' => __('loyalty::key.common.image'),
                    'target' => __('loyalty::key.common.target'),
                ],

                'brand_point' => [
                    'value' => __('loyalty::key.brand_point.value'),
                    'description' => __('loyalty::key.brand_point.description'),
                ],

                'gift_order' => [],

                'gift' => [
                    'en.description' => __('loyalty::key.gift.en_description'),
                    'ar.description' => __('loyalty::key.gift.ar_description'),
                    'quantity' => __('loyalty::key.gift.quantity'),
                    'due_date' => __('loyalty::key.gift.due_date'),
                ],
                'stage_design' => [
                    'color' => __('loyalty::key.stage_design.color'),
                ],
                'stage_order' => [],

                'stage' => [
                    'stage_design_id' => __('loyalty::key.stage.stage_design_id'),
                ],

            ],
            'notification' => [],
            'operation' => [
                'common' => [],
                'profile_id' => __('operation::key.profile_id'),
                'prescription_items' => __('operation::key.prescription_items'),
                'prescription_items.*.item_id' => __('operation::key.prescription_items_*_item_id'),
                'citizen_id' => __('operation::key.citizen_id'),
                'phone_number' => __('operation::key.phone_number'),
                'name' => __('operation::key.name'),
                'images' => __('operation::key.images'),
            ],
            'setting' => [],
            'profile' => [
                'common' => [
                    'ar' => __('profile::key.common.ar'),
                    'en' => __('profile::key.common.en'),
                    'en.text' => __('profile::key.common.en_text'),
                    'ar.text' => __('profile::key.common.ar_text'),

                    'value' => __('profile::key.common.value'),
                    'type_id' => __('profile::key.common.type_id'),

                    'work_hours' => __('profile::key.common.work_hours'),
                    'work_hours.*.from' =>  __('profile::key.common.work_hours_*_from'),
                    'work_hours.*.to' =>    __('profile::key.common.work_hours_*_to'),
                    'work_hours.*.day' =>   __('profile::key.common.work_hours_*_day'),
                    'work_hours.*.notes' => __('profile::key.common.work_hours_*_notes'),

                    'spec_id' => __('profile::key.common.spec_id'),
                    'role_id' => __('profile::key.common.role_id'),
                    'region_id' => __('profile::key.common.region_id'),
                    'profile_id' => __('profile::key.common.profile_id'),
                    'title' => __('profile::key.common.title'),

                    'image' => __('profile::key.common.image'),
                    'file' => __('profile::key.common.file'),
                    'key' => __('profile::key.common.key'),
                    'description' => __('profile::key.emergency.description'),
                ],
                'contact' => [
                    'contact_type_id'=>__('profile::key.contact.contact_type_id'),
                    'entity_key'=>__('profile::key.contact.entity_key')
                ],
                'contact_type' => [],
                'emergency' => [
                ],
                'general_work_hour' => [
                    'workplace_id' => __('profile::key.general_work_hour.workplace_id'),

                ],
                'insurance' => [],
                'profile' => [
                    'sub_spec_id' => __('profile::key.profile.sub_spec_id'),
                    'sub_spec_id.*' => __('profile::key.profile.sub_spec_id_*'),
                    'bio' => __('profile::key.profile.bio'),
                    'insurance_ids' => __('profile::key.profile.insurance_ids'),
                    'insurance_ids.*' => __('profile::key.profile.insurance_ids_*'),
                    'name' => __('profile::key.profile.name'),
                    'phone_number' => __('profile::key.profile.phone_number'),
                    'email' => __('profile::key.profile.email'),
                    'job_title' => __('profile::key.profile.job_title'),
                    'brand_id' => __('profile::key.profile.brand_id'),
                    'status' => __('profile::key.profile.status'),
                    'en.reason' => __('profile::key.profile.en_reason'),
                    'ar.reason' => __('profile::key.profile.ar_reason'),
                    'files' => __('profile::key.profile.files'),
                    'recommendor' => __('profile::key.profile.recommendor'),
                    'recommended' => __('profile::key.profile.recommended'),
                ],
                'service' => [],
                'specialization' => [],
                'sub_specialization' => [],
                'work_hour' => [],
                'workplace' => [
                    'lon' => __('profile::key.workplace.lon'),
                    'lat' => __('profile::key.workplace.lat'),
                    'workplace_type_id' => __('profile::key.workplace.workplace_type_id'),
                    'services' => __('profile::key.workplace.services'),
                    'services.*' => __('profile::key.workplace.services_*'),
                    'images' => __('profile::key.workplace.images'),
                    'ar.description' => __('profile::key.workplace.ar_description'),
                    'en.description' => __('profile::key.workplace.en_description'),
                    'ar.address' => __('profile::key.workplace.ar_address'),
                    'en.address' => __('profile::key.workplace.en_address'),
                    'contacts' => __('profile::key.workplace.contacts'),
                    'contacts.*.type_id' =>    __('profile::key.workplace.contacts_*_type_id'),
                    'contacts.*.value' =>      __('profile::key.workplace.contacts_*_value'),
                    'contacts.*.description' => __('profile::key.workplace.contacts_*_description'),
                    'location' => __('profile::key.workplace.location'),
                    'insurances' => __('profile::key.workplace.insurances'),
                    'insurances.*' => __('profile::key.workplace.insurances_*'),
                    'deleted_images' => __('profile::key.workplace.deleted_images'),
                ],
                'workplace_type' => [
                ],
            ],
            'catalog' => [
                'common' => [
                    'ar' => __('catalog::key.common.ar'),
                    'en' => __('catalog::key.common.en'),
                    'en.text' => __('catalog::key.common.en_text'),
                    'ar.text' => __('catalog::key.common.ar_text'),
                    'type_id' => __('catalog::key.common.type_id'),
                    'image' => __('catalog::key.common.image'),
                    'licenser_id' => __('catalog::key.common.licenser_id'),
                    'brand_id' => __('catalog::key.common.brand_id'),
                    'file' => __('catalog::key.common.file'),

                ],
                'brand' => [
                    'title' =>              __('catalog::key.brand.title'),
                    'value' =>              __('catalog::key.brand.value'),
                    'stages' =>             __('catalog::key.brand.stages'),
                    'stages.*' =>           __('catalog::key.brand.stages_*'),
                    'stages.stage_id' =>    __('catalog::key.brand.stages_stage_id'),
                    'contacts' =>           __('catalog::key.brand.contacts'),
                    'contacts.*.type_id' => __('catalog::key.brand.contacts_*_type_id'),
                    'contacts.*.value' =>   __('catalog::key.brand.contacts_*_value'),
                    'type' =>               __('catalog::key.brand.type'),
                    'lon' =>                __('catalog::key.brand.lon'),
                    'lat' =>                __('catalog::key.brand.lat'),
                    'location' =>           __('catalog::key.brand.location'),
                    'profile_id' =>         __('catalog::key.brand.profile_id'),
                ],
                'catalog_category' => [],
                'feature' => [
                    'key' => __('catalog::key.feature.key'),
                ],
                'indication' => [],
                'ingredient' => [],
                'item' => [
                    'product_id' =>             __('catalog::key.item.product_id'),
                    'prescription_file' =>      __('catalog::key.item.prescription_file'),
                    'en.package_description' => __('catalog::key.item.en_package_description'),
                    'ar.package_description' => __('catalog::key.item.ar_package_description'),
                    'indications' =>            __('catalog::key.item.indications'), //array
                    'indications.*' =>          __('catalog::key.item.indications_*'),
                    'ingredients' =>            __('catalog::key.item.ingredients'), //array
                    'ingredients.*' =>          __('catalog::key.item.ingredients_*'),
                    'caliber' =>                __('catalog::key.item.caliber'), //array
                    'caliber.*' =>              __('catalog::key.item.caliber_*'),
                    'caliber_unit_id' =>        __('catalog::key.item.caliber_unit_id'), //array
                    'caliber_unit_id.*' =>      __('catalog::key.item.caliber_unit_id_*'),
                    'barcodes' =>               __('catalog::key.item.barcodes'), //array
                    'barcodes.*' =>             __('catalog::key.item.barcodes_*'),
                    'price' =>                  __('catalog::key.item.price'),
                    'moh_price' =>              __('catalog::key.item.moh_price'),
                    'dosage_form_option_id' =>  __('catalog::key.item.dosage_form_option_id'),
                    'catalog_category_id' =>    __('catalog::key.item.catalog_category_id'),

                ],
                'licenser' => [],
                'option' => [
                    'unit_id' => __('catalog::key.option.unit_id'),
                    'feature' => __('catalog::key.option.feature'),
                ],
                'product' => [
                    'item.en' =>                     __('catalog::key.product.item_en'),
                    'item.ar' =>                     __('catalog::key.product.item_ar'),
                    'item.en.text' =>                __('catalog::key.product.item_en_text'),
                    'item.ar.text' =>                __('catalog::key.product.item_ar_text'),
                    'item.en.package_description' => __('catalog::key.product.item_en_package_description'),
                    'item.ar.package_description' => __('catalog::key.product.item_ar_package_description'),
                    'item.image' =>                  __('catalog::key.product.item_image'),
                    'item.prescription_file' =>      __('catalog::key.product.item_prescription_file'),
                    'item.indications' =>            __('catalog::key.product.item_indications'), //array
                    'item.indications.*' =>          __('catalog::key.product.item_indications_*'),
                    'item.ingredients' =>            __('catalog::key.product.item_ingredients'), //array
                    'item.ingredients.*'    =>       __('catalog::key.product.item_ingredients_*'),
                    'item.caliber' =>                __('catalog::key.product.item_caliber'), //array
                    'item.caliber.*' =>              __('catalog::key.product.item_caliber_*'),
                    'item.caliber_unit_id' =>        __('catalog::key.product.item_caliber_unit_id'), //array
                    'item.caliber_unit_id.*' =>      __('catalog::key.product.item_caliber_unit_id_*'),
                    'item.catalog_category_id' =>    __('catalog::key.product.item_catalog_category_id'),
                    'item.barcodes' =>               __('catalog::key.product.item_barcodes'), //array
                    'item.price' =>                  __('catalog::key.product.item_price'),
                    'item.moh_price' =>              __('catalog::key.product.item_moh_price'),
                    'item.dosage_form_option_id' =>  __('catalog::key.product.item_dosage_form_option_id'),
                ],

            ],
            'website' => [
                'common' => [
                    'items_per_page' => __('website::key.items_per_page'),
                ],
                'join_order'=>[
                    'message'=>__('website::key.join_order.message'),
                    'title'=>__('website::key.join_order.title'),
                    'name'=>__('website::key.join_order.name'),
                    'email'=>__('website::key.join_order.name'),
                ],
                'auth'=>[
                    'phone_number'=>__('website::key.auth.phone_number'),
                    'password'=>__('website::key.auth.password'),
                    'name'=>__('website::key.auth.name'),
                ],

                'key' => __('website::key.key'),
                'value' => __('website::key.value'),
                'type' => __('website::key.type'),
            ],


        ];
        if ($folder_name == "") {
            return array_merge(
                $translated_call[$module_name]['common'],
                $translated_call[$module_name],
            );
        }
        return array_merge(
            $translated_call[$module_name]['common'],
            $translated_call[$module_name][$folder_name],
        );
    }
}
