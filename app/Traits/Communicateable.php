<?php

namespace App\Traits;

use Modules\Interaction\Entities\Alert;
use Modules\Interaction\Enums\AlertAppearanceTypeEnum;
use Modules\Interaction\Enums\AlertDissmissibilityEnum;
use Modules\Interaction\Enums\AlertTypesEnum;

/**
 * This trait used with models that are comminicateable 
 * it containst alerts and can be also other communication systems
 */
trait Communicateable{

    public function polyAlerts()
    {
        return $this->morphMany(Alert::class, 'alertable');
    }

    public function polyAlert($profile_id, $message_en, $message_ar, $reason_key = "unknown_reason", $type = AlertTypesEnum::INFO, $appearance_type = AlertAppearanceTypeEnum::TOAST, $dismissibility = AlertDissmissibilityEnum::DISMISSIBLE, $expired_at = null){
        $this->polyAlerts()->create([
            'profile_id' => $profile_id,
            'en' => ['text' => $message_en],
            'ar' => ['text' => $message_ar],
            'type' => $type->name,
            'dismissibility' => $dismissibility->name,
            'appearance_type' => $appearance_type->name,
            'reason_key' => $reason_key,
            'expired_at' => $expired_at
        ]);
        $this->update();
    }
}
?>
