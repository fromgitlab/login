<?php

namespace App\Traits;

trait BaseEnum
{

    public static function isValidValue(string $value): bool
    {
        return in_array($value, explode('-', self::getValues()));
    }

    public static function getValues(): string
    {
        $values = [];
        foreach (self::cases() as $case) {
            $values[] = $case->value;
        } 
        return implode('-', $values);
    }
    public static function getValuesToArray(): array
    {
        $values = [];
        foreach (self::cases() as $case) {
            $values[] = $case->value;
        }
       
        return $values;
    }
}
