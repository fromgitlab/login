<?php

namespace App\Traits;

use Auth;
use Illuminate\Database\Eloquent\Builder;

/**
 * Trais used with the enitities that has is_hidden field
 */
trait Hideable
{
    /**
     * Scope a query to only include hidden fields if they should be considered.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeConsiderHidden(Builder $query)
    {
        if ($this->shouldShowHidden()) {
            return $query;
        }

        return $query->where('is_hidden', false);
    }

    /**
     * Scope a query to exclude hidden fields.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithoutHidden(Builder $query)
    {
        return $query->where('is_hidden', 0);
    }

    /**
     * Scope a query to only include fields with the given brand ID.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  int  $brandId
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeByBrandId(Builder $query, $brandId)
    {
        return $query->where('brand_id', $brandId);
    }

    /**
     * Hide the field.
     *
     * @return void
     */
    public function hide()
    {
        $this->is_hidden = 1;
        $this->save();
    }

    /**
     * Unhide the field.
     *
     * @return void
     */
    public function unhide()
    {
        $this->is_hidden = 0;
        $this->save();
    }

    /**
     * Determine if hidden fields should be shown.
     *
     * @return bool
     */
    protected function shouldShowHidden()
    {
        $user = Auth::user();
        if (!$user) {
            return false;
        }

        $visibleRoles = static::$visibleRoles ?? [];

        foreach ($visibleRoles as $role) {
            if ($user->roles->contains('name', $role)) {
                return true;
            }
        }

        return false;
    }
}
