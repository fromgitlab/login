<?php

namespace App\Traits;

use App\Suspensions\GeneralPendingSuspension;
use App\Abstracts\Suspension as AbstractSuspension;
use Modules\Category\Entities\Suspension;
use App\Scopes\SuspensionScope;

/**
 * Suspension trait that used with suspensionable traits
 */
trait Suspensionable
{
    protected static function bootSuspensionable(): void
    {
        //REVIEW - may be need this global scope
        // static::addGlobalScope(new SuspensionScope);
    }
    public function suspensions()
    {
        return $this->morphMany(Suspension::class, 'suspensionable');
    }

    public function hardSuspend($reason_en, $reason_ar, $type = 'pending')
    {
        $this->suspensions()->delete();
        $this->suspensions()->create([
            'suspension_type' => $type,
            'en' => ['reason' => $reason_en],
            'ar' => ['reason' => $reason_ar],
        ]);
        $this->update();
    }

    public function suspend(AbstractSuspension $suspension = new GeneralPendingSuspension())
    {
        //ANCHORE - if there is multi suspensions feature it needs to be fixed here
        $this->suspensions()->delete();
        $this->hardSuspend($suspension->getReason('en'), $suspension->getReason('ar'), $suspension->getType());
    }

    public function unsuspend()
    {
        $this->suspensions()->delete();
        $this->update();
    }

    public function isSuspended($type = 'pending')
    {
        return $this->suspensions->where('suspension_type', $type)->count();
    }

    public function getLatestSuspension()
    {
        return $this->suspensions->sortByDesc('created_at')->first();
    }


    // this filter can call from params  or call inside query in case $shape==false 
    public function scopeSuspended($query, $shape = null)
    {
        if (is_null($shape)) {
            return $query;
        }

        if ($shape === false) {
            return $query->whereDoesntHave('suspensions');
        }
        $base = self::baseOperationHas($shape);
        $value = self::checkIfArray($shape['value'] ?? null, false);

        $query->$base('suspensions', function ($inner_query) use ($shape, $value) {

            return $inner_query->where('suspension_type', $shape['op'], $value);
        });
    }
}
