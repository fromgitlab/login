<?php

namespace App\Traits;

/**
 * Trait used with models that will be suspended automatically on model creation
 */
trait ForcelySuspensionable {
    use Suspensionable;
    public static function bootForcelySuspensionable(){
        static::created(function ($model){
            $model->suspend('Awaiting approval', 'بانتظار الموافقة');
        });
    }
}
