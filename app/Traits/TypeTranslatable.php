<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Modules\Category\Entities\TypeTranslation;
use Astrotomic\Translatable\Translatable;

trait TypeTranslatable
{
    use Translatable;

    public function getTranslationModelName(): string
    {
        return TypeTranslation::class; // override
    }

    public function getTranslationRelationKey(): string
    {
        // if ($this->translationForeignKey) {
        //     return $this->translationForeignKey;
        // }

        // return $this->getForeignKey();
        return 'model_id'; // override
    }


    public function getNewTranslation(string $locale): Model
    {
        $modelName = $this->getTranslationModelName();

        /** @var Model $translation */
        $translation = new $modelName();
        $translation->setAttribute($this->getLocaleKey(), $locale);
        $translation->setAttribute('model_type', $this->getMorphClass()); // override
        $this->translations->add($translation);
        return $translation;
    }

    public function translations(): MorphMany
    {
        return $this->morphMany(
            $this->getTranslationModelName(),
            'model'
        ); // override
    }
}
