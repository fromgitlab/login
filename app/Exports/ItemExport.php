<?php

namespace App\Exports;

use Cache;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithHeadings;

/** 
 * FIXME
 * This class is not COMPLETED and created to be used to export the items in the system 
*/
class ItemExport implements FromCollection, WithHeadings
{
    protected $items;

    public function __construct(Collection $items)
    {
        $this->items = $items;
    }

    public function headings(): array
    {
        return [
            'id',
            'brand_en',
            'brand_ar',
            'licenser_en',
            'licenser_ar',
            'category_en',
            'category_ar',
            'product_name_en',
            'product_name_ar',
            'item_name_en',
            'item_name_ar',
            'indications_en',
            'indications_ar',
            'price',
            'moh_price',
            'barcodes',
            'dosage_form_en',
            'dosage_form_ar',
            'package_description_en',
            'package_description_ar',
            'ingredients_en',
            'ingredients_ar',
        ];
    }

    public function collection()
    {
        $data = [];
        foreach ($this->items as $item) {
            $data[] = [
                'id' => $item->bulk_id,
                'brand_en' => $item->product->brand->translate('en')->text,
                'brand_ar' => $item->product->brand->translate('ar')->text,
                'licenser_en' => $item->product->licenser->translate('en')->text,
                'licenser_ar' => $item->product->licenser->translate('ar')->text,
                'category_en' => $item->catalog_category?->translate('en')?->text,
                'category_ar' => $item->catalog_category?->translate('ar')?->text,
                'product_name_en' => $item->product->translate('en')->text,
                'product_name_ar' => $item->product->translate('ar')->text,
                'item_name_en' => $item->translate('en')->text,
                'item_name_ar' => $item->translate('ar')->text,
                'indications_en' => implode('+', $item->indications->map(function ($indication) {
                    return $indication->translate('en')->indication;
                })->toArray()),
                'indications_ar' => implode('+', $item->indications->map(function ($indication) {
                    return $indication->translate('ar')->indication;
                })->toArray()),
                'price' => $item->price,
                'moh_price' => $item->moh_price,
                'barcodes' => implode('+', $item->barcodes->pluck('barcode')->toArray()),
                'dosage_form_en' => implode('+', $item->options->where('feature.text', 'dosage_form')->map(function ($option) {
                    return $option->translate('en')->text;
                })->toArray()),
                'dosage_form_ar' => implode('+', $item->options->where('feature.text', 'dosage_form')->map(function ($option) {
                    return $option->translate('ar')->text;
                })->toArray()),
                'package_description_en' => $item->translate('en')->package_description,
                'package_description_ar' => $item->translate('ar')->package_description,
                'ingredients_en' => implode('+', $item->ingredients->map(function ($ingredient) {
                    return $ingredient->translate('en')->text;
                })->toArray()),
                'ingredients_ar' => implode('+', $item->ingredients->map(function ($ingredient) {
                    return $ingredient->translate('ar')->text;
                })->toArray()),

                // 'caliber' => '',
            ];
        }
        // $data = ;

        return collect($data);
    }
}
