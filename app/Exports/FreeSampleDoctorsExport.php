<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Modules\Announcement\Entities\FreeSample;

/**
 * This class is used to export an excel sheet for the doctors that they are selected to receive a free sample
 */
class FreeSampleDoctorsExport implements FromCollection, WithHeadings
{

    protected $free_sample;

    public function __construct(FreeSample $free_sample)
    {
        $this->free_sample = $free_sample;
    }

    public function headings(): array
    {
        return [
            'id',
            'name',
            'specialization',
            'contacts',
            'address',
        ];
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $data = [];
        foreach ($this->free_sample->profiles as $key => $profile) {
            $addresses = '';
            foreach ($profile->workplaces as $workplace) {
                $addresses .= $workplace->translate('ar')->address . '-' . $workplace->translate('en')->address . '  ●  ';
            }
            $profile_contacts = '';
            $contacts = $profile->contacts()->whereHas('contactType', function ($query) {
                return $query->whereTranslationLike('text', 'Phone Number');
            })->get();

            foreach ($contacts as $contact) {
                $profile_contacts .= $contact->value . ' - ';
            }
            $addresses = rtrim($addresses, '  ●  ');
            $profile_contacts = rtrim($profile_contacts, ' - ');
            $data[] = [
                'id' => (string) ($key + 1),
                'name' => $profile->user->name,
                'specialization' => $profile->profileInfo?->specialization?->translate()?->text,
                'contacts' => $profile_contacts,
                'address' => $addresses,
            ];
        }
        return collect($data);
    }
}
