<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Interfaces\V1\ActivityLogRepositoryInterface;
use App\Repositories\V1\ActivityLogRepository;


class RepositoryServiceProvider extends ServiceProvider
{

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        if(config('app.api_latest') == '1'){
            $this->app->bind(ActivityLogRepositoryInterface::class, ActivityLogRepository::class);            
        }

    }


}
