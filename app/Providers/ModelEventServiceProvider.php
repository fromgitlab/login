<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Observers\ModelObserver;
use Nwidart\Modules\Facades\Module;


class ModelEventServiceProvider extends ServiceProvider
{

    /**
     * List of endpoints to exclude from model event observation.
     *
     * @var array
     */
    public $except_endpoints = [
        "api/v1/products/upload-products"
    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        if (request()->method() == "GET") {
            return;
        }

        // Check if the current request path is in the excluded endpoints list. If so, do nothing.
        if (in_array(request()->path(), $this->except_endpoints)) {
            return;
        }


        //ANCHOR - to make observe on parent model 
        // $path = app_path('Models') . '/*.php';
        // collect(glob($path))->map(function ($file) {
        //     $model = DIRECTORY_SEPARATOR . "App" . DIRECTORY_SEPARATOR . 'Models' . DIRECTORY_SEPARATOR .  basename($file, '.php');
        //     $model::observe(ModelObserver::class);
        // });


        // Loop through all modules
        foreach (Module::all() as $module) {
            $path = base_path("Modules/{$module}/Entities/*.php");
            // Collect all entity files and apply the callback function
            collect(glob($path))->each(function ($file) use ($module) {
                $entity = "Modules\\{$module}\\Entities\\" . pathinfo($file, PATHINFO_FILENAME);
                // Observe events on the entity using the ModelObserver
                $entity::observe(ModelObserver::class);
            });
        }


        //ANCHOR - old way 
        // $tables = Schema::getAllTables();
        // foreach ($tables as $table) {
        // $ModelClass = getModelByKey(Str::singular(array_values((array)$table)[0]));
        // if (is_subclass_of($ModelClass, BaseModel::class)) {
        //         $ModelClass::observe(ModelObserver::class);
        //     }
        // }
    }
}
