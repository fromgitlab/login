<?php

namespace App\Repositories\V1;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Route;
use App\Interfaces\V1\ActivityLogRepositoryInterface;
use App\Actions\Logs\LogAction;
use App\Helpers\LogHelper;
use App\Enums\LogEnum;
use App\Filters\RangeFilter;
use App\Http\Resources\Logs\ActivityLogs\ActivityLogResource;
use App\Models\ActivityLog;

use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class ActivityLogRepository implements ActivityLogRepositoryInterface
{

    public function index($request): JsonResource
    {
        return ActivityLogResource::collection(
            QueryBuilder::for(ActivityLog::class)
                ->allowedFilters([
                    AllowedFilter::custom('created_at', new RangeFilter())
                ])
                ->allowedSorts(['created_at'])
                ->search($request->search)
                ->orderByDesc('created_at')
                ->select(['id','log_id','description','subject_type','subject_id','batch_uuid','created_at','updated_at']) 
                ->with('log', 'log.causer:id,name,email,phone_number', 'log.event:id,template','subject')
                ->paginate($request->items_per_page)
        );
    }


    public function store(LogEnum $action, Model $model): void
    {
        $log = LogAction::execute($action);

        $properties = $model->toArray();
        unset($properties['translations']);
        $description =  LogHelper::getLogDescription($action, $model);

        $log->activityLogs()->create(['description' => $description, 'subject_id' => $model->getKey(), 'subject_type' => get_class($model), 'properties' => collect($properties), 'batch_uuid' => request()->header('ulid')]);
    }

    public function storeGet(LogEnum $action, $response): void
    {
        $log = LogAction::execute($action);

        $description = LogEnum::GET->value ." ". Route::current()->uri();

        $log->activityLogs()->create(['description' => $description, 'subject_id' => null, 'subject_type' => null, 'properties' => collect($response), 'batch_uuid' => request()->header('ulid')]);
    }

    public function show(ActivityLog $activityLog): JsonResource
    {
        $activityLog->load('log', 'log.causer:id,name,email,phone_number', 'log.event:id,template');
        return new ActivityLogResource($activityLog);
    }
}
