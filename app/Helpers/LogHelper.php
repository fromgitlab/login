<?php

namespace App\Helpers;

use App\Enums\LogEnum;
use Illuminate\Support\Str;

class LogHelper
{
    public static function getLogDescription($action, $model)
    {
        $description = null;
        switch ($action) {
            case LogEnum::ADD:
                $description = 'Added ' . Str::singular($model->getTable());
                break;

            case LogEnum::EDIT:
                $description = 'Edited ' . Str::singular($model->getTable());
                break;

            case LogEnum::DELETE:
                $description = 'Deleted ' . Str::singular($model->getTable());
                break;

            case LogEnum::ACTIVITY:
                $description = 'Performed activity on ' . Str::singular($model->getTable());
                break;

            case LogEnum::GET:
                $description = 'Get ' . Str::singular($model->getTable());
                break;
        }
        return $description;
    }
}
