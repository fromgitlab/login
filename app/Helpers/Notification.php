<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Http;

class Notification
{
    public static function sendToGoogleWorkspace($message, $url)
    {

        Http::withHeaders([
            'Authorization' => config('googleworkspace.authorization'),
            'Content-Type' => "application/json; charset=UTF-8",
        ])->post(
            $url,
            [
                'text' => $message
            ]
        );
    }
}
