<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Http;

class Otp
{

    public static function createAndSend($user)
    {
        try {
            $user->otps()->where('type', 'otp_phone_number')->delete();

            $new_otp_value =rand(pow(10, 3), pow(10, 4) - 1) ;

            $user->otps()->create([
                'value' => $new_otp_value,
                'type' => 'otp_phone_number'
            ]);

            $message =  $new_otp_value . " is The Verification Code For: " . $user->name ?? "no opt";

            // Notification::sendToGoogleWorkspace($message, config('googleworkspace.otps_url'));

            $response = Http::get(config('otp.syriatel_url'), [
                'user_name' => config('otp.syriatel_user_name'),
                'password' => config('otp.syriatel_password'),
                'sender' => "PharmaWay",
                'param_list' => $new_otp_value . ';' . request()->header('app-key') ,
                'to' => '963' . substr($user->phone_number, 1, 9),
                'template_code' => app()->getLocale() == 'en' ? 'Oroodi1_T4' : 'Oroodi1_T3'
            ]);
            
        } catch (\Exception $e) {
            
            abort(503, "Syriatel service not available");
        }
    }
}
