<?php

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;
use Modules\Profile\Entities\Profile;
use Nwidart\Modules\Facades\Module;

/**
 * Creates an API response object.
 *
 * @param string $message The message to include in the response.
 * @param mixed $data The data to include in the response, defaults to null.
 * @param int $status The HTTP status code for the response, defaults to 200.
 * @param array|null $meta Any other additional data to include in the response, defaults to null.
 *
 * @return 
 */
function apiResponse($message, $data = [], $status = 200, $meta = null, $extra = null)
{
    return response()->json(
        [
            'status' => $status,
            'message' => $message,
            'data' => $data,
            'meta' => $meta,
            'extra' => $extra,
        ],
        $status
    );
}

/**
 * Creates an API Error response object.
 *
 * @param string $message The message to include in the response.
 * @param mixed $errors The data to include in the response, defaults to null.
 * @param int $status The HTTP status code for the response

 * @param array|null $other Any other additional data to include in the response, defaults to null.
 *
 * @return 
 */
function apiErrorResponse($message, $errors = [], $status)
{
    return response()->json(
        [
            'status' => $status,
            'message' => $message,
            'errors' => $errors,
        ],
        $status
    );
}


function getLangFromHeader()
{
    return (request()->header('Accept-Language')) ? request()->header('Accept-Language') : 'en';
}

function transferDataWithPagination($data)
{
    return  [
        'items' => $data->items(),
        'pagination' => [
            'total' => $data->total(),
            'perPage' => $data->perPage(),
            'currentPage' => $data->currentPage(),
            'from' => $data->firstItem(),
            'to' => $data->lastItem(),
            'lastPage' => $data->lastPage(),
        ]
    ];
}

function transferData($data)
{
    return  [
        'items' => $data

    ];
}


function cacheItem($item)
{
    $item->load(['translations', 'ingredients.units.translations', 'ingredients.translations', 'options.translations', 'options.feature.translations', 'options.unit.translations', 'barcodes', 'indications.translations', 'product.licenser.translations', 'product.brand.translations', 'product.brand.contacts.contactType.translations', 'product.translations', 'product.items', 'catalogCategory', 'product.announcements']);
    Cache::forget('download-items-' . $item->id);
    return Cache::rememberForever('download-items-' . $item->id, fn () => $item->toArray());
}
/**
 * Gets the fully-qualified class name of a model based on its key.
 *
 * @param string $key The key of the model.
 *
 * @return string The fully-qualified class name of the model.
 */
function getModelByKey($key): string
{
    $key = Str::snake($key);
    $name = Str::studly($key);
    $modules = array_keys(Module::all());
    $policy_class = false;
    foreach ($modules as $module_name) {
        $path = DIRECTORY_SEPARATOR . "Modules" . DIRECTORY_SEPARATOR . $module_name . DIRECTORY_SEPARATOR . "Entities";
        $models = scandir(base_path() . $path);
        foreach ($models as $model) {
            if (explode('.', $model)[0] === $name) {
                $policy_class = str_replace(DIRECTORY_SEPARATOR, '\\', $path) . '\\' . $name;
            }
        }
    }
    return $policy_class;
}


/**
 * Encrypts plaintext using AES-128-CBC algorithm.
 *
 * @param string $plaintext The data to be encrypted.
 *
 * @return string The encrypted data.
 */
function aes_128_encrypt($plaintext)
{
    $key = config('app.key');
    $algorithm = 'AES-128-CBC';

    $iv = hex2bin("9023459e9ad233b16bd03b0b67dfc209");

    $data = openssl_encrypt($plaintext, $algorithm, $key, 0, $iv);
    return $data;
}

/**
 * Decrypts ciphertext using AES-128-CBC algorithm.
 *
 * @param string $ciphertext The encrypted data to be decrypted.
 *
 * @return string The decrypted data.
 */
function aes_128_decrypt($ciphertext)
{
    $key = config('app.key');
    $algorithm = 'AES-128-CBC';

    $iv = hex2bin("9023459e9ad233b16bd03b0b67dfc209");

    return openssl_decrypt($ciphertext, $algorithm, $key, 0, $iv);
}


/**
 * Gets the authenticated user's profile data from the request headers.
 *
 * @return Profile|null The authenticated user's profile instance, or null if not found.
 */
function authProfile()
{
    return unserialize(request()->header('auth-profile')) ?: null;
}

/**
 * Sets the authenticated user's profile data in the request headers.
 *
 * @param Profile $profile The authenticated user's profile instance.
 *
 * @return void
 */
function setAuthProfile($profile)
{
    request()->headers->set('auth-profile', serialize($profile));
}

/**
 * Replaces all spaces in a string with underscores.
 *
 * @param string $str The string to be processed.
 *
 * @return string The processed string with all spaces replaced with underscores.
 */
function replaceSpacesWithUnderscores($str)
{
    $str = str_replace(' ', '_', $str);
    return $str;
}
