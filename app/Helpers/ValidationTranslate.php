<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;



   
    
      function  getValuesByKeys( $keys, $module)
    {

        $translated_call =  [
            'category' => [
        
                'en' => __('category::key.en'),
                'ar' => __('category::key.ar'),
                'en.text' => __('category::key.en.text'),
                'ar.text' => __('category::key.ar.text'),
                'province_id' => __('category::key.province_id'),
            ],
            'catalog' => [
        
                'en' => __('category::key.en'),
                'ar' => __('category::key.ar'),
                'en.text' => __('category::key.en.text'),
                'ar.text' => __('category::key.ar.text'),
                'province_id' => __('category::key.province_id'),
            ]
        ];
        
        return array_intersect_key($translated_call[$module], array_flip($keys));
    }
