<?php

namespace App\Observers;

use Illuminate\Database\Eloquent\Model;
use App\Enums\LogEnum;
use App\Interfaces\V1\ActivityLogRepositoryInterface;

/**
 * Observe events on model's methods
 * used for logging data
 */
class ModelObserver
{
    /**
     * Create the observer
     *
     * @return void
     */
    public function __construct(private ActivityLogRepositoryInterface  $activityLogRepositoryInterface)
    {
        //
    }

    public function created(Model $model)
    {
        $this->activityLogRepositoryInterface->store(LogEnum::ADD, $model);
    }

    public function updated(Model $model)
    {
        $this->activityLogRepositoryInterface->store(LogEnum::EDIT, $model);
    }

    public function deleting(Model $model)
    {
        $this->activityLogRepositoryInterface->store(LogEnum::DELETE, $model);
    }
    // public function retrieved($model)
    // {
    //     
    //         $this->activityLogRepositoryInterface->store(LogEnum::GET, $model);
    //     }

}
