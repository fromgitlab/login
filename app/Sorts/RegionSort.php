<?php

namespace App\Sorts;

use Illuminate\Database\Eloquent\Builder;
use Spatie\QueryBuilder\Sorts\Sort;

/**
 * Class RegionSort
 * A custom sort used by the spatie/query-builder Package
 * @link https://spatie.be/docs/laravel-query-builder/v5/introduction
 * To sort on a regions
 * @package App\sorts
 */
class RegionSort implements Sort
{

    public function __invoke(Builder $query, bool $descending, string $property)
    {
        $direction = $descending ? 'desc' : 'asc';
        $query->join('users', 'users.id', '=', 'profiles.user_id');
        $query->join('regions', 'regions.id', '=', 'users.region_id');
        $query->join('region_translations', 'region_translations.id'  , '=', 'regions.id')->where('region_translations.locale', app()->getLocale());


        $query->orderBy('region_translations.name', $direction);
    }
}
