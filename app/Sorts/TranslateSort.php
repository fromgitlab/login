<?php


namespace App\Sorts;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class TranslateSort
 * A custom sort used by the spatie/query-builder Package
 * @link https://spatie.be/docs/laravel-query-builder/v5/introduction
 * To sort on Translations created with astrotomic/translatable package
 * @link https://github.com/Astrotomic/laravel-translatable
 * @package App\sorts
 */
class TranslateSort implements \Spatie\QueryBuilder\Sorts\Sort
{
    public function __invoke(Builder $query, bool $descending, string $property)
    {
        $direction = $descending ? 'desc' : 'asc';

        $query->orderByTranslation($property,$direction);
    }
}
