<?php


namespace App\Sorts;

use Illuminate\Database\Eloquent\Builder;

/**
 * Class UserNameSort
 * A custom sort used by the spatie/query-builder Package
 * @link https://spatie.be/docs/laravel-query-builder/v5/introduction
 * To sort on User Name from the users table
 * @package App\sorts
 */
class UserNameSort implements \Spatie\QueryBuilder\Sorts\Sort
{
    public function __invoke(Builder $query, bool $descending, string $property)
    {
        $direction = $descending ? 'desc' : 'asc';
        $query->select('profiles.*')
            ->joinSub(
                'select id, name from users',
                'users',
                'profiles.user_id',
                '=',
                'users.id'
            )
            ->orderBy('users.name', $direction);
    }
}
