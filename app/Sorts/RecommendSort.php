<?php


namespace App\Sorts;
use Illuminate\Database\Eloquent\Builder;
use MatanYadaev\EloquentSpatial\Objects\Point;

/**
 * Class DistanceSort
 * A custom sort used by the spatie/query-builder Package
 * @link https://spatie.be/docs/laravel-query-builder/v5/introduction
 * To sort on a distance calculated by the haversine formula from the stored geometry point called location
 * @package App\sorts
 */
class RecommendSort implements \Spatie\QueryBuilder\Sorts\Sort
{
    public function __invoke(Builder $query, bool $descending, string $property)
    {
        $direction = $descending ? 'asc' : 'desc';
        $query->selectRaw('((SELECT count(*) FROM profile_recommendations WHERE profile_recommendations.profile_id = profiles.id)) AS is_recommended_status');
        $query->orderBy('is_recommended_status', $direction);
    }
}
