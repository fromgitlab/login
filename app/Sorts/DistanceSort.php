<?php


namespace App\Sorts;
use Illuminate\Database\Eloquent\Builder;
use MatanYadaev\EloquentSpatial\Objects\Point;

/**
 * Class DistanceSort
 * A custom sort used by the spatie/query-builder Package
 * @link https://spatie.be/docs/laravel-query-builder/v5/introduction
 * To sort on a distance calculated by the haversine formula from the stored geometry point called location
 * @package App\sorts
 */
class DistanceSort implements \Spatie\QueryBuilder\Sorts\Sort
{
    public function __invoke(Builder $query, bool $descending, string $property)
    {
        $direction = $descending ? 'desc' : 'asc';
        $location = explode(',', request()->input('location'));
        $lat = $location[0];
        $lon = $location[1];
        $query->orderByDistance('location', new Point($lat, $lon), $direction);
    }
}
