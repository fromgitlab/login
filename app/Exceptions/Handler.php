<?php

namespace App\Exceptions;

use App\Helpers\Notification;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Http;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\AuthenticationException;
use Throwable;
use Error;

class Handler extends ExceptionHandler
{
    /**
     * A list of exception types with their corresponding custom log levels.
     *
     * @var array<class-string<\Throwable>, \Psr\Log\LogLevel::*>
     */
    protected $levels = [
        //
    ];

    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<\Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            
        });
    }

    public function render($request, Throwable $exception)
    {
        // if ($exception instanceof Error) 
        // {
        //     $message = '<users/112160228934512137324> 500 Server Error: ' . $exception->getMessage();
        //     Notification::sendToGoogleWorkspace($message, config('googleworkspace.errors_url'));
        // }
        

        return parent::render($request, $exception);
    }

    /**
     * Create a response object from the given validation exception.
     *
     * @param  \Illuminate\Validation\ValidationException  $e
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function convertValidationExceptionToResponse(ValidationException $e, $request)
    {
        if ($e->response) {
            return $e->response;
        }
              
        return apiErrorResponse(
            $e->getMessage(),
            $e->errors(),
            $e->status
        ); 
            } 

            protected function unauthenticated($request, AuthenticationException $exception)
            {
                return apiErrorResponse(
                    $exception->getMessage(),
                    (object)[],
                    401 
                ); 
            }

     /**
     * Prepare a JSON response for the given exception.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $e
     * @return \Illuminate\Http\JsonResponse
     */
    protected function prepareJsonResponse($request, Throwable $e)
    {

         return apiErrorResponse(
            $e->getMessage(),
            (object)[],   ($this->isHttpException($e) ? $e->getStatusCode() : 500)
           
        ); 
  
    }
                    }
