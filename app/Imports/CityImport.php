<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Modules\Category\Entities\City;
use Modules\Category\Entities\Province;
use Modules\Category\Entities\Region;

/**
 * This class is used to import the cities and the regions inside it from an excel sheet
 */
class CityImport implements ToCollection, WithHeadingRow
{
    /**
     * @param Collection $collection
     */
    public function collection(Collection $rows)
    {
        // $provinces=Province::get();

        // Province::query()->delete();

        foreach ($rows as $row) {
            $province = Province::whereTranslation('text', $row['province_en'])->first();
            if (!$province) {
                $province = Province::create([
                    'en' => ['text' => $row['province_en']],
                    'ar' => ['text' => $row['province_ar']],
                ]);
            }
            $city = City::whereTranslation('text', $row['city_en'])->first();
            if (!$city) {
                $city =  City::create([
                    'en' => ['text' => $row['city_en']],
                    'ar' => ['text' => $row['city_ar']],
                    'province_id' => $province->id,
                ]);
            }
            $region = City::whereTranslation('text', $row['region_en'])->first();
            if (!$region) {
                $region = Region::create([
                    'en' => ['text' => $row['region_en']],
                    'ar' => ['text' => $row['region_ar']],
                    'city_id' => $city->id,
                ]);
            }
        }
    }
}
