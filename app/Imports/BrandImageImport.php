<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Modules\Catalog\Entities\Brand;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

/**
 * This importer used to import brand images from an excel sheet that has the brand name and the brand image path
 * to be stored in the database in brands table
 */
class BrandImageImport implements ToCollection, WithHeadingRow
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row)
        {
            $brand = Brand::whereTranslation('text', $row['name'])->first();
            if($brand){
                $brand->image = "brands/".str_replace('\\', '/',$row['image']);
                $brand->save();
            };
        }
    }
}
