<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Modules\Category\Entities\City;
use Modules\Category\Entities\Province;
use Modules\Category\Entities\Region;
use Modules\Category\Entities\Unit;

/**
 * This class is used to import the cities and the regions inside it from an excel sheet
 */
class UnitImport implements ToCollection, WithHeadingRow
{
    /**
     * @param Collection $collection
     */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            $unit_row =  [
                'en' => ['text' => $row['unit_en']],
                'ar' => ['text' => $row['unit_ar']],
                'type' => 'CALIBER_UNIT'
            ];

            $curr_unit = Unit::whereTranslation('text', $row['unit_en'])->first();
            $unit = $curr_unit ? $curr_unit : Unit::create($unit_row);
        }
    }
}
