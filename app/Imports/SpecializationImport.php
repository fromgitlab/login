<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Modules\Profile\Entities\Specialization;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Collection;
use Modules\Auth\Entities\Role;

/**
 * This importer used to import the specializations from an excel sheet
 */
class SpecializationImport implements ToCollection, WithHeadingRow
{
    /**
     * @param Collection $rows
     */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            $specialization = Specialization::whereTranslation('text', $row['specialization_en'])->first();
            $specialization = $specialization ?: Specialization::create([
                'en' => ['text' => $row['specialization_en']],
                'ar' => ['text' => $row['specialization_ar']],
                'role_id' => Role::where('name', 'doctor')->first()->id,
                'image' => $row['image'] ?  'specializations' . '/' . $row['image'] : null
            ]);
        }
    }
}
