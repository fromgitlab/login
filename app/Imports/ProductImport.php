<?php

namespace App\Imports;


use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;

use Modules\Catalog\Entities\CatalogCategory;
use Modules\Catalog\Entities\Ingredient;
use Modules\Catalog\Entities\Indication;
use Modules\Catalog\Entities\Feature;
use Modules\Catalog\Entities\Product;
use Modules\Category\Entities\Unit;
use Modules\Catalog\Entities\Brand;
use Modules\Catalog\Entities\Item;

use Modules\Catalog\Rules\Products\ProductMatchRule;

use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithValidation;

/**
 * This importer is used to import the products to the system with full information about the items in it
 */
class ProductImport implements
    ToCollection,
    WithHeadingRow,
    SkipsOnError,
    WithValidation,
    SkipsOnFailure,
    WithEvents
{
    use Importable, SkipsErrors, SkipsFailures, RegistersEventListeners;
    /**
     * @param Collection $collection
     */

    public function rules(): array
    {
        return [
            '*.id' => ['required'],
            '*.item_name_en' => ['required', new ProductMatchRule],
            '*.item_name_ar' => ['required'],
            '*.product_name_en' => ['required', new ProductMatchRule],
            '*.product_name_ar' => ['required'],
            '*.price' => ['required'],
            '*.brand_en' => ['required', new ProductMatchRule, 'exists:brand_translations,text'],
            '*.brand_ar' => ['required'],
            '*.item_image' => ['nullable'],
            '*.moh_price' => ['required'],
            '*.category_en' => ['required', new ProductMatchRule],
            '*.category_ar' => ['required'],
            '*.indications_en' => ['required', new ProductMatchRule],
            '*.indications_ar' => ['required'],
            '*.dosage_form_en' => ['required', new ProductMatchRule],
            '*.dosage_form_ar' => ['required'],
            '*.caliber' => ['required']
        ];
    }

    public function collection(Collection $rows)
    {


        foreach ($rows as $row) {

            $product = Product::whereTranslation('text', $row['product_name_en'])->first();
            $category = CatalogCategory::whereTranslation('text', $row['category_en'])->first();
            $brand = Brand::whereTranslation('text', $row['brand_en'])->first();

            $product = $product ?: Product::create([
                'en' => ['text' => $row['product_name_en']],
                'ar' => ['text' => $row['product_name_ar']],
                'brand_id' => $brand->id,
                'licenser_id' => null,      //FIXME 
            ]);

            if (!$category && $row['category_en'] && $row['category_ar']) {
                $category = $category ?: CatalogCategory::create([
                    'en' => ['text' => $row['category_en'] ?: ''],
                    'ar' => ['text' => $row['category_ar'] ?: ''],
                ]);
            }

            $feature_names = ['Dosage Form'];
            $feature_keys = ['dosage_form'];
            $product->features()->detach();
            for ($i = 0; $i < count($feature_names); $i++) {
                $feature = Feature::where('key', $feature_keys[$i])->first();
                $feature = $feature ?: Feature::create([
                    'en' => ['text' => $feature_names[$i]],
                    'ar' => ['text' => $feature_names[$i]],
                    'key' => $feature_keys[$i],

                ]);
                $product->features()->attach($feature);
            }

            $parts = [];

            if (preg_match("/(.*)\((.*)\)/", str_replace([' +', '+ '], '+', $row['indications_en']), $parts)) {
                $other = collect(explode('+', str_replace([' +', '+ '], '+', $parts[2])))->map(function ($indication) use ($parts) {
                    return $indication . '(' . $parts[1] . ')';
                });
                $indications_en = $other;
            } else {
                $indications_en = explode('+', str_replace([' +', '+ '], '+', $row['indications_en']));
            }
            $parts = [];
            if (preg_match("/(.*)\((.*)\)/", str_replace([' +', '+ '], '+', $row['indications_ar']), $parts)) {
                $other = collect(explode('+', str_replace([' +', '+ '], '+', $parts[2])))->map(function ($indication) use ($parts) {
                    return $indication . '(' . $parts[1] . ')';
                });
                $indications_ar = $other;
            } else {
                $indications_ar = explode('+', str_replace([' +', '+ '], '+', $row['indications_ar']));
            }

            $item = Item::updateOrCreate(['bulk_id' => $row['id']], [
                'en' => [
                    'text' => $row['item_name_en'],
                    'package_description' => $row['package_description_en'],
                ],
                'ar' => [
                    'text' => $row['item_name_ar'],
                    'package_description' => $row['package_description_ar']
                ],
                'batch_ulid' => request()->header('ulid'),
                'product_id' => $product->id,
                'price' => $row['price'],
                'moh_price' => $row['moh_price'],
                'image' => $row['item_image'] ? 'item/' . $row['item_image'] : null,
                'bulk_id' => $row['id'],
                'catalog_category_id' => $category->id,
            ]);



            $indications_count = count($indications_en);
            $item->indications()->detach();
            for ($i = 0; $i < $indications_count; $i++) {

                $indication = Indication::whereTranslation('text', $indications_en[$i])->first();
                $indication = $indication ?: Indication::create([
                    'en' => ['text' => $indications_en[$i]],
                    'ar' => ['text' => $indications_ar[$i]],
                ]);
                $item->indications()->attach($indication);
            }


            //FIXME  must store barcode but not found in excel file 

            //------------------------------------------------ Ingredients Caliber !!! ---------------------------------------------

            $ingredients_en = explode('+', $row['ingredients_en']);
            //FIXME   must store ingredients_ar from excel file   
            $ingredients_ar = explode('+', $row['ingredients_ar']); //FIXME 

            $caliber_field = $row['caliber'];
            preg_match_all('/\//', $caliber_field, $matches);
            if (count($matches[0]) == 1 && !preg_match('/\/.+\+/', $caliber_field)) {
                $calibers_units_additional  = explode('/', $caliber_field);
            } else {
                $calibers_units_additional  = [$caliber_field];
            }

            $calibers_units = $calibers_units_additional[0];
            $additional_unit = $calibers_units_additional[1] ?? null;

            $calibers = [];
            $units = [];

            foreach (explode('+', $calibers_units) as $caliber_unit) {
                preg_match_all("/[A-Za-z\%]/", $caliber_unit, $matches, PREG_OFFSET_CAPTURE);
                $split_in = $matches[0][0][1] ?? strlen($caliber_unit);

                $unit = substr($caliber_unit, $split_in, strlen($caliber_unit) + 1);
                $calibers[] = substr($caliber_unit, 0, $split_in);
                $units[] = $additional_unit ? $unit . '/' . $additional_unit : $unit;
            }

            $ingredients_count = count($ingredients_en);

            $item->ingredients()->detach();
            $item->options()->detach();
            for ($i = 0; $i < $ingredients_count; $i++) {
                $ingredient = Ingredient::whereTranslation('text', $ingredients_en[$i])->first();
                $ingredient = $ingredient ?: Ingredient::create([
                    'en' => ['text' => $ingredients_en[$i]],
                    'ar' => ['text' => $ingredients_ar[$i]],
                ]);
                if (isset($units[$i])) {
                    $unit_id = Unit::whereTranslation('text', $units[$i])->first();
                    $unit_id = $unit_id ?: Unit::create([
                        'en' => ['text' => $units[$i]],
                        'ar' => ['text' => $units[$i]],
                        'type' => 'CALIBER_UNIT'
                    ]);

                    $item->ingredients()->attach($ingredient, ['caliber' => $calibers[$i] ?? null, 'unit_id' => $unit_id->id]);
                } else {

                    $item->ingredients()->attach($ingredient, ['caliber' => isset($calibers[$i]) ?? $calibers[0], 'unit_id' => null]); //FIXME - check 
                }
            }

            //------------------------------------------------------------------------------------------------------

            $dosage_form_option = $row['dosage_form_en'];

            $dosage_form = Feature::where('key', 'dosage_form')->first();
            $dosage_form_option = $dosage_form->options()->whereTranslation('text', $dosage_form_option)->first();
            $dosage_form_option = $dosage_form_option ?: $dosage_form->options()->create([
                'en' => ['text' => $row['dosage_form_en']],
                'ar' => ['text' => $row['dosage_form_ar']],
                'unit_id' => 1   //FIXME 
            ]);
            $item->options()->attach($dosage_form_option);
            cacheItem($item);
        }
    }
}
