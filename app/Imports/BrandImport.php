<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Modules\Catalog\Entities\Brand;
use Point;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Modules\Profile\Entities\ContactType;

class BrandImport implements
    ToCollection,
    WithHeadingRow,
    WithValidation,
    SkipsOnFailure,
    WithEvents
{
    use Importable, SkipsFailures, RegistersEventListeners;

    public function rules(): array
    {
        return [
            'brand_en' => ['required'],
            'brand_ar' => ['required'],
            'image' => ['sometimes'],
            'location' => ['sometimes'],
            'phone' => [
                'sometimes',
                // 'regex:/^(\+?\d{9,14})+$/'
            ],
            'telephone' => [
                'sometimes',
                // 'regex:/^(?!09|9)\d{4,10}$/'
            ],
            'email' => [
                'sometimes',
                // 'regex:/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/'
            ],
            'website' => [
                'sometimes',
                // 'regex:/^(https?:\/\/)?(www\.)?[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.[a-zA-Z]{2,}([\/?][^\s]*)?$/'
            ],
        ];
    }
    /**
     * @param Collection $collection
     */
    public function collection(Collection $rows)
    {
        $phone_number_contact_type = ContactType::whereTranslationLike('text', 'Phone Number')->firstOr(function () {
            return ContactType::create([
                'ar' => ['text' => 'Phone Number'],
                'en' => ['text' => 'رقم الجوال'],
            ]);
        });

        $telephone_contact_type = ContactType::whereTranslationLike('text', 'Telephone')->firstOr(function () {
            return ContactType::create([
                'ar' => ['text' => 'Telephone'],
                'en' => ['text' => 'رقم الهاتف'],
            ]);
        });

        $email_contact_type = ContactType::whereTranslationLike('text', 'Email')->firstOr(function () {
            return ContactType::create([
                'ar' => ['text' => 'Email'],
                'en' => ['text' => 'بريد الكتروني'],
            ]);
        });

        $website_contact_type = ContactType::whereTranslationLike('text', 'Website')->firstOr(function () {
            return ContactType::create([
                'ar' => ['text' => 'Website'],
                'en' => ['text' => 'موقع إلكتروني'],
            ]);
        });

        foreach ($rows as $row) {

            $brand = Brand::whereTranslation('text', $row['brand_en'])->firstOr(function () use ($row) {
                return Brand::create([
                    'en' => ['text' => $row['brand_en']],
                    'ar' => ['text' => $row['brand_ar']],
                    'image' => ($row['image'])                        ? ('brands/' . $row['image']) : null,
                    'location' => ($row['location'] instanceof Point) ?  $row['location']           : null,
                ]);
            });


            if (isset($row['phone'])) {

                $brand->contacts()->create([
                    'value' => $row['phone'],
                    'contact_type_id' => $phone_number_contact_type->id,
                ]);
            }
            if (isset($row['telephone'])) {

                $brand->contacts()->create([
                    'value' => $row['telephone'],
                    'contact_type_id' => $telephone_contact_type->id,
                ]);
            }
            if (isset($row['email'])) {

                $brand->contacts()->create([
                    'value' => $row['email'],
                    'contact_type_id' => $email_contact_type->id,
                ]);
            }

            if (isset($row['website'])) {
                $brand->contacts()->create([
                    'value' => $row['website'],
                    'contact_type_id' => $website_contact_type->id,
                ]);
            }
        }
    }
}
