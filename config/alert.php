<?php

use Modules\Interaction\Enums\AlertAppearanceTypeEnum;
use Modules\Interaction\Enums\AlertDissmissibilityEnum;
use Modules\Interaction\Enums\AlertTypesEnum;

return [
    'blocked' => [
        'type' => AlertTypesEnum::DANGER->name,
        'dismissibility' => AlertDissmissibilityEnum::NON_DISMISSIBLE->name,
        'appearance_type' => AlertAppearanceTypeEnum::POPUP->name,
        'reason_key' => 'admin_block',
        'expired_at' =>  null,
    ],
    'pending' => [
        'type' => AlertTypesEnum::WARNING->name,
        'dismissibility' => AlertDissmissibilityEnum::LOCALLY_DISMISSIBLE->name,
        'appearance_type' => AlertAppearanceTypeEnum::POPUP->name,
        'reason_key' => 'admin_pending',
        'expired_at' =>  null
    ],
    'rejected' => [
        'type' => AlertTypesEnum::WARNING->name,
        'dismissibility' => AlertDissmissibilityEnum::DISMISSIBLE->name,
        'appearance_type' => AlertAppearanceTypeEnum::POPUP->name,
        'reason_key' => 'admin_reject',
        'expired_at' =>  null
    ],
    // Add more states and their corresponding names as needed
];