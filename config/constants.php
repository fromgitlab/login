<?php 

return [
    'roles' => [
        'doctor' => 'provider',
        'brand_operator' => 'provider',
        'admin' => 'admin',
        'user' => 'user'
    ]
]
?>