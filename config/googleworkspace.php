<?php

return [
    'otps_url' => env('GOOGLE_OTPS_SPACE_URL'),
    'errors_url' => env('GOOGLE_OTPS_SPACE_URL'),
    'authorization'=>env('GOOGLE_WORKSPACE_AUTHORIZATION'),
];
