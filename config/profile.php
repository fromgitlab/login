<?php

return [
    'name' => 'Profile',

    // the rules that the authenticated profile follows in case of suspension
    'suspension_rules' => [
        // for pending type, and so on
        'pending' => [
            // the routes of these methods are allowed
            'allowed_methods' => [
                'GET'
            ],
            //those routes are allowed
            'excluded_routes' => [
                'user' => [
                    'api/v1/device/register' => 'POST',
                    'api/v1/auth/logout' => 'POST',
                    'api/v1/auth/login/web' => 'POST',
                    'api/v1/auth/login/mobile' => 'POST',
                    'api/v1/auth/login-check-phone' => 'POST',
                ],

                'doctor' => [
                    'api/v1/device/register' => 'POST',
                    'api/v1/auth/logout' => 'POST',
                    'api/v1/auth/login/web' => 'POST',
                    'api/v1/auth/login/mobile' => 'POST',
                    'api/v1/auth/login-check-phone' => 'POST',
                ],

                'brand_operator' => [
                    // 'api/v1/device/register' => 'POST',
                    // 'api/v1/auth/logout' => 'POST',
                    // 'api/v1/auth/login/web' => 'POST',
                    // 'api/v1/auth/login/mobile' => 'POST',
                    // 'api/v1/auth/login-check-phone' => 'POST',
                ],
            ],
            //those routes are not allowed if the profile is suspended
            'protected_routes' => [
                'user' => [],
                'doctor' => [
                    // 'api/v1/v1/device/register' => 'POST',
                    // 'api/v1/items' => 'GET'

                ],
                'brand_operator' => [],
            ]
        ],
        'rejected' => [
            'allowed_methods' => [
                'GET'
            ],
            'excluded_routes' => [
                'user' => [
                    // 'api/v1/device/register' => 'POST',
                    // 'api/v1/auth/logout' => 'POST',
                    // 'api/v1/auth/login/web' => 'POST',
                    // 'api/v1/auth/login/mobile' => 'POST',
                    // 'api/v1/auth/login-check-phone' => 'POST',
                ],

                'doctor' => [
                    // 'api/v1/device/register' => 'POST',
                    // 'api/v1/auth/logout' => 'POST',
                    // 'api/v1/auth/login/web' => 'POST',
                    // 'api/v1/auth/login/mobile' => 'POST',
                    // 'api/v1/auth/login-check-phone' => 'POST',
                ],

                'brand_operator' => [
                    // 'api/v1/device/register' => 'POST',
                    // 'api/v1/auth/logout' => 'POST',
                    // 'api/v1/auth/login/web' => 'POST',
                    // 'api/v1/auth/login/mobile' => 'POST',
                    // 'api/v1/auth/login-check-phone' => 'POST',
                ],
            ],
            'protected_routes' => [
                'user' => [],
                'doctor' => [],
                'brand_operator' => [
                    'api/v1/auth/login/web' => 'POST',
                    'api/v1/auth/login/mobile' => 'POST',
                ],
            ]
        ],
        'blocked' => [
            'allowed_methods' => [
                // 'GET'
            ],
            'excluded_routes' => [
                'user' => [
                    // 'api/v1/device/register' => 'POST',
                    // 'api/v1/auth/logout' => 'POST',
                    // 'api/v1/auth/login/web' => 'POST',
                    // 'api/v1/auth/login/mobile' => 'POST',
                    // 'api/v1/auth/login-check-phone' => 'POST',

                ],

                'doctor' => [
                    // 'api/v1/device/register' => 'POST',
                    // 'api/v1/auth/logout' => 'POST',
                    // 'api/v1/auth/login/web' => 'POST',
                    // 'api/v1/auth/login/mobile' => 'POST',
                    // 'api/v1/auth/login-check-phone' => 'POST',
                ],

                'brand_operator' => [
                    // 'api/v1/device/register' => 'POST',
                    // 'api/v1/auth/logout' => 'POST',
                    // 'api/v1/auth/login/web' => 'POST',
                    // 'api/v1/auth/login/mobile' => 'POST',
                    // 'api/v1/auth/login-check-phone' => 'POST',
                ],
            ],
            'protected_routes' => [
                'user' => [],
                'doctor' => [],
                'brand_operator' => [
                    'api/v1/auth/login/web' => 'POST',
                    'api/v1/auth/login/mobile' => 'POST',
                ],
            ]
        ]
    ]
];
