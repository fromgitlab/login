<?php

return [
    'expire' => 5,
    'syriatel_url' => env('SYRIATEL_URL'),
    'syriatel_user_name' => env('SYRIATEL_USER_NAME'),
    'syriatel_password' => env('SYRIATEL_PASSWORD'),
];
