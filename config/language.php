<?php
return [
    'header'=>[
        'Accept-language' => app()->runningInConsole() === false? request()->header('Accept-Language') : 'en',
    ]
];
