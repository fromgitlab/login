<?php

return [
    'name' => 'catalog',
    'item' => [
        'optimize_queries' => [
            'donwload' => [
                'response' => [
                    'item' => [
                        'fields' => 'id,name,description,image,videos,price,moh_price,product,barcodes,ingredients,dosage_form,package_form,package_content,siblings_images,indications,catalog_category,package_description',
                        'only_fields' => '1',
                    ], 'product' => [
                        'fields' => 'id,name,brand,licenser',
                        'only_fields' => '1',
                    ], 'brand' => [
                        'fields' => 'id,name,image,active,contacts',
                        'only_fields' => '1',
                    ], 'licenser' => [
                        'fields' => 'id,name',
                        'only_fields' => '1',
                    ], 'option' => [
                        'fields' => 'id,name,unit',
                        'only_fields' => '1',
                    ], 'unit' => [
                        'fields' => 'id,name,type',
                        'only_fields' => '1',
                    ], 'indication' => [
                        'fields' => 'id,name',
                        'only_fields' => '1',
                    ], 'catalog_category' => [
                        'fields' => 'id,name',
                        'only_fields' => '1',
                    ],
                ]
            ]
        ]
    ]
];
