<?php
namespace Modules\Notification\Http\Controllers;


use App\Http\Controllers\BaseController;
use Modules\Notification\Entities\Notification;
use Modules\Notification\Http\Requests\NotificationIndexRequest;
use Modules\Notification\Http\Requests\NotificationShowRequest;


class NotificationController extends BaseController
{

    /**
     * Get all notifications filtered and paginated
     * the notifications are filtered on the user (each user reads his own notifications)
     * @param NotificationIndexRequest $request
     * @return 
     */
    public function index(NotificationIndexRequest $request)
    {
    }


    /**
     * @param NotificationShowRequest $request
     * @param EntitiesNotification $notification
    
     * @throws \Spatie\DataTransferObject\Exceptions\UnknownProperties
     */
    public function show(NotificationShowRequest $request, Notification $notification)
    {

    }

    /**
     * **PROBABLY AN ABANDONED FUNCTION
     * @return \Illuminate\Http\JsonResponse
     */
    public function getNotificationsNumber(){
    }


    /**
     * **PROBABLY AN ABANDONED FUNCTION
     * @param $notification_info
    
     * @throws \Spatie\DataTransferObject\Exceptions\UnknownProperties
     */
    public function store($notification_info)
    {
    }

}
