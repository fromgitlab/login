<?php

namespace Modules\Notification\Enums;

enum NotificationTypeEnum : string{
    case NOTIFICATION = 'notification';
    case AUTHENTICATION = 'authentication';
}