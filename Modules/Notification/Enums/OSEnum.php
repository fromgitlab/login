<?php

namespace Modules\Notification\Enums;

enum OSEnum:string {
    case ANDROID = 'ANDROID';
    case IOS = 'IOS';
}