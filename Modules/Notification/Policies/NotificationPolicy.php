<?php

namespace Modules\Notification\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

class NotificationPolicy
{
    public static function browse($user = null){
        $allowed = true;

        return $allowed;
    }

    public static function read($user){
        $allowed = true;

        return $allowed;
    }

    public static function add($user){
        $allowed = true;

        return $allowed;
    }

    public static function edit($user){
        $allowed = true;

        return $allowed;
    }

    public static function delete($user){
        $allowed = true;

        return $allowed;
    }
}
