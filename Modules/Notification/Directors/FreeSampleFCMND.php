<?php 

namespace Modules\Notification\Directors;

use Modules\Announcement\Entities\FreeSample;
use Modules\Notification\Directors\Abstracts\FCMNotificationDirector;
use Modules\Notification\Notifications\Abstracts\FCMNotification;
use Modules\Profile\Entities\Profile;

class FreeSampleFCMND extends FCMNotificationDirector{

    protected $free_sample;

    public function __construct(FreeSample $free_sample, FCMNotification $notification)
    {
        $this->notification = $notification;
        $this->free_sample = $free_sample;
    }

    
    // NOTE This function targets the entities related to the current entity (GiftOrder)
    // regardless of the actual notification
    public function target()
    {
        // Pick the related profiles
        $this->profiles = ($this->free_sample->profiles)->
        push($this->free_sample->item->product->brand->profile)
        ->merge(Profile::whereHas('role', function($q){
            return $q->where('roles.name', 'admin');
        })->get());
        return $this;
    }
}
