<?php 

namespace Modules\Notification\Directors;

use Modules\Announcement\Entities\Survey;
use Modules\Announcement\Entities\SurveyAnswer;
use Modules\Notification\Directors\Abstracts\FCMNotificationDirector;
use Modules\Notification\Notifications\Abstracts\FCMNotification;
use Modules\Profile\Entities\Profile;

class SurveyAnswerFCMND extends FCMNotificationDirector{

    protected $survey_answer;

    public function __construct(SurveyAnswer $survey_answer, FCMNotification $notification)
    {
        $this->survey_answer = $survey_answer;
        $this->notification = $notification;
    }

    public function target()
    {
        // Pick the related profiles
        $this->profiles = collect([$this->survey_answer->profile])
        ->push($this->survey_answer->survey->freeSample->item->product->brand->profile);
        return $this;
    }
}


?>