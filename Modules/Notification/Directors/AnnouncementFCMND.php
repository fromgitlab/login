<?php 

namespace Modules\Notification\Directors;

use Modules\Announcement\Entities\Announcement;
use Modules\Notification\Directors\Abstracts\FCMNotificationDirector;
use Modules\Notification\Notifications\Abstracts\FCMNotification;
use Modules\Profile\Entities\Profile;

class AnnouncementFCMND extends FCMNotificationDirector{
    protected $announcement;
    public function __construct(Announcement $announcement, FCMNotification $notification)
    {
        $this->announcement = $announcement;
        $this->notification = $notification;
    }

    public function target()
    {
        // Pick the related profiles
        $this->profiles = collect([$this->announcement->product->brand->profile])
        ->merge(Profile::whereHas('role', function($q){
            return $q->where('roles.name', 'admin');
        })->get())
        ->merge(Profile::whereHas('role', function($q){
            return $q->where('roles.name', 'doctor');
        })->get());
        
        return $this;
    }
}
