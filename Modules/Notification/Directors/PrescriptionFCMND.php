<?php

namespace Modules\Notification\Directors;

use Modules\Notification\Directors\Abstracts\FCMNotificationDirector;
use Modules\Notification\Notifications\Abstracts\FCMNotification;
use Modules\Operation\Entities\Prescription;
use Modules\Profile\Entities\Profile;

class PrescriptionFCMND extends FCMNotificationDirector
{

    protected $prescription;

    public function __construct(Prescription $prescription, FCMNotification $notification)
    {
        $this->notification = $notification;
        $this->prescription = $prescription;
    }

    
    // NOTE This function targets the entities related to the current entity (GiftOrder)
    // regardless of the actual notification
    public function target()
    {
        // Pick the related profiles
        $this->profiles = collect([$this->prescription->profile])
        //the doctor who wrote the prescription
        ->push($this->prescription->profile)
        //the patient who the prescription wrote for
        ->push($this->prescription->citizen?->user->userProfile)
        //All brands that have items in this prescription
        ->merge(Profile::whereHas('brands.products.items', function ($query) {
            $query->whereIn('items.id', $this->prescription->items->pluck('id'));
        })) 
        //all Admins
        ->merge(Profile::whereHas('role', function($q){
            return $q->where('roles.name', 'admin');
        })->get());
        
        return $this;
    }
}
