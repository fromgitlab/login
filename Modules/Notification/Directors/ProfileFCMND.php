<?php

namespace Modules\Notification\Directors;

use Modules\Notification\Directors\Abstracts\FCMNotificationDirector;
use Modules\Notification\Notifications\Abstracts\FCMNotification;
use Modules\Profile\Entities\Profile;

class ProfileFCMND extends FCMNotificationDirector
{

    protected $profile;

    public function __construct(Profile $profile, FCMNotification $notification)
    {
        $this->notification = $notification;
        $this->profile = $profile;
    }

    
    // NOTE This function targets the entities related to the current entity (GiftOrder)
    // regardless of the actual notification
    public function target()
    {
        // Pick the related profiles
        $this->profiles = collect([$this->profile])
        ->merge(Profile::whereHas('role', function($q){
            return $q->where('roles.name', 'admin');
        })->get());
        return $this;
    }
}
