<?php

namespace Modules\Notification\Directors;

use Modules\Auth\Entities\User;
use Modules\Notification\Directors\Abstracts\FCMNotificationDirector;
use Modules\Notification\Notifications\Abstracts\FCMNotification;
use Modules\Profile\Entities\Profile;

class AuthUserFCMND extends FCMNotificationDirector
{

    protected $user;

    public function __construct(User $user, FCMNotification $notification)
    {
        $this->notification = $notification;
        $this->user = $user;
    }

    
    // NOTE This function targets the entities related to the current entity (User)
    // regardless of the actual notification
    public function target()
    {
        // Pick the related profiles
        $this->profiles = $this->user->profiles;
        return $this;
    }
}
