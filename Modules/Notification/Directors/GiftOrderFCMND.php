<?php

namespace Modules\Notification\Directors;

use Modules\Loyalty\Entities\GiftOrder;
use Modules\Notification\Directors\Abstracts\FCMNotificationDirector;
use Modules\Notification\Notifications\Abstracts\FCMNotification;
use Modules\Profile\Entities\Profile;

class GiftOrderFCMND extends FCMNotificationDirector
{

    protected $gift_order;

    public function __construct(GiftOrder $order, FCMNotification $notification)
    {
        $this->notification = $notification;
        $this->gift_order = $order;
    }

    
    // NOTE This function targets the entities related to the current entity (GiftOrder)
    // regardless of the actual notification
    public function target()
    {
        // Pick the related profiles
        $this->profiles = collect([$this->gift_order->gift->brand->profile])
        ->push($this->gift_order->profile)->merge(Profile::whereHas('role', function($q){
            return $q->where('roles.name', 'admin');
        })->get());
        return $this;
    }
}
