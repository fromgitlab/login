<?php

namespace Modules\Notification\Directors;

use Modules\Loyalty\Entities\Gift;
use Modules\Notification\Directors\Abstracts\FCMNotificationDirector;
use Modules\Notification\Notifications\Abstracts\FCMNotification;
use Modules\Profile\Entities\Profile;

class GiftFCMND extends FCMNotificationDirector
{

    protected $gift;
    public function __construct(Gift $gift, FCMNotification $notification)
    {
        $this->gift = $gift;
        $this->notification = $notification;
    }

    public function target()
    {
        // Pick the related profiles
        $this->profiles = collect([$this->gift->brand->profile])
        ->merge(Profile::whereHas('role', function($q){
            return $q->where('roles.name', 'admin');
        })->get());
        return $this;
    }
}
