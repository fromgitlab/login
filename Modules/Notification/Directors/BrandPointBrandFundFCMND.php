<?php

namespace Modules\Notification\Directors;

use Modules\Catalog\Entities\Brand;
use Modules\Loyalty\Entities\GiftOrder;
use Modules\Notification\Directors\Abstracts\FCMNotificationDirector;
use Modules\Notification\Notifications\Abstracts\FCMNotification;
use Modules\Profile\Entities\Profile;

class BrandPointBrandFundFCMND extends FCMNotificationDirector
{

    protected $brand;

    public function __construct(Brand $brand, FCMNotification $notification)
    {
        $this->notification = $notification;
        $this->brand = $brand;
    }

    
    // NOTE This function targets the entities related to the current entity (GiftOrder)
    // regardless of the actual notification
    public function target()
    {
        // Pick the related profiles
        $this->profiles = collect([$this->brand->profile])->merge(Profile::whereHas('role', function($q){
            return $q->where('roles.name', 'admin');
        })->get());
        return $this;
    }
}
