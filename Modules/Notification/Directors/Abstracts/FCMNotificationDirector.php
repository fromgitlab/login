<?php


namespace Modules\Notification\Directors\Abstracts;


use Illuminate\Support\Collection;
use Modules\Notification\Contracts\NotificationMaker;
use Modules\Notification\Contracts\Notifier;
use Illuminate\Database\Eloquent\Model;
use Modules\Auth\Entities\Device;
use Modules\Auth\Entities\User;
use Modules\Catalog\Entities\Product;
use Modules\Notification\Abstracts\NotificationDirector;
use Modules\Notification\Senders\FCMSender;

/**
 * Class NotificationDirector
 * A class that provides specific notification build logic 
 * It decides what users to send the notifications to.
 * @package Modules\Notification\Makers
 */
abstract class FCMNotificationDirector implements NotificationDirector
{
    protected $notification;
    protected $profiles;

    public function instanciateNotifications(): array
    {
        // Assign their devices to the instance
        $devices = Device::whereIn('profile_id', $this->profiles->pluck('id')->toArray())->with(['profile.role'])->get();
        $roles = array_keys($this->notification->getMessages());
        $notifications = [];
        foreach ($roles as $role) {
            $notifications[] = [
                'notification' => (clone $this->notification)->setLocale('en')->setRole($role),
                'devices' => $devices->where('profile.role.name', $role)->where('locale', 'en')
            ];
            $notifications[] =  [
                'notification' => (clone $this->notification)->setLocale('ar')->setRole($role),
                'devices' => $devices->where('profile.role.name', $role)->where('locale', 'ar')
            ];
        }
        return $notifications;
    }

    public function send()
    {
        $this->target();
        $notifications = $this->instanciateNotifications();
  
        foreach ($notifications as $instance) {
            if(isset($instance['devices']))
            FCMSender::send($instance['notification'], $instance['devices']);
        }
    }
}
