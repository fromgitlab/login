<?php


namespace Modules\Notification\Directors\Abstracts;


use Illuminate\Support\Collection;
use Modules\Notification\Contracts\Notifier;
use Illuminate\Database\Eloquent\Model;
use Modules\Auth\Entities\User;
use Modules\Catalog\Entities\Product;
use Modules\Notification\Abstracts\NotificationDirector;

/**
 * Class NotificationDirector
 * A class that provides specific notification build logic 
 * It decides what users to send the notifications to.
 * @package Modules\Notification\Makers
 */
class AuthNotificationDirector implements NotificationDirector
{

    public function target(){
       
    }

    public function instanciateNotifications(){

    }

    public function send(){

    }

}
