<?php

namespace Modules\Notification\Notifications;

use Modules\Loyalty\Entities\GiftOrder;
use Modules\Notification\Enums\NotificationTypeEnum;
use Modules\Notification\Notifications\Abstracts\FCMNotification;

class GiftOrderStoreNotification extends FCMNotification
{
    
    public $gift_order;

    public function __construct(GiftOrder $order)
    {
        $this->gift_order = $order;
        $this->type = NotificationTypeEnum::NOTIFICATION->value;
    }

    public  function getMessages($locale = 'en'): array
    {
        return [
            'brand_operator' => [
                'title' => "A gift has been requested",
                'body' => $this->gift_order->profile->user->name . ' ordered ' . $this->gift_order->gift->translate()->name,
            ],
            // 'doctor' => [
            //     'title' => "Hello Doctor, A gift has been requested",
            //     'body' => $this->gift_order->profile->user->name . ' ordered ' . $this->gift_order->gift->translate()->name,
            // ],'admin' => [
            //     'title' => "Hello Admin, A gift has been requested",
            //     'body' => $this->gift_order->profile->user->name . ' ordered ' . $this->gift_order->gift->translate()->name,
            // ],
        ];
    }

    public  function generateData(): array
    {
        return [];
    }
}
