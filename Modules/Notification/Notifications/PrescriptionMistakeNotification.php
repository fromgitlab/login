<?php 

namespace Modules\Notification\Notifications;

use Modules\Notification\Enums\NotificationTypeEnum;
use Modules\Notification\Notifications\Abstracts\FCMNotification;
use Modules\Operation\Entities\Prescription;

class PrescriptionMistakeNotification extends FCMNotification{
    public $prescription;

    public function __construct(Prescription $prescription)
    {
        $this->prescription = $prescription;
        $this->type = NotificationTypeEnum::NOTIFICATION->value;
    }

    public  function getMessages($locale = 'en'): array
    {
        return [
            'doctor' => [
                'title' => "A Mistake Happened",
                'body' => 'Sorry we re-calculate some of your points due to an entry mistake happened.'
            ],
        ];
    }

    public  function generateData(): array
    {
        return [];
    }
}
?>