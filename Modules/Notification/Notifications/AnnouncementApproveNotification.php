<?php 

namespace Modules\Notification\Notifications;

use Modules\Announcement\Entities\Announcement;
use Modules\Notification\Enums\NotificationTypeEnum;
use Modules\Notification\Notifications\Abstracts\FCMNotification;

class AnnouncementApproveNotification extends FCMNotification {

    protected $announcement;

    public function __construct(Announcement $announcement)
    {
        $this->announcement = $announcement;
        $this->type = NotificationTypeEnum::NOTIFICATION->value;
    }

    public  function getMessages($locale = 'en'): array
    {
        return [
            'brand_operator' => [
                'title' => 'Your Video Request accepted',
                'body' => $this->announcement->translate($locale)->title . 'published successfully.',
            ],
            'doctor'=>[
                'title' => $this->announcement->product->brand->translate($locale)->name. 'added a new video',
                'body' => 'Browse more videos about medicines now!',                
            ]
        ];
    }

    public  function generateData(): array
    {
        return [];
    }
}

?>