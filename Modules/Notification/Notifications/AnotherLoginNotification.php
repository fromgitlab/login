<?php 

namespace Modules\Notification\Notifications;

use Modules\Announcement\Entities\SurveyAnswer;
use Modules\Auth\Entities\User;
use Modules\Notification\Enums\NotificationTypeEnum;
use Modules\Notification\Notifications\Abstracts\FCMNotification;

class AnotherLoginNotification extends FCMNotification{
    
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
        $this->type = NotificationTypeEnum::NOTIFICATION->value;
    }

    public  function getMessages($locale = 'en'): array
    {
        return [
            'brand_operator' => [
                'title' => 'Someone tried to login to your account',
                'body' => 'Hello, '.$this->user->name. ", Someone logged into your account, if it's not you, consider changing the password",
            ],
        ];
    }

    public  function generateData(): array
    {
        return [];
    }
}

?>