<?php 

namespace Modules\Notification\Notifications;

use Modules\Announcement\Entities\FreeSample;
use Modules\Catalog\Entities\Brand;
use Modules\Loyalty\Entities\Gift;
use Modules\Notification\Enums\NotificationTypeEnum;
use Modules\Notification\Notifications\Abstracts\FCMNotification;

class FreeSampleSendNotification extends FCMNotification{
    
    public $free_sample;

    public function __construct(FreeSample $free_sample)
    {
        $this->free_sample  = $free_sample;
        $this->type = NotificationTypeEnum::NOTIFICATION->value;
    }

    public  function getMessages($locale = 'en'): array
    {
        return [
            'doctor' => [
                'title' => "New Free Sample",
                'body' => $this->free_sample->item->product->brand->translate($locale)->name .' sent you a free sample',
            ],
        ];
    }

    public  function generateData(): array
    {
        return [];
    }
}
?>