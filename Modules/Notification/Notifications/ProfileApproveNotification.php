<?php 

namespace Modules\Notification\Notifications;

use Modules\Loyalty\Entities\GiftOrder;
use Modules\Notification\Enums\NotificationTypeEnum;
use Modules\Notification\Notifications\Abstracts\FCMNotification;
use Modules\Profile\Entities\Profile;

class ProfileApproveNotification extends FCMNotification{
    
    public $profile;

    public function __construct(Profile $profile)
    {
        $this->profile = $profile;
        $this->type = NotificationTypeEnum::NOTIFICATION->value;
    }

    public  function getMessages($locale = 'en'): array
    {
        return [
            'doctor' => [
                'title' => "Welcome To Pharmaway",
                'body' => 'Your account has been confirmed successfully, you can benefit from our services now.',
            ],
            'brand_operator' => [
                'title' => "Welcome To Pharmaway",
                'body' => 'Your account has been confirmed successfully, you can benefit from our services now.',
            ],
            'user' => [
                'title' => "Welcome To Pharmaway",
                'body' => 'Your account has been confirmed successfully, you can benefit from our services now.',
            ],
        ];
    }

    public  function generateData(): array
    {
        return [];
    }
}
?>