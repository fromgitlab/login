<?php 

namespace Modules\Notification\Notifications;

use Modules\Catalog\Entities\Brand;
use Modules\Loyalty\Entities\Gift;
use Modules\Notification\Enums\NotificationTypeEnum;
use Modules\Notification\Notifications\Abstracts\FCMNotification;

class GiftLowQuantityNotification extends FCMNotification{
    
    public $gift;

    public function __construct(Gift $gift)
    {
        $this->gift  = $gift;
        $this->type = NotificationTypeEnum::NOTIFICATION->value;
    }

    public  function getMessages($locale = 'en'): array
    {
        return [
            'brand_operator' => [
                'title' => "Your gifts quantity are about to expire",
                'body' => 'The remainder of '. $this->gift->translate($locale)->name .' gift about '.$this->gift->quantity.'. You can modify the quantity now',
            ],
        ];
    }

    public  function generateData(): array
    {
        return [];
    }
}
?>