<?php

namespace Modules\Notification\Notifications;

use Modules\Announcement\Entities\FreeSample;
use Modules\Catalog\Entities\Brand;
use Modules\Loyalty\Entities\Gift;
use Modules\Notification\Enums\NotificationTypeEnum;
use Modules\Notification\Notifications\Abstracts\FCMNotification;
use Modules\Profile\Entities\Profile;

class FreeSampleReceiveNotification extends FCMNotification
{

    public $free_sample;
    public $profile;

    public function __construct(FreeSample $free_sample, Profile $profile)
    {
        $this->free_sample  = $free_sample;
        $this->profile  = $profile;
        $this->type = NotificationTypeEnum::NOTIFICATION->value;
    }

    public  function getMessages($locale = 'en'): array
    {
        if ($locale = 'en') {
            return [
                'brand_operator' => [
                    'title' => "Free sample received succesfully",
                    'body' =>  $this->profile->user->name . ' received ' . $this->free_sample->item->translate($locale)->name . ' sample.'
                ],
            ];
        }else{
            return [
                'brand_operator' => [
                    'title' => "تم استلام العيّنة المجانية",
                    'body' =>  $this->profile->user->name . '  استَلم عيِّنة' . $this->free_sample->item->translate($locale)->name
                ],
            ];
        }
    }

    public  function generateData(): array
    {
        return [];
    }
}
