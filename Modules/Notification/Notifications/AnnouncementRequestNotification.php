<?php 

namespace Modules\Notification\Notifications;

use Modules\Announcement\Entities\Announcement;
use Modules\Notification\Enums\NotificationTypeEnum;
use Modules\Notification\Notifications\Abstracts\FCMNotification;

class AnnouncementRequestNotification extends FCMNotification {

    protected $announcement;

    public function __construct(Announcement $announcement)
    {
        $this->announcement = $announcement;
        $this->type = NotificationTypeEnum::NOTIFICATION->value;
    }

    public  function getMessages($locale = 'en'): array
    {
        return [
            'admin' => [
                'title' => 'Video Request',
                'body' => $this->announcement->product->brand->translate($locale)->name .' requests a video with name '. $this->announcement->translate($locale)->title,
            ],
        ];
    }

    public  function generateData(): array
    {
        return [];
    }
}

?>