<?php 

namespace Modules\Notification\Notifications;

use Modules\Loyalty\Entities\GiftOrder;
use Modules\Notification\Enums\NotificationTypeEnum;
use Modules\Notification\Notifications\Abstracts\FCMNotification;

class GiftOrderReceiveNotification extends FCMNotification{
    
    public $gift_order;

    public function __construct(GiftOrder $order)
    {
        $this->gift_order = $order;
        $this->type = NotificationTypeEnum::NOTIFICATION->value;
    }

    public  function getMessages($locale = 'en'): array
    {
        return [
            'brand_operator' => [
                'title' => "A gift has been received successfuly",
                'body' => $this->gift_order->profile->user->name . ' received ' . $this->gift_order->gift->translate()->name . 'successfully',
            ],
        ];
    }

    public  function generateData(): array
    {
        return [];
    }
}
?>