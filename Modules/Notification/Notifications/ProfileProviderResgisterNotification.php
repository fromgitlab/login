<?php 

namespace Modules\Notification\Notifications;

use Modules\Loyalty\Entities\GiftOrder;
use Modules\Notification\Enums\NotificationTypeEnum;
use Modules\Notification\Notifications\Abstracts\FCMNotification;
use Modules\Profile\Entities\Profile;

class ProfileProviderResgisterNotification extends FCMNotification{
    
    public $profile;

    public function __construct(Profile $profile)
    {
        $this->profile = $profile;
        $this->type = NotificationTypeEnum::NOTIFICATION->value;
    }

    public  function getMessages($locale = 'en'): array
    {
        return [
            'admin' => [
                'title' => "New Joining Request",
                'body' => $this->profile->user->name . ' wants to join the system.',
            ],
        ];
    }

    public  function generateData(): array
    {
        return [];
    }
}
?>