<?php 

namespace Modules\Notification\Notifications;

use Modules\Announcement\Entities\SurveyAnswer;
use Modules\Notification\Enums\NotificationTypeEnum;
use Modules\Notification\Notifications\Abstracts\FCMNotification;

class SurveyAnswerNotification extends FCMNotification{
    
    protected $survey_answer;

    public function __construct(SurveyAnswer $survey_answer)
    {
        $this->survey_answer = $survey_answer;
        $this->type = NotificationTypeEnum::NOTIFICATION->value;
    }

    public  function getMessages($locale = 'en'): array
    {
        return [
            'brand_operator' => [
                'title' => 'New answer to your '. $this->survey_answer->survey->freeSample->item->translate($locale)->name. ' survey',
                'body' => $this->survey_answer->profile->user->name . ' answered the survey you created it for ' . $this->survey_answer->survey->freeSample->item->translate($locale)->name,
            ],
        ];
    }

    public  function generateData(): array
    {
        return [];
    }
}

?>