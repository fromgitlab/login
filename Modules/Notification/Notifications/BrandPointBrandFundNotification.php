<?php 

namespace Modules\Notification\Notifications;

use Modules\Catalog\Entities\Brand;
use Modules\Notification\Enums\NotificationTypeEnum;
use Modules\Notification\Notifications\Abstracts\FCMNotification;

class BrandPointBrandFundNotification extends FCMNotification{
    
    public $brand;
    public $number_of_points;

    public function __construct(Brand $brand, $number_of_points)
    {
        $this->number_of_points = $number_of_points;
        $this->brand = $brand;
        $this->type = NotificationTypeEnum::NOTIFICATION->value;
    }

    public  function getMessages($locale = 'en'): array
    {
        return [
            'brand_operator' => [
                'title' => "The balance has been charged ",
                'body' => 'The balance has been charged with '. $this->number_of_points. ' points.',
            ],
        ];
    }

    public  function generateData(): array
    {
        return [];
    }
}
?>