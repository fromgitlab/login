<?php

namespace Modules\Notification\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Modules\User\Entities\User;

class Notification extends Model
{
    use HasFactory;

    /**
     * @var string[] The fillable attributes of the notification table
     */
    protected $fillable = [
        'id',
        'content',
        'notifiable',
        'notifiable_id',
        'user_id',
        'is_read',
        'title',
        'body',
        'pointer_type',
        'pointer_id',
    ];
    /**
     * Get the notifiable model that is related to this notification.
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function notifiable()
    {
        return $this->morphTo('notifiable', 'notifiable');
    }

    /**
     * **PROBABLY ABANDONED OLD NOTIFICATION
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ticket()
    {
        return $this->belongsTo(Ticket::class, 'ticket_id');
    }

    /**
     * The user that receives this notification
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Filter on the notification pointer
     * The notification indicates a sub-entity that the notification is referring to,
     * so that the frontend could use it to redirect the user to a specific screen or show a specific piece of data
     * @param Builder $query
     * @param $class
     * @return Builder
     */
    public function scopePointer(Builder $query, $class)
    {
        $q = $query->where('pointer_type', $class);
        return $q;
    }
}
