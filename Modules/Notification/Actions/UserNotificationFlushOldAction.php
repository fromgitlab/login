<?php


namespace Modules\Notification\Actions;

/**
 * Class UserNotificationFlushOldAction
 * Flush (delete) old user notifications to save space.
 * @package Modules\Notification\Actions
 */
class UserNotificationFlushOldAction
{

    /**
     * notifications that had been created one week and earlier from the last user request to the system
     * are deleted
     * @param $user
     */
    public static function execute($user)
    {
//        $user->notifications()
//            ->where('created_at','<',$user->second_used_at->subWeeks((setting('admin.notification.flush_before'))))
//            ->delete();

        $user->notifications()
            ->where('created_at','<',$user->last_request_at->subWeeks(1))
            ->delete();

    }
}
