<?php


namespace Modules\Notification\Actions;


use Illuminate\Database\Eloquent\Model;
use Modules\Auth\Entities\User;


/**
 * Class UserNotificationFlushAction
 * Flush (delete) all user notifications.
 * used in some cases when the user role changes.
 * @package Modules\Notification\Actions
 */
class UserNotificationFlushAction
{

    public static function execute(?Model $model, User $user)
    {
        $notifications = $user->notifications();
        $notifications = $model ?
            $notifications
            ->where('notifiable',  get_class($model))
            ->where('notifiable_id',  $model->id)
            : $notifications;

        $notifications = $notifications->get()->map(function ($notification) {
            $notification->delete();
        });
    }
}
