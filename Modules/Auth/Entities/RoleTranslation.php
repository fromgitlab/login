<?php

namespace Modules\Auth\Entities;

use App\Models\BaseModel;

class RoleTranslation  extends BaseModel
{
    public $timestamps = false;
    protected $fillable = ['text'];
}
