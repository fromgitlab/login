<?php

namespace Modules\Auth\Entities;

use App\Models\BaseModel;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\Profile\Entities\Profile;

class Role extends Model
{
    use   Translatable;

    protected $fillable = ['name', 'type_id'];
    public $translatedAttributes= ['text'];

    //SECTION - Relations -------------------------------------------------------------------------------------------------------
    public function profiles()
    {
        return $this->hasMany(Profile::class, 'user_id');
    }
}
