<?php

namespace Modules\Auth\Entities;

use App\Models\BaseModel;

use Modules\Operation\Entities\Prescription;

class Citizen  extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'national_number',
        'id_card_data',
    ];

    //SECTION - Relations -------------------------------------------------------------------------------------------------------
    public function user(){
        return $this->hasOne(User::class , 'citizen_id');
    }
    public function referrer(){
        return $this->belongsTo( User::class, 'by_who');
    }

}
