<?php

namespace Modules\Auth\Entities;

use Modules\Profile\Entities\LifestyleInformation;
use Modules\Profile\Entities\PersonalInformation;
use Modules\Profile\Entities\MedicalInformation;
use Modules\Announcement\Entities\Announcement;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Modules\Category\Entities\Region;
use Modules\Profile\Entities\Profile;
use Modules\GPT3\Entities\GPTMessage;
use Laravel\Sanctum\HasApiTokens;
use App\Traits\Communicateable;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Modules\Profile\Enums\ProfileStatusEnum;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, Communicateable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'phone_number',
        'name',
        'email',
        'phone_number_verified_at',
        'password',
        'birth_date',
        'avatar',
        'gender',
        'address',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'phone_number_verified_at' => 'datetime',
    ];

    //SECTION - Relations -------------------------------------------------------------------------------------------------------
    public function citizen()
    {
        return $this->belongsTo(Citizen::class);
    }

    public function region()
    {
        return $this->belongsTo(Region::class, 'region_id');
    }

    public function referrals()
    {
        return $this->hasMany(Citizen::class, 'by_who');
    }

    public function otps()
    {
        return $this->hasMany(Otp::class, 'user_id');
    }

    public function profiles()
    {
        return $this->hasMany(Profile::class, 'user_id');
    }
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'profiles')->withTimestamps()
            ->withPivot('id', 'image');
    }

    public function announcements()
    {
        return $this->belongsToMany(Announcement::class, 'announcement_user')->withTimestamps()->withPivot('count');
    }
    public function devices()
    {
        return $this->hasMany(Device::class, 'user_id');
    }

    public function personalInformation()
    {
        return $this->hasOne(PersonalInformation::class, 'user_id');
    }

    public function medicalInformation()
    {
        return $this->hasOne(MedicalInformation::class, 'user_id');
    }

    public function lifestyleInformation()
    {
        return $this->hasOne(LifestyleInformation::class, 'user_id');
    }

    public function gptMessages()
    {
        return $this->hasMany(GPTMessage::class, 'user_id');
    }


    public function brandProfile()
    {
        return $this->profiles->where('role.name',  'brand_operator')->first();
    }

    public function userProfile()
    {
        return $this->profiles->where('role.name', 'user')->first();
    }

    public function doctorProfile()
    {
        return $this->profiles->where('role.name', 'doctor')->first();
    }

    public function adminProfile()
    {
        return $this->profiles->where('role.name', 'admin')->first();
    }

    //SECTION - Attributes -------------------------------------------------------------------------------------------------------
    public function is_phone_verified()
    {
        return $this->phone_number_verified_at != null ? true : false;
    }

    public function is_id_verified()
    {
        return $this->citizen != null ? true : false;
    }

    public function getProfileTypeAttribute()
    {
        $role = $this->default_profile->role;
        return $role?->name ? $role?->name : 'guest';
    }

    public function getProfileIdAttribute()
    {
        return $this->default_profile->id;
    }

    public function getAddressAttribute()
    {
        return $this->region ? $this->region->translate(getLangFromHeader())?->text . ', ' . $this->region->city->translate(getLangFromHeader())?->text . ', ' . $this->region?->city->province->translate(getLangFromHeader())?->text : null;
    }

    public function getDefaultProfileAttribute()
    {
        $roles_priotity = ['admin', 'doctor', 'brand_operator', 'user'];
        $profiles = $this->profiles->sortByDesc('created_at');
        foreach ($roles_priotity as $role) {
            $profile = $profiles->where('role.name', $role)->first();
            if ($profile) {
                return $profile;
            }
        }
    }

    public function getProfileByRoleNameAttribute($role)
    {
        return $this->profiles->where('role.name', $role)->first();
    }


    //SECTION - Scopes -------------------------------------------------------------------------------------------------------
    public function scopeSearch($query, $value)
    {
        if (isset($value)) {
            return $query->where('name', 'like', "%$value%")
                ->orWhere('phone_number', 'like', "%$value%");
        }
        return $query;
    }
    public function scopeWhereStatusType($query, $shape)
    {

        return $query->whereHas('profiles', function ($query) use ($shape) {
            $query->whereHas('role', function ($query) {
                $query->where('name', 'user');
            });
            switch ($shape['value']) {
                case ProfileStatusEnum::ACTIVE:
                    return $query->suspended(false);
                case ProfileStatusEnum::BLOCKED || ProfileStatusEnum::PENDING || ProfileStatusEnum::REJECTED:
                    return $query->suspended($shape);
                default:
                    abort(400, 'value must be (ACTIVE,BLOCKED,PENDING,REJECTED)');
                    // return $query->suspended(null);
            }
            // $query->suspended($shape);
        });
    }


    public function hasRole($name)
    {
        return   Role::where('name', $name)->first()?->name;
    }
}
