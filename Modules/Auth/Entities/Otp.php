<?php

namespace Modules\Auth\Entities;

use App\Models\BaseModel;


class Otp  extends BaseModel
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'value',
        'type'
    ];
    
    //SECTION - Relations -------------------------------------------------------------------------------------------------------
    public function user(){
        $this->belongsTo(User::class, 'user_id');
    }
}
