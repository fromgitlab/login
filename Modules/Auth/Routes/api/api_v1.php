<?php

use Modules\Auth\Http\Controllers\API\V1\ProfileAuthController;
use Modules\Auth\Http\Controllers\API\V1\CitizenController;
use Modules\Auth\Http\Controllers\API\V1\AuthController;
use Modules\Auth\Http\Controllers\API\V1\OtpController;
use Modules\Auth\Http\Controllers\API\V1\RoleController;
use Modules\Auth\Http\Controllers\API\V1\UserController;
use Illuminate\Support\Facades\Route;


Route::post('/auth/register', [AuthController::class, 'register']);

//login to your account
Route::post('/auth/login/web', [AuthController::class, 'loginWeb'])->middleware('access-limit:1,401,3');

Route::post('/auth/login/mobile', [AuthController::class, 'loginMobile'])->middleware('access-limit:1,401,3');

//register a device to the database
Route::post('/device/register', [AuthController::class, 'registerDevice']);

//forget a password for unauthenticated user
Route::post('/forget-password', [UserController::class, 'forgetPassword']);

//ANCHOR - OTP  
//Send otp to the phone numbers
Route::post('/auth/send-otp', [OtpController::class, 'sendOTP'])->middleware('access-limit:1,200,1');

//TODO -  Check Throttle here
Route::group(['middleware' => ['throttle:30,1']], function () {
    //check the phone number if it exists before
    Route::post('/auth/login-check-phone', [AuthController::class, 'checkPhoneNumber']);
    // Generate a qr code for the given device_id to authenticate via notification (Notification Authentication)
    //REVIEW - Not used - to be reviewd in the future
    // Route::post('device/generate-qr-code', [AuthController::class, 'generateQrCode']);
});



Route::group(['middleware' => 'auth:sanctum'], function () {

    Route::get('auth/get-me'          , [AuthController::class, 'getMe']);

    Route::get('/roles', [RoleController::class, 'index']);

    Route::get('/users', [UserController::class, 'index']);
    Route::get('/users/{user}', [UserController::class, 'show']);

    //change a user password
    Route::put('/users/{user}/admin-change-password', [UserController::class, 'adminChangePassword']);
    //promote a user to and admin
    // Route::post('/users/{user}/promote-admin', [UserController::class, 'promoteAdmin']);
    //change my password as an authenticated user
    Route::put('/auth/change-password', [UserController::class, 'authChangePassword']);

    //ANCHOR - Change Phone Number end-point
    //change phone number endpoint
    // Route::put('auth/phone-number', [AuthController::class, 'changePhoneNumber']);

    Route::post('auth/validate-phone-number', [AuthController::class, 'validatePhoneNumber']);
    //validate id card for the authed user
    Route::post('auth/validate-id', [AuthController::class, 'validateId']);
    //logout from my account
    Route::post('auth/logout', [AuthController::class, 'logout']);

    // Send access token via notification to the given device qr code (Notification Authentication)
    // this feature currently suspended
    // Route::post('auth/authenticate-device', [AuthController::class, 'authenticateDevice']);
    Route::post('/validate-qr-code/citizens', [AuthController::class, 'citizenValidateQrCode']);

    //-----------------------------------------DEPRECATED-----------------------------------------
    // Route::post('/auth/users/validate-qr-code', [AuthController::class, 'userValidateQrCode']);
    //--------------------------------------------------------------------------------------------
    //Add new citizen (Doctors and admin)
    Route::post('/citizens', [CitizenController::class, 'addCitizen']);
    //login to a specific profile through the profile type (Switch profile)

    //mount to get a mixed data on splash screens
    Route::get('/mount', [AuthController::class, 'mount']);
    Route::post('/auth/login/profiles', [ProfileAuthController::class, 'login']);
});
