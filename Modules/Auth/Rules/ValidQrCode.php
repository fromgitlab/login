<?php

namespace Modules\Auth\Rules;

use App\Http\Encryption;
use Illuminate\Contracts\Validation\Rule;

class ValidQrCode implements Rule
{
    public function passes($attribute, $value)
    {
        $encryptor = new Encryption();

        try {
            $data = $encryptor->decrypt($value);
            return !empty($data);
        } catch (\Exception $e) {
            return false;
        }
    }

    public function message()
    {
        return 'The QR code is not valid.';
    }
}
