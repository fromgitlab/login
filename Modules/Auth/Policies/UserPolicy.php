<?php

namespace Modules\Auth\Policies;

use App\Policies\BasePolicy;
use Modules\Profile\Entities\Profile;

class UserPolicy
{

    public static function browse($user)
    {
        return true;
    }

    public static function read($user)
    {
        return true;
    }

    public static function add($user)
    {
        return true;
    }

    public static function edit($user)
    {
        //TODO - not in this way
        $allowed = optional($user)->id == request()->route('users')->id;

        return $allowed;
    }

    public static function delete($user)
    {
        $allowed = optional($user)->id == request()->route('users')->id;
        return $allowed;
    }

    public static function changePassword(Profile $profile)
    {
        return $profile->role->name == 'admin';
    }

    public static function promoteToAdmin(Profile $profile)
    {
        return $profile->role->name == 'admin';
    }
}
