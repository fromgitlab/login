<?php

namespace Modules\Auth\Policies;

use Modules\Auth\Entities\Citizen;
use Illuminate\Auth\Access\HandlesAuthorization;
use Modules\Auth\Entities\User;
use Modules\Profile\Entities\Profile;

class CitizenPolicy
{
    use HandlesAuthorization;

    public static function browse(User $user)
    {
        return true;
    }

    public static function read(User $user, Citizen $citizen)
    {
        return true;
    }

    public static function add(Profile $profile)
    {
        return $profile->role->name == 'doctor';
    }

    public static function edit(User $user, Citizen $citizen)
    {
        return true;
    }

    public static function delete(User $user, Citizen $citizen)
    {
        return true;
    }
}
