<?php

namespace Modules\Auth\Policies;

use Modules\Auth\Entities\Otp;
use Illuminate\Auth\Access\HandlesAuthorization;

class OtpPolicy
{
 use HandlesAuthorization;

 public function browse(User $user)
 {
 return $user->hasPermission('Auth.Otp.browse');
 }

 public function read(User $user, Otp $Otp)
 {
 return $user->hasPermission('Auth.Otp.read');
 }

 public function add(User $user)
 {
 return $user->hasPermission('Auth.Otp.add');
 }

 public function edit(User $user, Otp $Otp)
 {
 return $user->hasPermission('Auth.Otp.edit');
 }

 public function delete(User $user, Otp $Otp)
 {
 return $user->hasPermission('Auth.Otp.delete');
 }
}