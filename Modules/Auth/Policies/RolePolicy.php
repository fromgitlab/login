<?php

namespace Modules\Auth\Policies;

use Modules\Auth\Entities\Role;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
 use HandlesAuthorization;

 public function browse(User $user)
 {
 return $user->hasPermission('Auth.Role.browse');
 }

 public function read(User $user, Role $Role)
 {
 return $user->hasPermission('Auth.Role.read');
 }

 public function add(User $user)
 {
 return $user->hasPermission('Auth.Role.add');
 }

 public function edit(User $user, Role $Role)
 {
 return $user->hasPermission('Auth.Role.edit');
 }

 public function delete(User $user, Role $Role)
 {
 return $user->hasPermission('Auth.Role.delete');
 }
}