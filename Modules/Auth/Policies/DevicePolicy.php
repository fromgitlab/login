<?php

namespace Modules\Auth\Policies;

use Modules\Auth\Entities\Device;
use Illuminate\Auth\Access\HandlesAuthorization;

class DevicePolicy
{
 use HandlesAuthorization;

 public function browse(User $user)
 {
 return $user->hasPermission('Auth.Device.browse');
 }

 public function read(User $user, Device $Device)
 {
 return $user->hasPermission('Auth.Device.read');
 }

 public function add(User $user)
 {
 return $user->hasPermission('Auth.Device.add');
 }

 public function edit(User $user, Device $Device)
 {
 return $user->hasPermission('Auth.Device.edit');
 }

 public function delete(User $user, Device $Device)
 {
 return $user->hasPermission('Auth.Device.delete');
 }
}