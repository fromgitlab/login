<?php

namespace Modules\Auth\Actions;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Modules\Auth\Entities\User;

/**
 * Class UserChangePasswordAction
 * Change the password of a user 
 * @package Modules\Auth\Actions
 */
 class UserChangePasswordAction{

    public static function execute(User $user, $password){
        $user->password = Hash::make($password);

        $user->save();
    }
 }
