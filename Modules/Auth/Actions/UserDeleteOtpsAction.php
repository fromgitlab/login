<?php

namespace Modules\Auth\Actions;

use Modules\Auth\Entities\User;

/**
 * Class UserDeleteOtpsAction
 * Delete all otps for a user from a specific type
 * @package Modules\Auth\Actions
 */
class UserDeleteOtpsAction{

    public static function execute(User $user, $type_id)
    {
        $user->otps()->where('type_id', $type_id)->delete();
    }
}
