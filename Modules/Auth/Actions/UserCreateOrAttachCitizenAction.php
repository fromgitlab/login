<?php

namespace Modules\Auth\Actions;

use Modules\Auth\Contracts\ICreateForUser;
use Modules\Auth\Entities\Citizen;
use Modules\Auth\Entities\User;

/**
 * Class UserCreateOrAttachCitizenAction
 * Creates or attach a citizen in the system related to a user.
 * @package Modules\Auth\Actions
 */
class UserCreateOrAttachCitizenAction implements ICreateForUser{

    public static function execute(User $user, $request): ?User
    {
        $citizen = Citizen::firstOrCreate(
            [
                "national_number"=>$request->national_number,
            ],
            $request->validated()
        );
        $user->citizen()->associate($citizen);
        $user->region()->associate($request->region_id);
        $user->birth_date = $request->birth_date;
        $user->save();
        return $user;
    }
}
