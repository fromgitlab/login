<?php

namespace Modules\Auth\Actions;

use Modules\Auth\Contracts\ICreateToken;
use Modules\Auth\Entities\User;

/**
 * Class DefaultProfileCreateTokenAction
 * Creates an authentication token for the profile.
 * @package Modules\Auth\Actions
 */
class DefaultProfileCreateTokenAction implements ICreateToken{

    public static function execute(User $user): ?string
    {
        //TODO - cahnge name of this action
        return  $user->default_profile->createToken('token')->plainTextToken;
    }
}
