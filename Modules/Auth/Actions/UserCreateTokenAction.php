<?php 

namespace Modules\Auth\Actions; 


namespace Modules\Auth\Actions;

use Modules\Auth\Contracts\ICreateToken;
use Modules\Auth\Entities\User;

/**
 * Class UserCreateTokenAction
 * Creates an authentication token for the user.
 * @package Modules\Auth\Actions
 */
class UserCreateTokenAction implements ICreateToken{

    public static function execute(User $user): ?string
    {
        return  $user->createToken('token')->plainTextToken;
    }
}


?>