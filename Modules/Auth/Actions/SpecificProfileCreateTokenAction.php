<?php

namespace Modules\Auth\Actions;

use Auth;
use Modules\Auth\Contracts\ICreateToken;
use Modules\Auth\Entities\User;
use Str;

/**
 * Class DefaultProfileCreateTokenAction
 * Creates an authentication token for the profile.
 * @package Modules\Auth\Actions
 */
class SpecificProfileCreateTokenAction implements ICreateToken{

    public static function execute(User $user, $profile_type = null): ?string
    {
        $user->load(['profiles.role']);
        if($profile_type){
            $profile = $user?->profiles->where('role.name', Str::lower($profile_type))?->first();
        }else{
            $profile = $user?->default_profile;
        }
        //TODO to be refactored and places in other place
        setAuthProfile($profile);
        Auth::setUser($user);
        //TODO - cahnge name of this action
        return $profile->createToken('token')->plainTextToken;
    }
}
