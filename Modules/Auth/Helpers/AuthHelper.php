<?php

namespace Modules\Auth\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Modules\Auth\Actions\UserChangePasswordAction;
use Modules\Auth\Entities\User;
use Modules\Category\Entities\Avatar;
use App\Helpers\Notification;

class AuthHelper
{

    public static function defaultProfileCreateToken($user): string
    {
        return  $user->default_profile->createToken('token')->plainTextToken;
    }

    public static function specificProfileCreateToken(User $user, $profile_type): ?string
    {
        $user->load(['profiles.role']);

        $profile = $user?->profiles->where('role.name', Str::lower($profile_type))?->first();
        if (!$profile)
            abort(404, "profile not found");

        setAuthProfile($profile);
        Auth::setUser($user);
        return $profile->createToken('token')->plainTextToken;
    }


    public static function userVerifiedPhone($user)
    {
        $user->phone_number_verified_at = now();
        $user->save();
        self::userDeleteOtps($user, 'otp_phone_number');
    }

    public static function userDeleteOtps(User $user, $type)
    {
        $user->otps()->where('type', $type)->delete();
    }

    public static function avatarChooserAction($gender)
    {
        $birth_date = auth()->user()->birth_date;
        $age = date_diff(date_create($birth_date), date_create('now'))->y;
        $range_string = self::rangeAgeAction($age);
        $avatar_image_name =  $range_string . '-' . ucfirst(strtolower($gender)) . '.png';
        $avatar = Avatar::where('name', $avatar_image_name)->first();
        return $avatar;
    }

    public static function rangeAgeAction($age)
    {
        if ($age >= 0 && $age < 18) {
            return "0-18";
        } elseif ($age >= 18 && $age < 30) {
            return "18-30";
        } elseif ($age >= 30 && $age < 60) {
            return "30-60";
        } elseif ($age >= 60 && $age <= 100) {
            return "60-100";
        } else {
            return "60-100";
        }
    }

    public static function userChangePassword($user, $new_password)
    {
        $user->password = Hash::make($new_password);

        $user->save();
    }
    public static function userLogoutAllDevices($user)
    {
        $user->tokens()->delete();
    }

    public static function userCheckOtp($user, $type, $value)
    {
        $user_otp = $user->otps()->where('type', $type)->first();
        if (!$user_otp)  abort(400, 'Wrong Otp');

        $status = false;
        if ($type = 'otp_phone_number') {
            if (!($user_otp->created_at->diffInMinutes(now()) > (config('otp.expire')))) {
                if ($user_otp->value == $value) {
                    $status = true;
                }
            } else {
                $user_otp->delete();
            }
        }
        return  $status;
    }

    public static function getUserOrCreate(string $phone, string $email, string $name)
    {
        $user =  DB::table('users')->where('email', $email)->where('phone_number', $phone)->first();
        if ($user == null) {
            $user_role =  DB::table('roles')->where('name', 'user')->first();
            if ($user_role == null) {
                $user_role = DB::table('roles')->insert([
                    'name' => 'user',
                ]);
                $user_role =  DB::table('roles')->where('name', 'user')->first();

                DB::table('role_translations')->insert([
                    'locale' => 'en',
                    'text' => 'Normal User',
                    'role_id' => $user_role->id,
                ]);
                DB::table('role_translations')->insert([
                    'locale' => 'ar',
                    'text' => 'مستخدم',
                    'role_id' => $user_role->id,

                ]);
                $user_role =  DB::table('roles')->where('name', 'user')->first();
            }

            $user = DB::table('users')->insert([
                'name' => $name,
                'phone_number' => $phone,
                'role_id' => $user_role->id,
                'email' => $email,
                'avatar' => 'avatars/30-60-Male.png',
                'phone_number_verified_at' => now(),
                'password' => '$2a$12$NtaOVNCsx1Lt06ozIMYj2uaaB.Jqjy8pS3JsFT/b6NMXbptN2e6km',
                'birth_date' => '1980-01-01',
                'gender' => 'Male',
            ]);
        }
        return $user = DB::table('users')->where('email', $email)->where('phone_number', $phone)->first();
    }
}
