<?php

namespace Modules\Auth\HelperClasses;

use Modules\Auth\Entities\User;
use Modules\Auth\Http\Resources\Users\UserLoginResource;
use Modules\Auth\Traits\HelperMethods;

/**
 * Class AuthUser
 * Helper Class For the Authed User to Add Attributes with User
 * @package Modules\Auth\HelperClasses
 */
class AuthUser{

    use HelperMethods;

    private ?UserLoginResource $user_resource;
    private ?User $user;
    private bool $can_continue;
    private $reason;
    private $status;
    private $token;



    public function __construct($user,$user_resource, $can_continue, $reason = null, $status , $token)
    {
        $this->user_resource = $user_resource;
        $this->user = $user;
        $this->can_continue = $can_continue;
        $this->reason = $reason;
        $this->status = $status;
        $this->token = $token;
    }

}
