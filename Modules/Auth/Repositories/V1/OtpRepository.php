<?php

namespace Modules\Auth\Repositories\V1;

use App\Helpers\Otp;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Modules\Auth\Entities\User;
use Modules\Auth\Interfaces\V1\OtpRepositoryInterface;

class OtpRepository  implements OtpRepositoryInterface
{
    public function sendOTP($request)
    {
        $user = Auth::check() ? Auth::user() : User::where('phone_number', $request->phone_number)->first();
        if (!$user)
        {
            abort(404, "Phone Number Not Found!");
        }
        DB::beginTransaction();
        Otp::createAndSend($user);
        DB::commit();

        return;
    }
}
