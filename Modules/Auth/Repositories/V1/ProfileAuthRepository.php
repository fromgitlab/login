<?php

namespace Modules\Auth\Repositories\V1;

use Illuminate\Support\Facades\Auth;
use Modules\Auth\Enums\RoleEnum;
use Modules\Auth\Interfaces\V1\ProfileAuthRepositoryInterface;

class ProfileAuthRepository implements ProfileAuthRepositoryInterface
{
    public function login($request)
    {
        $profile = Auth::user()->load('profiles.role')->profiles->where('role.name', RoleEnum::getCase($request->profile_type)->value)->first();
        return  ($profile) ? $profile->createToken('token')->plainTextToken : null;
    }
}
