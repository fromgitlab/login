<?php

namespace Modules\Auth\Repositories\V1;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Helpers\Otp;
use Modules\Auth\Actions\UserCreateOrAttachCitizenAction;
use Modules\Auth\Entities\Citizen;
use Modules\Auth\Entities\Device;
use Modules\Auth\Entities\Role;
use Modules\Auth\Entities\User;
use Modules\Auth\Enums\RegisterRuleEnum;
use Modules\Auth\HelperClasses\AuthUser;
use Modules\Auth\Helpers\AuthHelper;
use Modules\Auth\Http\Requests\Auth\DeviceRegisterRequest;
use Modules\Auth\Http\Resources\Auth\MountResource;
use Modules\Auth\Http\Resources\Auth\ScanCitizenInfoResource;
use Modules\Auth\Http\Resources\Common\Catalog\OptionResource;
use Modules\Auth\Http\Resources\Common\Category\UnitResource;
use Modules\Auth\Http\Resources\Users\GetMeResource;
use Modules\Auth\Http\Resources\Users\UserLoginResource;
use Modules\Auth\Http\Resources\Users\UserRegisterDeviceResource;
use Modules\Auth\Http\Resources\Users\UserValidateIdResource;
use Modules\Auth\Interfaces\V1\AuthRepositoryInterface;
use Modules\Catalog\Entities\Item;
use Modules\Catalog\Entities\Option;
use Modules\Category\Entities\Unit;
use Modules\Profile\Entities\Profile;
use Modules\Profile\Enums\ProfileStatusEnum;
use Modules\Setting\Entities\Setting;




class AuthRepository  implements AuthRepositoryInterface
{
    public function mount(): JsonResource
    {
        $version_settings = Setting::whereIn('key', [
            'app.android_min_version',
            'app.android_current_version',
            'app.android_current_version_url',
            'app.ios_min_version',
            'app.ios_current_version',
            'app.ios_current_version_url',
        ])->get();

        $version_data = [
            'android_min_version' => $version_settings->where('key', 'android_min_version')->first(),
            'android_current_version' => $version_settings->where('key', 'android_current_version')->first(),
            'android_current_version_url' => $version_settings->where('key', 'android_current_version_url')->first(),
            'ios_min_version' => $version_settings->where('key', 'ios_min_version')->first(),
            'ios_current_version' => $version_settings->where('key', 'ios_current_version')->first(),
            'ios_current_version_url' => $version_settings->where('key', 'ios_current_version_url')->first(),
        ];

        $item_version = Item::orderBy('updated_at', 'desc')->first()?->updated_at->format('Y-m-d H:i:s');

        $units_data = UnitResource::collection(Unit::all());

        $user = Auth::guard('sanctum')->user();
        $roles_list = $user->roles->pluck('name');
        $is_admin_or_brand = $roles_list->contains(function ($value) {
            return $value == 'admin' || $value == 'brand_operator';
        });


        if ($is_admin_or_brand == true) {
            $options = Option::with(['translations', 'feature.translations'])->get();
            $package_forms =  OptionResource::collection($options->where('feature.key', 'package_form'));
            $dosage_forms =  OptionResource::collection($options->where('feature.key', 'dosage_form'));
        } else {
            $package_forms = null;
            $dosage_forms = null;
        }


        return new MountResource($version_data, $item_version, $units_data, $package_forms, $dosage_forms);
    }

    public function registerDevice(DeviceRegisterRequest $request)
    {
        $device_data = $request->validated();

        $auth_profile_id  = Auth::guard('sanctum')->user() ? authProfile()->id : null;

        $device = Device::where('device_id', $device_data['device_id'])->first();

        $profile_id = ($auth_profile_id) ? $auth_profile_id : $device?->profile_id;

        Device::updateOrCreate(
            [
                'device_id' => $device_data['device_id'],
            ],
            [
                'device_id'     => $device_data['device_id'],
                'fcm_token'     => isset($device_data['fcm_token'])     ? $device_data['fcm_token'] : null,
                'locale'        => isset($device_data['locale'])        ? $device_data['locale'] : "en",
                'last_login_at' => $device_data['last_login_at'],
                'profile_id' =>    $profile_id,
            ]
        );

        $user = Auth::guard('sanctum')->user();

        return ($user != null) ? new UserRegisterDeviceResource($user) : null;
    }
    public function getMe()
    {
        return new GetMeResource(Auth::user()) ;
    }

    public function register($request)
    {
        $data = $request->validated();
        $data['password'] = Hash::make($data['password']);

        DB::beginTransaction();
        $user = User::create($data);

        $user->roles()->syncWithoutDetaching(Role::where('name', 'user')->first()?->id);

        if ($request->national_number) {
            $citizen = Citizen::firstOrCreate(
                [
                    "national_number" => $data['national_number'],
                ],
                ["national_number" => $data['national_number'], 'id_card_data' => $data['id_card_data']]
            );
            $user->citizen()->associate($citizen);
            $user->region()->associate($request->region_id);
            $user->birth_date = $request->birth_date;
            $user->save();
        }

        Otp::createAndSend($user);
        DB::commit();

        return $user;
    }

    public function loginWeb($request)
    {
        $user = $request->user; // the user merged in form request after validation

        $token = AuthHelper::defaultProfileCreateToken($user);

        if (!$user->is_phone_verified()) {
            Otp::createAndSend($user);
            return new AuthUser(null, null, false, __("auth.login.phone_is_not_verified"), 434, $token); //Phone Number is Not Verified
        }
        return new AuthUser($user, new UserLoginResource($user), true, __("auth.login.success"), 200, $token); //login successfully

    }
    public function loginMobile($request)
    {
        $user = $request->user; // the user merged in form request after validation

        if ($request->profile_type == RegisterRuleEnum::USER->value) {
            $token = AuthHelper::specificProfileCreateToken($user, $request->profile_type);
        } else {
            $token = AuthHelper::defaultProfileCreateToken($user);
        }

        if (!$user->is_phone_verified()) {
            Otp::createAndSend($user);
            return new AuthUser(null, null, false, __("auth.login.phone_is_not_verified"), 434, $token); //Phone Number is Not Verified
        }
        return new AuthUser($user, null, true, __("auth.login.success"), 200, $token); //login successfully
    }

    public function checkPhoneNumber($request)
    {
        return  User::where('phone_number', $request->phone_number)->first();
    }

    public function validatePhoneNumber($request)
    {
        $user = Auth::user();

        $status = AuthHelper::userCheckOtp($user, 'otp_phone_number', $request->otp);

        return ["status" => $status, "user" => $user];
    }

    public function validateId($request): JsonResource
    {
        $user = Auth::user();
        UserCreateOrAttachCitizenAction::execute($user, $request);
        $user->name = $request->name;
        $user->gender = $request->gender;
        $user->avatar = $request->avatar;
        $user->save();
        return new UserValidateIdResource($user);
    }

    // public function authenticateDevice($request)
    // {
    // }

    public function citizenValidateQrCode($request): JsonResource
    {
        $user = $user = User::find($request->user_id);
        $citizen = $user->citizen;
        return new ScanCitizenInfoResource($citizen);
    }
    public function logout()
    {
        authProfile()?->currentAccessToken()?->delete();
    }
}
