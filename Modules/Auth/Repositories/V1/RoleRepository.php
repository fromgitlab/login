<?php

namespace Modules\Auth\Repositories\V1;

use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Auth\Entities\Role;
use Modules\Auth\Http\Resources\Common\RoleResource;
use Modules\Auth\Interfaces\V1\RoleRepositoryInterface;
use Spatie\QueryBuilder\QueryBuilder;

class RoleRepository implements RoleRepositoryInterface
{

    public function index($request):JsonResource
    {
        return RoleResource::collection(QueryBuilder::for(Role::class)
            ->where('id', '!=', Role::where('name', 'admin')->first()->id)
            ->where('id', '!=', Role::where('name', 'user')->first()->id)
            ->paginate($request->items_per_page));
    }
}
