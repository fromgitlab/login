<?php

namespace Modules\Auth\Repositories\V1;

use Modules\Auth\Entities\Citizen;
use Modules\Auth\Http\Resources\Auth\CitizenResource;
use Modules\Auth\Interfaces\V1\CitizenRepositoryInterface;

class CitizenRepository  implements CitizenRepositoryInterface
{
    public function addCitizen( $request)
    {   
        $citizen = Citizen::firstOrCreate(
            [
                "national_number" => $request->national_number,
            ],
            ['national_number' => $request->national_number, 'id_card_data' => $request->id_card_data]
        );
        return new CitizenResource($citizen);
    }
}
