<?php

namespace Modules\Auth\Repositories\V1;

use App\Filters\BuilderFilter;
use App\Filters\RangeFilter;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Modules\Auth\Entities\Role;
use Modules\Auth\Entities\User;
use Modules\Auth\Helpers\AuthHelper;
use Modules\Auth\Http\Resources\Users\UserIndexResource;
use Modules\Auth\Http\Resources\Users\UserShowResource;
use Modules\Auth\Interfaces\V1\UserRepositoryInterface;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class UserRepository implements UserRepositoryInterface
{
    public function index($request): JsonResource
    {
        return UserIndexResource::collection(QueryBuilder::for(User::class)
            ->allowedFilters([
                AllowedFilter::custom('name', new BuilderFilter()),
                AllowedFilter::custom('registered_between', new RangeFilter(), 'created_at'),
                AllowedFilter::scope('status', 'whereStatusType')
            ])
            ->allowedSorts([
                'name',
                'created_at'
            ])
            ->search($request->search)
            ->with([
                'region.translations',
                'region.city.translations',
                'region.city.province.translations',
                'profiles.suspensions',
                'profiles.profileInfo',
                'profiles.role',
                'roles',
            ])
            ->paginate($request->items_per_page));
    }

    public function show(User $user): JsonResource
    {
        $user->load(
            'profiles.contacts',
            'profiles.workplaces.workplaceType.translations',
            'profiles.workplaces.translations',
            'profiles.suspensions',
            'profiles.role',
            // 'profiles.profileInfo.specialization',
            // 'profiles.profileInfo.subSpecializations',
            // 'profiles.profileInfo.files',
            'region.translations',
            'region.city.province.translations',

        );
        return new UserShowResource($user);
    }

    public function adminChangePassword($request, User $user)
    {
        if ($user->roles->contains('name', 'admin')) {
            abort(400, __('user.change_admin_password'));
        }

        AuthHelper::userChangePassword($user, $request->password);
        AuthHelper::userLogoutAllDevices($user);
        AuthHelper::userDeleteOtps($user, 'otp_phone_number');
    }
    public function authChangePassword($request)
    {
        $user = Auth::user();
        if ($user->roles->contains('name', 'admin')) {
            abort(400, __('user.change_admin_password'));
        }

        if (Hash::check($request->old_password, $user->password)) {
            AuthHelper::userChangePassword($user, $request->password);
        } else {
            abort(400, __('user.authChangePassword.fail'));
        }
    }
    public function forgetPassword($request)
    {
        $user = User::where('phone_number', $request->phone_number)->first();
        if ($user == null) {
            abort(400, __('user.forgetPassword.fail'));
        }
        if ($user->roles->contains('name', 'admin')) {
            abort(400, __('user.change_admin_password'));
        }
        if (AuthHelper::userCheckOtp($user,'otp_phone_number', $request->otp)) {
            AuthHelper::userChangePassword($user,  $request->password);
        } else {
            abort(400, __('user.forgetPassword.fail'));
        }
    }
    public function promoteAdmin(User $user)
    {
        $user->roles()->attach(Role::where('name', 'admin')->first()->id);
    }
}
