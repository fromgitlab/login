<?php

namespace Modules\Auth\Interfaces\V1;

use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Auth\Http\Requests\Auth\DeviceRegisterRequest;

interface AuthRepositoryInterface
{

    public function mount():JsonResource;
    public function registerDevice(DeviceRegisterRequest $request);
    public function getMe();
    public function register($request);
    // public  function login($request);
    public function loginWeb($request);
    public function loginMobile($request);

    public function checkPhoneNumber($request);
    public function validatePhoneNumber($request);
    public function validateId($request):JsonResource;
    // public function authenticateDevice($request);
 
    public function citizenValidateQrCode($request):JsonResource;
    public function logout();

}
