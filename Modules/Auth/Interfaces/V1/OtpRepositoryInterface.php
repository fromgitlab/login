<?php

namespace Modules\Auth\Interfaces\V1;

interface OtpRepositoryInterface
{
    public function sendOTP($request);
}
