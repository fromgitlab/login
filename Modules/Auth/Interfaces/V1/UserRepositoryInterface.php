<?php

namespace Modules\Auth\Interfaces\V1;

use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Auth\Entities\User;

interface UserRepositoryInterface
{

    public function index($request):JsonResource;

    public function show(User $user):JsonResource;

    public function adminChangePassword($request, User $user);

    public function authChangePassword($request);

    public function forgetPassword($request);

    public function promoteAdmin(User $user);

}
