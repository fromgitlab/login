<?php

namespace Modules\Auth\Interfaces\V1;

interface CitizenRepositoryInterface
{
    public function addCitizen( $request);
}
