<?php

namespace Modules\Auth\Interfaces\V1;

use Illuminate\Http\Resources\Json\JsonResource;

interface RoleRepositoryInterface
{
  public function index($request):JsonResource;
}
