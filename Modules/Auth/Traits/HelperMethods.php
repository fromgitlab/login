<?php

namespace Modules\Auth\Traits;

trait HelperMethods
{
    public function __get(string $name)
    {
        return $this->$name ?? null;
    }

    public function __set(string $name, $value)
    {
        $this->$name = $value;
    }

    public function __isset(string $name)
    {
        return isset($this->$name);
    }
}
