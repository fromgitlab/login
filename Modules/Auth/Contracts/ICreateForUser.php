<?php

namespace Modules\Auth\Contracts;

use Modules\Auth\Entities\User;

/**
 * Interface ICreateForUser
 * **ABANDONED INTERFACE, FUNCTIONALITY IS USED DIRECTLY FROM THE CHILD CLASS
 * @package Modules\Auth\Contracts
 */

interface ICreateForUser
{
    public static function execute(User $user, $data): ?User;
}
