<?php


namespace Modules\Auth\Contracts;

use Modules\Auth\Entities\User;
use Modules\Auth\HelperClasses\AuthUser;

/**
 * Interface IAuthFetcher
 * **ABANDONED INTERFACE, FUNCTIONALITY IS USED DIRECTLY FROM THE CHILD CLASS
 * @package Modules\Auth\Contracts
 */
interface IAuthFetcher
{
    public static function execute($data) : ?AuthUser;
}
