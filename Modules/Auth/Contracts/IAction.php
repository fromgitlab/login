<?php

namespace Modules\Auth\Contracts;

/**
 * Interface IAction
 * **ABANDONED INTERFACE, FUNCTIONALITY IS USED DIRECTLY FROM THE CHILD CLASS
 * @package Modules\Auth\Contracts
 */
interface IAction
{
    public static function execute($data);

}
