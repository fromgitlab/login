<?php

namespace Modules\Auth\Contracts;

use Modules\Auth\Entities\User;

/**
 * Interface ICreateToken
 * **ABANDONED INTERFACE, FUNCTIONALITY IS USED DIRECTLY FROM THE CHILD CLASS
 * @package Modules\Auth\Contracts
 */
interface ICreateToken
{
    public static function execute(User $user) : ?string;
}
