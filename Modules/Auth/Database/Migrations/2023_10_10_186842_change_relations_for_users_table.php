<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Schemas\CustomSchema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sessions', function (Blueprint $table) {

            if (CustomSchema::hasForeign('sessions', ['user_id'])) {
                $table->dropForeign(['user_id']);
            }
            $table->foreign('user_id')->references('id')->on('users')->cascadeOnDelete()->cascadeOnUpdate();
        });


        Schema::table('notifications', function (Blueprint $table) {
            if (CustomSchema::hasForeign('notifications', ['user_id'])) {
                $table->dropForeign(['user_id']);
            }
            $table->foreign('user_id')->references('id')->on('users')->cascadeOnDelete()->cascadeOnUpdate();
        });
        Schema::table('otps', function (Blueprint $table) {
            if (CustomSchema::hasForeign('otps', ['user_id'])) {
                $table->dropForeign(['user_id']);
            }
            $table->foreign('user_id')->references('id')->on('users')->cascadeOnDelete()->cascadeOnUpdate();
        });


        // Schema::table('devices', function (Blueprint $table) {
        //     if (CustomSchema::hasForeign('devices', ['profile_id'])) {
        //         $table->dropForeign(['profile_id']);
        //     }
        //     $table->foreign('profile_id')->references('id')->on('profiles')->cascadeOnDelete()->cascadeOnUpdate();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sessions', function (Blueprint $table) {
            if (CustomSchema::hasForeign('sessions', ['user_id'])) {
                $table->dropForeign(['user_id']);
            }
            $table->foreign('user_id')->references('id')->on('users')->onDelete('restrict')->onUpdate('restrict');
        });


        Schema::table('notifications', function (Blueprint $table) {
            if (CustomSchema::hasForeign('notifications', ['user_id'])) {
                $table->dropForeign(['user_id']);
            }
            $table->foreign('user_id')->references('id')->on('users')->onDelete('restrict')->onUpdate('restrict');
        });

        Schema::table('otps', function (Blueprint $table) {
            if (CustomSchema::hasForeign('otps', ['user_id'])) {
                $table->dropForeign(['user_id']);
            }
            $table->foreign('user_id')->references('id')->on('users')->onDelete('restrict')->onUpdate('restrict');
        });

        Schema::table('devices', function (Blueprint $table) {
            if (CustomSchema::hasForeign('devices', ['profile_id'])) {
                $table->dropForeign(['profile_id']);
            }
            $table->foreign('profile_id')->references('id')->on('profiles')->onDelete('restrict')->onUpdate('restrict');
        });
    }
};
