<?php

use App\Schemas\CustomSchema;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('roles', function (Blueprint $table) {
            if (CustomSchema::hasForeign('roles', ['type_id'])) {
                $table->dropForeign(['type_id']);
            }
            $table->dropColumn('type_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('roles', function (Blueprint $table) {
            if (!Schema::hasColumn('roles', 'type_id')) {
                $table->foreignId("type_id")->nullable()
                    ->constrained()
                    ->cascadeOnDelete()->cascadeOnUpdate();
            }
        });
    }
};
