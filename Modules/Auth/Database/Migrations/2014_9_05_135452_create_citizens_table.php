<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('citizens', function (Blueprint $table) {
            $table->id();

            $table->string("national_number");
            $table->index(['national_number']);

            $table->text('id_card_data');
            $table->timestamps();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->foreignId("citizen_id")->nullable()
            ->constrained()
            ->cascadeOnDelete()->cascadeOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('citizens');
    }
};
