<?php

use App\Schemas\CustomSchema;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('otps', function (Blueprint $table) {
            if (Schema::hasColumn('otps', 'type_id')) {
                if (CustomSchema::hasForeign('otps', ['type_id'])) {
                    $table->dropForeign(['type_id']);
                }
                $table->dropColumn('type_id');
            }
            if (!Schema::hasColumn('otps', 'type')) {
                $table->string('type');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('otps', function (Blueprint $table) {

            if (Schema::hasColumn('otps', 'type')) {

                $table->dropColumn('type');
            }

            $table->foreignId("type_id")->nullable()
                ->constrained()
                ->cascadeOnDelete()
                ->cascadeOnUpdate();
        });
    }
};
