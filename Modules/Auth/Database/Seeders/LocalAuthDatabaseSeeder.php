<?php

namespace Modules\Auth\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Auth\Entities\Citizen;
use Modules\Auth\Entities\Role;
use Modules\Auth\Entities\User;
use Modules\Catalog\Entities\Brand;
use Modules\Category\Entities\Region;
// use Modules\Profile\Entities\Specialization;
// use Modules\Profile\Entities\SubSpecialization;

class LocalAuthDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
      
        $admin_role= Role::where('name','admin')->first();
        $user_role= Role::where('name','user')->first();
        // $brand_operator_role= Role::where('name','brand_operator')->first();
        // $doctor_role= Role::where('name','doctor')->first();
        // $first_region=Region::first();

        // $citizen = Citizen::firstOrCreate([
        //     'national_number' => '1234567892',
        //     'id_card_data' => '1234567892',
        // ], [
        //     'national_number' => '1234567892',
        //     'id_card_data' => '1234567892',
        // ]);

        $user = User::where('phone_number', '0966666666')->first();

        if ($user == null) {
            $user = User::create([
                'name' => 'Admin',
                'phone_number' => '0966666666',
                'role_id' => $admin_role->id,
                'email' => 'admin@gmial.com',
                // 'avatar' => 'avatars/30-60-Male.png',
                'phone_number_verified_at' => now(),
                'password' => '$2a$12$NtaOVNCsx1Lt06ozIMYj2uaaB.Jqjy8pS3JsFT/b6NMXbptN2e6km',
                'birth_date' => '1980-01-01',
                // 'region_id' => $first_region->id,
                'gender' => 'male',
                // 'citizen_id' => $citizen->id,
            ]);
        } else {
            $user->update([
                'name' => 'Admin',
                'phone_number' => '0966666666',
                'role_id' => $admin_role->id,
                'email' => 'admin@gmial.com',
                // 'avatar' => 'avatars/30-60-Male.png',
                'phone_number_verified_at' => now(),
                'password' => '$2a$12$NtaOVNCsx1Lt06ozIMYj2uaaB.Jqjy8pS3JsFT/b6NMXbptN2e6km',
                'birth_date' => '1980-01-01',
                // 'region_id' => $first_region->id,
                'gender' => 'male',
                // 'citizen_id' => $citizen->id,
            ]);
        }

        $user->roles()->syncWithoutDetaching($user_role->id);
        $user->roles()->syncWithoutDetaching($admin_role->id);
//---------------------------------------------------------------------------------

//         $citizen = Citizen::firstOrCreate([
//             'national_number' => '123412367892',
//             'id_card_data' => '123214567892',
//         ], [
//             'national_number' => '1232124567892',
//             'id_card_data' => '1234562137892',
//         ]);

//         $user = User::where('phone_number', '0000000000')->first();
//         if ($user == null) {
//             $user = User::create([
//                 'name' => 'seeder user',
//                 'phone_number' => '0000000000',
//                 'role_id' => $user_role?->id,
//                 'email' => 'seeder@gmail.com',
//                 'avatar' => 'avatars/30-60-Male.png',
//                 'phone_number_verified_at' => now(),
//                 'password' => '$2a$12$NtaOVNCsx1Lt06ozIMYj2uaaB.Jqjy8pS3JsFT/b6NMXbptN2e6km',
//                 'birth_date' => '1980-01-01',
//                 'region_id' => $first_region?->id,
//                 'gender' => 'male',
//                 'citizen_id' => $citizen->id,
//             ]);
//         } else {
//             $user->update([
//                 'name' => 'seeder user',
//                 'phone_number' => '0000000000',
//                 'role_id' => $user_role->id,
//                 'email' => 'seeder@gmail.com',
//                 'avatar' => 'avatars/30-60-Male.png',
//                 'phone_number_verified_at' => now(),
//                 'password' => '$2a$12$NtaOVNCsx1Lt06ozIMYj2uaaB.Jqjy8pS3JsFT/b6NMXbptN2e6km',
//                 'birth_date' => '1980-01-01',
//                 'region_id' => $first_region?->id,
//                 'gender' => 'male',
//                 'citizen_id' => $citizen?->id,
//             ]);
//         }

//         $user->roles()->syncWithoutDetaching($user_role?->id);

// //---------------------------------------------------------------------------------



//         $citizen = Citizen::firstOrCreate([
//             'national_number' => '123453367892',
//             'id_card_data' => '1232112567892',
//         ], [
//             'national_number' => '123453367892',
//             'id_card_data' => '1232112567892',
//         ]);

//         $user = User::where('phone_number', '0900000000')->first();

//         if ($user == null) {
//             $user = User::create([
//                 'name' => 'anonymous user',
//                 'phone_number' => '0900000000',
//                 'role_id' => $user_role->id,
//                 'email' => 'anonymous_user@gmail.com',
//                 'avatar' => 'avatars/30-60-Male.png',
//                 'phone_number_verified_at' => now(),
//                 'password' => '$2a$12$NtaOVNCsx1Lt06ozIMYj2uaaB.Jqjy8pS3JsFT/b6NMXbptN2e6km',
//                 'birth_date' => '1980-01-01',
//                 'region_id' => $first_region->id,
//                 'gender' => 'male',
//                 'citizen_id' => $citizen->id,
//             ]);
//         } else {
//             $user->update([
//                 'name' => 'anonymous user',
//                 'phone_number' => '0900000000',
//                 'role_id' => $user_role->id,
//                 'email' => 'anonymous_user@gmail.com',
//                 'avatar' => 'avatars/30-60-Male.png',
//                 'phone_number_verified_at' => now(),
//                 'password' => '$2a$12$NtaOVNCsx1Lt06ozIMYj2uaaB.Jqjy8pS3JsFT/b6NMXbptN2e6km',
//                 'birth_date' => '1980-01-01',
//                 'region_id' => $first_region->id,
//                 'gender' => 'male',
//                 'citizen_id' => $citizen->id,
//             ]);
//         }

//         $user->roles()->syncWithoutDetaching($user_role->id);

        //--------------------------------------------------------------------
        // //SECTION - Users

        // $citizen = Citizen::firstOrCreate([
        //     'national_number' => 'user_123456789',
        //     'id_card_data' => 'user_id_card_data',
        // ], [
        //     'national_number' => 'user_123456789',
        //     'id_card_data' => 'user_id_card_data',
        // ]);
        // $user = User::firstOrCreate([
        //     'phone_number' => '0999999999',
        // ], [
        //     'name' => 'user testoo',
        //     'phone_number' => '0999999999',
        //     'role_id' => $user_role->id,
        //     'email' => 'testingo.dingo@gmail.com',
        //     'avatar' => 'avatars/18-30-Male.png',
        //     'phone_number_verified_at' => now(),
        //     'password' => '$2a$12$NtaOVNCsx1Lt06ozIMYj2uaaB.Jqjy8pS3JsFT/b6NMXbptN2e6km',
        //     'birth_date' => '2001-01-01',
        //     'region_id' => $first_region?->id,
        //     'gender' => 'male',
        //     'citizen_id' => $citizen->id,
        // ]);
        // $user->roles()->syncWithoutDetaching($user_role->id);
        // //-----------------------------------------------------------------------------------------


        // //SECTION - Doctors
        // $specialization_one_id = Specialization::where('id', 1)->first()?->id;
        // $specialization_two_id = Specialization::where('id', 2)->first()?->id;

        // $sub_specialization_one_id = SubSpecialization::where('id', 1)->first()?->id;
        // $sub_specialization_two_id = SubSpecialization::where('id', 2)->first()?->id;
        // $sub_specialization_three_id = SubSpecialization::where('id', 3)->first()?->id;


        // $citizen = Citizen::firstOrCreate([
        //     'national_number' => 'doctor_123124567896',
        //     'id_card_data' => 'doctor_1234565327896',
        // ], [
        //     'national_number' => 'doctor_123124567896',
        //     'id_card_data' => 'doctor_1234565327896',
        // ]);

        // $user = User::firstOrCreate([
        //     'phone_number' => '0922222222',
        // ], [
        //     'name' => 'doctor pharmaway',
        //     'phone_number' => '0922222222',
        //     'role_id' => $doctor_role->id,
        //     'email' => 'tetes@gmail.com',
        //     'avatar' => null,
        //     'phone_number_verified_at' => now(),
        //     'password' => '$2a$12$NtaOVNCsx1Lt06ozIMYj2uaaB.Jqjy8pS3JsFT/b6NMXbptN2e6km',
        //     'birth_date' => '2001-01-01',
        //     'region_id' => $first_region?->id,
        //     'gender' => 'female',
        //     'citizen_id' => $citizen->id,
        // ]);
        // $user->roles()->syncWithoutDetaching($user_role->id);
        // $user->roles()->syncWithoutDetaching([$doctor_role->id => ['image' => 'avatars/18-30-Female.png']]);

        // $profile = $user->profiles->where('role_id', 3)->first();
        // if (!$profile->profileInfo) {
        //     $profile_info = $profile->profileInfo()->create([
        //         'spec_id' => $specialization_one_id,
        //         'job_title' => null,
        //         'bio' => '[]'

        //     ]);
        //     $profile_info->subSpecializations()->attach([$sub_specialization_one_id, $sub_specialization_two_id, $sub_specialization_three_id]);
        // }




        //-----------------------------------------------------------------------------------------

        // SECTION - Brand Operators///////////////////////////////////////////////
    //     $citizen = Citizen::firstOrCreate([
    //         'national_number' => '1234567894',
    //         'id_card_data' => '1234567894',
    //     ], [
    //         'national_number' => '1234567894',
    //         'id_card_data' => '1234567894',
    //     ]);

    //     $brand = Brand::find(1);

    //     $user = User::firstOrCreate([
    //         'phone_number' => '0944444444',
    //     ], [
    //         'name' => "$brand->text Manager",
    //         'phone_number' => '0944444444',
    //         'role_id' => $brand_operator_role->id,
    //         'email' => 'text@gmail.com',
    //         'avatar' => null,
    //         'phone_number_verified_at' => now(),
    //         'password' => '$2a$12$NtaOVNCsx1Lt06ozIMYj2uaaB.Jqjy8pS3JsFT/b6NMXbptN2e6km',
    //         'birth_date' => '1980-01-01',
    //         'region_id' => $first_region?->id,
    //         'gender' => 'male',
    //         'citizen_id' => $citizen->id,
    //     ]);
    //     $user->roles()->syncWithoutDetaching($user_role->id);
    //     $user->roles()->syncWithoutDetaching($brand_operator_role->id);
    //     $brand->profiles()->attach($user->profiles->where('role.name', 'brand_operator')->first());
    //     $brand->save();
    }
}
