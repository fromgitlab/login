<?php

namespace Modules\Auth\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Auth\Interfaces\V1\AuthRepositoryInterface;
use Modules\Auth\Interfaces\V1\CitizenRepositoryInterface;
use Modules\Auth\Interfaces\V1\OtpRepositoryInterface;
use Modules\Auth\Interfaces\V1\ProfileAuthRepositoryInterface;
use Modules\Auth\Interfaces\V1\RoleRepositoryInterface;
use Modules\Auth\Interfaces\V1\UserRepositoryInterface;
use Modules\Auth\Repositories\V1\AuthRepository;
use Modules\Auth\Repositories\V1\CitizenRepository;
use Modules\Auth\Repositories\V1\OtpRepository;
use Modules\Auth\Repositories\V1\ProfileAuthRepository;
use Modules\Auth\Repositories\V1\RoleRepository;
use Modules\Auth\Repositories\V1\UserRepository;

class RepositoryServiceProvider extends ServiceProvider
{

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
 
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        if(config('app.api_latest') == '1'){

            $this->app->bind(AuthRepositoryInterface::class, AuthRepository::class);
            $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
            $this->app->bind(OtpRepositoryInterface::class, OtpRepository::class);
            $this->app->bind(RoleRepositoryInterface::class, RoleRepository::class);
            $this->app->bind(ProfileAuthRepositoryInterface::class, ProfileAuthRepository::class);
            $this->app->bind(CitizenRepositoryInterface::class, CitizenRepository::class);
        }
      
    }

  
}
