<?php

namespace Modules\Auth\Enums;

enum RegisterRuleEnum: string
{
    case USER = 'user';
    case PROVIDER = 'provider';

    public static function getCase(string $name): RegisterRuleEnum
    {
        return match ($name) {
            'USER' => self::USER,
            'PROVIDER' => self::PROVIDER,
            default => throw new \Exception('Invalid Register Rule Name'),
        };
    }
}
