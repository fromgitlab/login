<?php

namespace Modules\Auth\Enums;

enum RoleEnum: string
{
    case USER = 'user';
    case DOCTOR = 'doctor';
    case BRAND = 'brand_operator';
    case ADMIN = 'admin';

    public static function getCase(string $name): RoleEnum
    {
        return match ($name) {
            'USER' => self::USER,
            'DOCTOR' => self::DOCTOR,
            'BRAND' => self::BRAND,
            'ADMIN' => self::ADMIN,
            default => throw new \Exception('Invalid Role Name'),
        };
    }
}
