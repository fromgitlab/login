<?php
return [
    'common' => [
        'phone_number' => 'رقم الهاتف',
        'password' => 'كلمة المرور ',
        'profile_type' => 'نوع الحساب الشخصي',
        'national_number' => 'الرقم الوطني',
        'id_card_data' => 'معلومات الهوية ',
        'otp' => 'otp',
    ],
    'auth' => [
        'device_id' => 'معرف الجهاز',
        'created_at' => 'أنشئت في',
        'fcm_token' => 'fcm_token',
        'locale' => 'اللغة',
        'last_login_at' => 'اخر تسجل دخول',
        'user' => 'المستخدم',
        'name' => 'الاسم',
        'birth_date' => 'تاريخ الميلاد',
        'region_id' => 'معرف المنطقة',
        'gender' => 'الجنس',
        'avatar' => 'الصورة الرمزية',
        'qr_code' => 'رمز qr',
        'user_id' => 'معرف المستخدم',
    ],
    'citizen' => [],
    'otp' => [
        'type_id' => 'معرف النوع',
    ],
    'profile_auth' => [],
    'user' => [
        'old_password' => 'كلمة المرور القديمة',
        'email' => 'البريد الكتروني ',
    ],
];