<?php
return [
    'common' => [
        'phone_number' => 'phone number',
        'password' => 'password',
        'profile_type' => 'profile type',
        'national_number' => 'national number',
        'id_card_data' => 'id card data',
        'otp' => 'otp',
    ],
    'auth' => [
        'device_id' => 'device id',
        'created_at' => 'created at',
        'fcm_token' => 'fcm_token',
        'locale' => 'locale',
        'last_login_at' => 'last_login_at',
        'user' => 'user',
        'name' => 'name',
        'birth_date' => 'birth date',
        'region_id' => 'region id',
        'gender' => 'gender',
        'avatar' => 'avatar',
        'qr_code' => 'qr_code',
        'user_id' => 'user_id',
    ],
    'citizen' => [],
    'otp' => [
        'type_id' => 'type id',
    ],
    'profile_auth' => [],
    'user' => [
        'old_password' => 'old password',
        'email' => 'email',
    ],
];