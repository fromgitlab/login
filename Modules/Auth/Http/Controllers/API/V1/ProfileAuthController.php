<?php

namespace Modules\Auth\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use Modules\Auth\Http\Requests\ProfileAuth\ProfileLoginRequest;
use Modules\Auth\Interfaces\V1\ProfileAuthRepositoryInterface;

class ProfileAuthController extends BaseController
{


    public function __construct(private ProfileAuthRepositoryInterface $profileAuthRepository)
    {
        parent::__construct();
    }
    /**
     * Login a profile.
     * @param Request $request
     * @return 
     */
    public function login(ProfileLoginRequest $request)
    {
        return apiResponse(
            __("profileAuth.login"),
            ["token" => $this->profileAuthRepository->login($request)],
            200,
            null,
        );
    }
}
