<?php

namespace Modules\Auth\Http\Controllers\API\V1;

use App\Http\Controllers\BaseController;
use Modules\Auth\Http\Requests\Citizens\CitizenAddRequest;
use Modules\Auth\Interfaces\V1\CitizenRepositoryInterface;

class CitizenController extends BaseController
{
    
    public function __construct(private CitizenRepositoryInterface $citizenRepository)
    {
        parent::__construct();
    }
    /**
     * add only a citizen to the system without being a user
     * @param CitizenAddRequest $request
     * @return 
     */
    public function addCitizen(CitizenAddRequest $request)
    {
        return apiResponse(
            __("citizen.addCitizen"),
            $this->citizenRepository->addCitizen($request),
            201
        );
    }
}
