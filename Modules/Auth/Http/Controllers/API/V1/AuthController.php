<?php

namespace Modules\Auth\Http\Controllers\API\V1;


use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use Modules\Auth\Http\Requests\Auth\DeviceRegisterRequest;
use Modules\Auth\Http\Requests\Auth\UserValidateQrCodeRequest;
use Modules\Auth\Http\Requests\Auth\UserValidatePhoneNumberRequest;
use Modules\Auth\Http\Requests\Auth\UserValidateIdRequest;
use Modules\Auth\Http\Requests\Auth\UserRegisterRequest;
use Modules\Auth\Http\Requests\Auth\CheckPhoneRequest;
use Modules\Auth\Helpers\AuthHelper;
use Modules\Auth\Http\Requests\Auth\UserLoginMobileRequest;
use Modules\Auth\Http\Requests\Auth\UserLoginWebRequest;
use Modules\Notification\Directors\AuthUserFCMND;
use Modules\Notification\Notifications\AnotherLoginNotification;
use Modules\Auth\Interfaces\V1\AuthRepositoryInterface;

class AuthController extends BaseController
{

    public function __construct(private AuthRepositoryInterface $authRepository)
    {
        parent::__construct();
    }


    /**
     * Returns neccessary data for the app mount to function according to user state and info.
     * @return 
     */
    public function mount()
    {
        return apiResponse(
            __("auth.mount"),
            $this->authRepository->mount(),
            200
        );
    }


    public function getMe()
    {
        return apiResponse(
            __("auth.getMe"),
            $this->authRepository->getMe(),
            200
        );
    }
    public function registerDevice(DeviceRegisterRequest $request)
    {
        return apiResponse(
            __("auth.registerDevice"),
            ['user' => $this->authRepository->registerDevice($request)],
            200
        );
    }

    /**
     * Register in the system with the ID card and phone number.
     * @param UserRegisterRequest $requestW
     * @return 
     */
    public function register(UserRegisterRequest $request)
    {
        $user = $this->authRepository->register($request);

        return apiResponse(
            __("auth.register"),
            [
                "token" => AuthHelper::defaultProfileCreateToken($user),
            ],
            434,
            null,
        );
    }

    /**
     * Login in the system with phone number and password.
     * @param UserLoginWebRequest $request
     * @return 
     */
    public function loginWeb(UserLoginWebRequest $request)
    {
        $auth_user = $this->authRepository->loginWeb($request);
        if ($auth_user->status == 200) {
            (new AuthUserFCMND($auth_user->user, new AnotherLoginNotification($auth_user->user)))->send();
        }

        return apiResponse(
            $auth_user->reason,
            [
                "user" => $auth_user->user_resource,
                "token" => $auth_user->token ? $auth_user->token : null,
            ],
            $auth_user->status,
            null,
        );
    }

    public function loginMobile(UserLoginMobileRequest $request)
    {
        $auth_user = $this->authRepository->loginMobile($request);
        if ($auth_user->status == 200) {
            (new AuthUserFCMND($auth_user->user, new AnotherLoginNotification($auth_user->user)))->send();
        }

        return apiResponse(
            $auth_user->reason,
            [
                "token" => $auth_user->token ? $auth_user->token : null,
            ],
            $auth_user->status,
            null,
        );
    }

    /**
     * check if the phone number exists in the database
     * @param Request $request
     * @return 
     */
    public function checkPhoneNumber(CheckPhoneRequest $request)
    {
        $user = $this->authRepository->checkPhoneNumber($request);
        if ($user) {
            return apiResponse(
                __("auth.checkPhoneNumber.exist"),
                null,
                200
            );
        }
        return apiResponse(
            __("auth.checkPhoneNumber.notExist"),
            null,
            404
        );
    }

    /**
     * Validate phone number of a user
     * @param UserValidatePhoneNumberRequest $request
     */
    public function validatePhoneNumber(UserValidatePhoneNumberRequest $request)
    {
        $result = $this->authRepository->validatePhoneNumber($request);

        if ($result['status']) {
            AuthHelper::userVerifiedPhone($result['user']);
            return apiResponse(
                __("auth.validatePhoneNumber.success"),
                null,
                200
            );
        }
        return apiResponse(
            __("auth.validatePhoneNumber.fail"),
            null,
            403
        );
    }

    /**
     * validate the id card data
     * @param UserValidateIdRequest $request
     * @return 
     */
    public function validateId(UserValidateIdRequest $request)
    {
        return apiResponse(
            __("auth.validateId"),
            $this->authRepository->validateId($request),
            200
        );
    }

    /**
     * Validate the qr code of a citizen
     * @param UserValidateQrCodeRequest $request
     * @return 
     */
    public function citizenValidateQrCode(UserValidateQrCodeRequest $request)
    {

        if ($this->authRepository->citizenValidateQrCode($request)) {
            return apiResponse(
                __("auth.citizenValidateQrCode.success"),
                $this->authRepository->citizenValidateQrCode($request),
                200
            );
        } else {
            return apiResponse(
                __("auth.citizenValidateQrCode.fail"),
                null,
                404
            );
        }
    }

    /**
     * Logout and delete the current profile access token
     * @return 
     */
    public function logout()
    {
        return apiResponse(
            __("auth.logout"),
            $this->authRepository->logout(),
            200
        );
    }



    // this feature currently suspended
    // public function authenticateDevice(DeviceAuthenticateRequest $request)
    // {
    //     //send notification to the device token using the device stored in the devices table
    //     // using firebase fcm package
    //     $device = Device::where('device_id', $request->validated()['device_id'])->first();
    //     $messaging = app('firebase.messaging');

    //     $test = config('firebase.projects.app.credentials.file');
    //     (new Factory)->withServiceAccount($test);


    //     $message = CloudMessage::new()->withNotification(Notification::fromArray([]))->withData([
    //         'token' => Auth::user()->createToken('token')->plainTextToken
    //     ]);

    //     $messaging->sendMulticast(
    //         $message,
    //         [$device->fcm_token]
    //     );


    //     return apiResponse(
    // __("auth.authenticateDevice"),
    //         null,
    //         200
    //     );
    // }

}
