<?php

namespace Modules\Auth\Http\Controllers\API\V1;

use Modules\Auth\Entities\User;
use Modules\Auth\Http\Requests\Users\AuthChangePasswordRequest;
use Modules\Auth\Http\Requests\Users\ForgetPasswordRequest;
use Modules\Auth\Http\Requests\Users\UserPromoteToAdminRequest;
use Modules\Auth\Http\Requests\Users\UserChangePasswordRequest;
use Modules\Auth\Interfaces\V1\UserRepositoryInterface;
use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use Modules\Auth\Http\Requests\Users\AdminChangePasswordRequest;

class UserController extends BaseController
{
    public function __construct(private UserRepositoryInterface $userRepository)
    {
        parent::__construct();
    }

    public function index(Request $request)
    {
        return apiResponse(
            __("user.index"), //'all users'
            transferDataWithPagination($this->userRepository->index($request)),
            200,
        );
    }


    public function show(User $user)
    {
        return apiResponse(
            __("user.show"), //'User'
            $this->userRepository->show($user),
            200
        );
    }

    /**
     * Change the password of a registered user
     * @param AdminChangePasswordRequest $request
     */
    //change user password by Admin for any user
    public function adminChangePassword(AdminChangePasswordRequest $request, User $user)
    {
        return apiResponse(
            __("user.adminChangePassword"),
            $this->userRepository->adminChangePassword($request, $user),
            200
        );
    }

    /**
     * Change the password of an authed user
     * @param AuthChangePasswordRequest $request
     * @return 
     */

    // change password by user
    public function authChangePassword(AuthChangePasswordRequest $request)
    {
        return apiResponse(
            __("user.authChangePassword"),
            $this->userRepository->authChangePassword($request),
            200
        );
    }

    /**
     * Change the pinfor a user forget his password
     * @param ForgetPasswordRequest $request
     * @return 
     */
    public function forgetPassword(ForgetPasswordRequest $request)
    {
        return apiResponse(
            __("user.forgetPassword"),
            $this->userRepository->forgetPassword($request),
            200
        );
    }

    /**
     * Promote a user to an admin
     * @param UserPromoteToAdminRequest $request
     * @param User $user
     * @return 
     */
    public function promoteAdmin(UserPromoteToAdminRequest $request, User $user)
    {
        return apiResponse(
            __("user.promoteAdmin"),
            $this->userRepository->promoteAdmin($user),
            200
        );
    }
}
