<?php

namespace Modules\Auth\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use Modules\Auth\Interfaces\V1\RoleRepositoryInterface;

class RoleController extends BaseController
{

    public function __construct(private RoleRepositoryInterface $roleRepository)
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     * @return 
     */
    public function index(Request $request)
    {

        return apiResponse(
            __("role.index"),
            transferDataWithPagination( $this->roleRepository->index($request)),
            200,
        );
    }
}
