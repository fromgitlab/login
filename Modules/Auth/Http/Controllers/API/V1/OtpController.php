<?php

namespace Modules\Auth\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use Modules\Auth\Http\Requests\Otp\UserSendOtpRequest;
use Modules\Auth\Interfaces\V1\OtpRepositoryInterface;

class OtpController extends BaseController
{


    public function __construct(private OtpRepositoryInterface $otpRepository)
    {
        parent::__construct();
    }

    /**
     * Send a one time password to the user phone number.
     * @param Request $request
     * @return 
     */
    public function sendOTP(UserSendOtpRequest $request){

        return apiResponse(
            __("otp.sendOTP"),
            $this->otpRepository->sendOTP($request),
            200  
        );
    }
}