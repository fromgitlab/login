<?php

namespace Modules\Auth\Http\Middleware;

use Auth;
use Closure;
use Illuminate\Http\Request;
use Modules\Profile\Entities\Profile;

class AuthenticateProfileTokens
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = Auth::guard('sanctum')->user();
        if($user instanceof Profile){
            setAuthProfile(Auth::guard('sanctum')->user());
            $user?->user ? Auth::guard('sanctum')->setUser($user->user) : null;
            // Auth::guard('sanctum')->user()->accessToken = $user->currentAccessToken(); 
        }

        return $next($request);
    }
}
