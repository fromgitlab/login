<?php

namespace Modules\Auth\Http\Resources\Common\Catalog;

use App\Http\Resources\TranslationsResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OptionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'text' =>  $this->translate(getLangFromHeader())?->text,
            'unit_id' => $this->unit?->id,
            'unit' =>[
                'text'=> $this->unit?->text
            ],
            // 'translations'=>new TranslationsResource($this->translations),
        ];
    }
}
