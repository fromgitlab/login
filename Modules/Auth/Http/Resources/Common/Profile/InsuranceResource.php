<?php
namespace Modules\Auth\Http\Resources\Common\Profile;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Resources\Json\JsonResource;

class InsuranceResource extends JsonResource
{
 
       /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray($request)
    {
        return [
            'id'=> $this->id,
            'text' => $this->translate(getLangFromHeader())?->text,
        ];
    }
}