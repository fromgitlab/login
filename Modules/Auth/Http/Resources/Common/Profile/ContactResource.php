<?php

namespace Modules\Auth\Http\Resources\Common\Profile;

use Illuminate\Http\Resources\Json\JsonResource;

class ContactResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'value' => $this->value,
            'description' => $this->description,
            'text' => $this->translate(getLangFromHeader())?->text,
            'contact_type' => [
                'id' => $this->contactType->id,
                'text' => (string) $this->contactType->translate(getLangFromHeader())?->text,
            ],
        ];
    }
}
