<?php

namespace Modules\Auth\Http\Resources\Common\Profile;

use Illuminate\Http\Resources\Json\JsonResource;


class WorkplaceResource extends JsonResource
{

    public function toArray($request)
    {
        return  [
            'id' => $this->id,
            'text' => $this->translate(getLangFromHeader())?->text,
            'workplace_type' => new WorkplaceTypeResource($this->workplaceType),
        ];
    }
}
