<?php

namespace Modules\Auth\Http\Resources\Common\Profile\Specialization;

use Illuminate\Http\Resources\Json\JsonResource;

class SubSpecializationResource extends JsonResource
{

    public function toArray($request)
    {
        return  [
            'id'=> $this->id,
            'text'=>  $this->translate(getLangFromHeader())?->text,
            'specialization' => new SpecializationResource($this->specialization)
            

        ];
       
    }
}
