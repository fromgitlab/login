<?php

namespace Modules\Auth\Http\Resources\Common\Profile\Specialization;

use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Auth\Http\Resources\Common\RoleResource;


class SpecializationResource extends JsonResource
{

    public function toArray($request)
    {
       return [
            'id'=> $this->id,
            'text'=>  $this->translate(getLangFromHeader())?->text,
            'role'=> $this->role ? new  RoleResource($this->role) : null,
            'image'=> $this->image ? url('storage/' . $this->image) : null,
        ];
       
    }
}
