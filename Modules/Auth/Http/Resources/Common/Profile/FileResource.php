<?php

namespace Modules\Auth\Http\Resources\Common\Profile;

use Illuminate\Http\Resources\Json\JsonResource;

class FileResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'path' => $this->path ? url('storage/' .  $this->path) : null,
        ];
    }
}
