<?php

namespace Modules\Auth\Http\Resources\Common;

use Illuminate\Http\Resources\Json\JsonResource;


class RoleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray($request)
    {
        return [
            'id' =>  $this->id,
            'name' => $this->name,
            'text' => $this?->translate(getLangFromHeader())->text
        ];
    }
}
