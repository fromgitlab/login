<?php

namespace Modules\Auth\Http\Resources\Common\Category;

use Illuminate\Http\Resources\Json\JsonResource;

class TypeResource extends JsonResource 
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray($request)
    {
        return [
            'id'=> $this->id,
            'text'=> (string) $this->translate(getLangFromHeader())?->text,
            'type'=> $this->type,
            'key'=> $this->key,
        ];
    }
}
