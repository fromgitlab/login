<?php

namespace Modules\Auth\Http\Resources\Common\Category;


use Illuminate\Http\Resources\Json\JsonResource;

class UnitResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'text' =>  $this->translate(getLangFromHeader())?->text,
            'type' =>  $this->type,
        ];
    }
}
