<?php


namespace Modules\Auth\Http\Resources\Profiles;

use Illuminate\Http\Resources\Json\JsonResource;

class ProfileCustomShowResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray($request)
    {
        


        switch ($this->role->name) {
            case 'admin':
                // TODO - add admin profile data
                $data = new UserProfileShowResource($this );
                break;


            case 'user':
                $data = new UserProfileShowResource($this);
                break;


            case 'doctor':
                $data = new DoctorProfileShowResource($this);
                break;


            case 'brand_operator':
                $data = new BrandProfileShowResource($this);
                break;
        }
        return $data;
    }
}
