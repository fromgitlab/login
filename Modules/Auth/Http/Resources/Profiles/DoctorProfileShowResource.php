<?php

namespace Modules\Auth\Http\Resources\Profiles;

use App\Http\Resources\TranslationsResource;
use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Auth\Http\Resources\Common\Profile\ContactResource;
use Modules\Auth\Http\Resources\Common\Profile\FileResource;
use Modules\Auth\Http\Resources\Common\Profile\InsuranceResource;
use Modules\Auth\Http\Resources\Common\Profile\Specialization\SpecializationResource;
use Modules\Auth\Http\Resources\Common\Profile\Specialization\SubSpecializationResource;
use Modules\Auth\Http\Resources\Common\Profile\WorkplaceResource;
use Modules\Auth\Http\Resources\Common\RoleResource;

class DoctorProfileShowResource extends JsonResource
{

    public function toArray($request)
    {
        
        $suspension = $this->getLatestSuspension(); 
        return  [
 
            'id' => $this->id,
            'image' => ($this->image) ? url('storage/' . $this->image) : null,
            
            'profile_type' => $this->role->name,
            'role' => new RoleResource($this->role),

            // 
            'statistics' => $this->profileInfo ? [
                // 'recommendations' => $this->recommendors_count,
                'recommendations' => $this->recommendors->count(),

                // 'number_of_prescriptions' => $this->prescriptions_count,
                'number_of_prescriptions' => $this->prescriptions->count(),
            ] : null,
      
            'bio' => $this->profileInfo?->bio,
            'spec' =>  $this->profileInfo?->specialization ? new SpecializationResource($this->profileInfo->specialization) : null,
            'sub_specs' => $this->profileInfo?->subSpecializations ? SubSpecializationResource::collection($this->profileInfo?->subSpecializations) : [],
            'sub_specs_string' => $this->profileInfo?->subSpecializations ? implode('-', $this->profileInfo->subSpecializations->pluck('text')->toArray()) : null,
            'status' => [
                'key' =>       $suspension ? $suspension->suspension_type : 'active',
                'reason' => (string)  $suspension?->translate(getLangFromHeader())?->reason,
                'translations' => new TranslationsResource($suspension?->translations),
            ],
            'contacts' => $this->contacts ?  ContactResource::collection($this->contacts) : [],
            'workplaces' =>  WorkplaceResource::collection($this->workplaces) ,
            'insurances' => InsuranceResource::collection($this->insurances),
            'insurances_string' => $this->insurances->count() ? implode('-', $this->insurances->pluck('text')->toArray()) : null,
            'bp_doctor_points' =>  $this->wallet_balance,
            'recommended_from' =>(int) $this->recommendors->count(),
            
            
            'files' => $this->profileInfo && $this->profileInfo?->files ? FileResource::collection($this->profileInfo?->files) : [],
        ];
    }
}
