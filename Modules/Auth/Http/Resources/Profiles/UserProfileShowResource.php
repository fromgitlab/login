<?php

namespace Modules\Auth\Http\Resources\Profiles;

use App\Http\Resources\TranslationsResource;
use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Auth\Http\Resources\Common\RoleResource;


class UserProfileShowResource extends JsonResource
{
    public function toArray($request)
    {
        $suspension = $this->getLatestSuspension();

        return  [
            'id' => $this->id,
            'profile_type' => $this->role->name,
            'role' => new RoleResource($this->role),
            'status' => [
                'key' =>       $suspension ? $suspension->suspension_type : 'active',
                'reason' => (string)  $suspension?->translate(getLangFromHeader())?->reason,
                'translations' => new TranslationsResource($suspension?->translations),
            ],
        ];
    }
}
