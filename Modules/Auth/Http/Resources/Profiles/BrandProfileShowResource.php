<?php

namespace Modules\Auth\Http\Resources\Profiles;

use App\Http\Resources\TranslationsResource;
use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Auth\Http\Resources\Common\Profile\FileResource;
use Modules\Auth\Http\Resources\Common\RoleResource;

class BrandProfileShowResource extends JsonResource
{

    public function toArray($request)
    {

        $suspension = $this->getLatestSuspension();

        return [

            'id' => $this->id,
            'profile_type' => $this->role->name,
            'role' => new RoleResource($this->role),
            'created_at' => $this->created_at,
            'brand' => [
                'id' =>  $this->brand?->id,
                'text' => $this->brand?->text,
                'image' => $this->brand?->image ? url('storage/' . $this->brand->image) : null,
            ],
            'status' => [
                'key' =>       $suspension ? $suspension->suspension_type : 'active',
                'reason' => (string)  $suspension?->translate(getLangFromHeader())?->reason,
                'translations' => new TranslationsResource($suspension?->translations),
            ],

            'files' => $this?->profileInfo && $this?->profileInfo?->files ? FileResource::collection($this->profileInfo->files) : [],
            'job_title' => $this?->profileInfo?->job_title ? $this->profileInfo->job_title : null,
        ];
    }
}
