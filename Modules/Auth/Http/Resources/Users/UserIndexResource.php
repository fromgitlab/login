<?php

namespace Modules\Auth\Http\Resources\Users;

use App\Helpers\Util;
use Illuminate\Http\Resources\Json\JsonResource;


class UserIndexResource extends JsonResource
{


    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray($request)
    {
        $suspension = $this->userProfile()?->getLatestSuspension();

        return
            [
                'id' =>             $this->id, 
                'name' =>           $this->name,
                'gender' =>         $this->gender,
                'avatar' =>         $this->avatar ? url('storage/' . $this->avatar) : null,
                'phone_number' =>   $this->phone_number,
                'birth_date' =>     $this->birth_date ?  Util::YMDToDMY($this->birth_date) : null,
                'created_at' => $this->created_at,
                'is_id_verified' => $this->is_id_verified(), 
                'role_string' => $this->roles->map(function($role){
                    return $role->translate(getLangFromHeader())->text;
                })->implode('-'),

                'status' => [
                    'key' => $suspension ? $suspension->suspension_type : 'active',
                    'reason_en' => $suspension ? $suspension?->translate('en')->reason : null,
                    'reason_ar' => $suspension ? $suspension?->translate('ar')->reason : null,
                ],
                'address' =>      $this->address,
            ];
    }
}
