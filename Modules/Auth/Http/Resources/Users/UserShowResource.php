<?php

namespace Modules\Auth\Http\Resources\Users;

use App\Helpers\Util;
use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Auth\Http\Resources\Profiles\ProfileCustomShowResource;

class UserShowResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray($request)
    {

        return [
            'id' =>             $this->id, 
            'name' =>           $this->name,
            'gender' =>         $this->gender,
            'avatar' =>         $this->avatar ? url('storage/' . $this->avatar) : null,
            'phone_number' =>   $this->phone_number,
            'birth_date' =>     $this->birth_date ?  Util::YMDToDMY($this->birth_date) : null,
            'is_id_verified' => $this->is_id_verified(), 
            'address' =>      $this->address,

            'profiles' => ProfileCustomShowResource::collection($this->profiles),
        ];
    }
}
