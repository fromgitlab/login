<?php

namespace Modules\Auth\Http\Resources\Users;

use Illuminate\Http\Resources\Json\JsonResource;

class ProfileTypeResource extends JsonResource
{


    public function toArray($request)
    {

        $roles_list = $this->roles->pluck('id', 'name');
        $profile_types = [];

        foreach ($roles_list as $role_name => $role_id) {

            $profile_types[] = [
                'id' => $role_id,
                'name' => $role_name,
                'is_default' => $this->default_profile->role_id == $role_id,
            ];
        }
        return $profile_types;
    }
}
