<?php

namespace Modules\Auth\Http\Resources\Auth;

use Illuminate\Http\Resources\Json\JsonResource;

class MountResource extends JsonResource
{

    public function __construct(
        private $version_data,
        private $item_version,
        private $units_data,
        private $package_forms = null,
        private $dosage_forms = null
    ) {}

    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray($request)
    {
        return
            [
                'versioning' => $this->version_data,
                'item_version' => $this->item_version,
                'units' =>  $this->units_data,
                'options' =>   $this->when(($this->package_forms), [
                    'package_forms' => $this->package_forms,
                    'dosage_forms' => $this->dosage_forms,
                ])
            ];
    }
}
