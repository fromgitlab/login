<?php

namespace Modules\Auth\Http\Resources\Auth;

use Illuminate\Http\Resources\Json\JsonResource;

class ScanCitizenInfoResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray($request)
    {
        return
            [
                'citizen_id' => $this->id,
                'name' => $this->user?->name,
                'phone_number' => $this->user?->phone_number,
            ];
    }
}
