<?php

namespace Modules\Auth\Http\Requests\Citizens;

use App\Http\Requests\BaseRequest;
use App\Enums\ModuleName;
use Modules\Auth\Policies\CitizenPolicy;

class CitizenAddRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'national_number' => ['required'],
            'id_card_data' => ['required', 'string'],
        ];
    }

    public function attributes()
    {
        return parent::getTranslatedAttributes(
            array_keys($this->rules()),
            ModuleName::Auth,
            'citizen'
        );
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return CitizenPolicy::add(authProfile());
    }
}
