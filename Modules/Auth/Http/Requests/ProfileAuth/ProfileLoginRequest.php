<?php

namespace Modules\Auth\Http\Requests\ProfileAuth;

use App\Http\Requests\BaseRequest;
use App\Enums\ModuleName;
use Illuminate\Validation\Rule;
use Modules\Auth\Enums\RoleEnum;

class ProfileLoginRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'profile_type' => ['required', 'string', Rule::in(array_column(RoleEnum::cases(), 'name'))]
        ];
    }
    public function attributes()
    {
        return parent::getTranslatedAttributes(
            array_keys($this->rules()),
            ModuleName::Auth,
            'profile_auth'
        );
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
