<?php

namespace Modules\Auth\Http\Requests\Users;

use App\Http\Requests\BaseRequest;
use App\Enums\ModuleName;

class AuthChangePasswordRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_password' => ['required', 'regex:"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$"'],
            'password' => ['required', 'regex:"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$"'],
        ];
    }

    public function attributes()
    {
        return parent::getTranslatedAttributes(
            array_keys($this->rules()),
            ModuleName::Auth,
            'user'
        );
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !(auth()->user()->roles->contains('name', 'admin'));
    }
}
