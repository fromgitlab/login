<?php

namespace Modules\Auth\Http\Requests\Users;

use App\Http\Requests\BaseRequest;
use App\Enums\ModuleName;
use Modules\Auth\Policies\UserPolicy;

class UserPromoteToAdminRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // return true ;
        return UserPolicy::promoteToAdmin(authProfile());
    }
}
