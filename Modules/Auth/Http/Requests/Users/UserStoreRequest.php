<?php

namespace Modules\Auth\Http\Requests\Users;

use App\Http\Requests\BaseRequest;
use App\Enums\ModuleName;

class UserStoreRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     * name is required
     * email is required and must be type of email
     * password is required
     * password should has at-least one symbol
     * password should has at-least 1 Uppercase
     * password should has at-least 1 Lowercase
     * password should has at-least 1 Numeric and 1 special character
     * password must be confirmed
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone_number' => ['required', 'regex:"(?=09)\d{10}"', 'unique:users,phone_number'],
            'email' => ['email', 'unique:users,email'],
        ];
    }


    public function attributes()
    {
        return parent::getTranslatedAttributes(
            array_keys($this->rules()),
            ModuleName::Auth,
            'user'
        );
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
