<?php

namespace Modules\Auth\Http\Requests\Otp;

use App\Http\Requests\BaseRequest;
use App\Enums\ModuleName;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;

class UserSendOtpRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     * type is required from the values PHONE or EMAIL
     * phone_number is required if the user dosen't has a token
     * @return array
     */
    public function rules()
    {
        Auth::check() ? Auth::setUser(Auth::guard('sanctum')->user()) : null;

        return [
            // 'type_id' => ["required", Rule::exists('types', 'id')->where('type', 'OTP')],
            'phone_number' => [Rule::requiredIf(!Auth::check())]
        ];
    }

    public function attributes()
    {
        return parent::getTranslatedAttributes(
            array_keys($this->rules()),
            ModuleName::Auth,
            'otp'
        );
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
