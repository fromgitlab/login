<?php

namespace Modules\Auth\Http\Requests\Auth;

use App\Http\Requests\BaseRequest;
use App\Enums\ModuleName;
use Illuminate\Support\Facades\Hash;
use Modules\Auth\Entities\User;
use Modules\Profile\Enums\ProfileStatusEnum;

class UserLoginWebRequest extends BaseRequest
{

    public function rules()
    {
        return [
            'phone_number' => ['required'],
            'password' => ['required'],
            'user' => ['prohibited'],
        ];
    }

    public function attributes()
    {
        return parent::getTranslatedAttributes(
            array_keys($this->rules()),
            ModuleName::Auth,
            'auth'
        );
    }




    public function passedValidation()
    {
        if ($this->phone_number == "0900000000" || $this->phone_number == "0000000000") {
            abort(400, __("auth.login.wrong_credentials"));
        }
        $user = User::where('phone_number', $this->phone_number)->first();
        if (!$user || !(Hash::check($this->password, $user->password))) {
            abort(401, __("auth.login.wrong_credentials"));
        }

        if (!in_array($user->default_profile->role->name, ['brand_operator', 'admin'])) {
            abort(403, __("auth.login.forbiddenWeb")); //'Your account dosen\'t support the web version. You can benefit from Pharmaway Servicers by downloading the mobile application.'
        }

        if ($user->default_profile?->getLatestSuspension()?->suspension_type == ProfileStatusEnum::BLOCKED) {
            abort(401, __("profile.hasSuspension")); // "Your profile not allowed to do this action."
        }

        $this->merge(['user' => $user]);
        return true;
    }
}
