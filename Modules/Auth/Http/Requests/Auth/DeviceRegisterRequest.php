<?php

namespace Modules\Auth\Http\Requests\Auth;

use App\Http\Requests\BaseRequest;
use App\Enums\ModuleName;
use Illuminate\Support\Facades\Auth;

class DeviceRegisterRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->merge(['last_login_at' => now()]);


        return [
            'device_id' => ['required', 'string', 'max:255'],
            'fcm_token' => ['nullable', 'string', 'max:500'],
            'locale' => ['nullable', 'string', 'max:2'],
            'last_login_at' => ['required']

        ];
    }
    public function attributes()
    {
        return parent::getTranslatedAttributes(
            array_keys($this->rules()),
            ModuleName::Auth,
            'auth'
        );
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
