<?php

namespace Modules\Auth\Http\Requests\Auth;

use App\Http\Requests\BaseRequest;
use App\Enums\ModuleName;

class UserValidatePhoneNumberRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'otp' => ['required']
        ];
    }
    public function attributes()
    {
        return parent::getTranslatedAttributes(
            array_keys($this->rules()),
            ModuleName::Auth,
            'auth'
        );
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
