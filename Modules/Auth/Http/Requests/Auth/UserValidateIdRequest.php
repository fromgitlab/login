<?php

namespace Modules\Auth\Http\Requests\Auth;

use App\Http\Requests\BaseRequest;
use App\Enums\ModuleName;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Validator;
use Modules\Auth\Entities\Citizen;
use Modules\Auth\Helpers\AuthHelper;
use Modules\Category\Rules\GenderValidationRule;
use Modules\Profile\Actions\AvatarChooserAction;

class UserValidateIdRequest extends BaseRequest
{

    public function prepareForValidation()
    {
        $avatar = AuthHelper::avatarChooserAction($this->gender);
        $this->merge(['avatar' => $avatar?->url]);
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'avatar' => ['sometimes', 'string'],
            'national_number' => ['required', 'string'],
            'id_card_data' => ['required', 'string'],
            'birth_date' => ['required', 'date', 'date_format:Y-m-d'],
            'region_id' => ['required', 'integer'],
            'gender' => ['required', Rule::requiredIf((bool)Auth::user()->gender), new GenderValidationRule()],
        ];
    }
    public function attributes()
    {
        return parent::getTranslatedAttributes(
            array_keys($this->rules()),
            ModuleName::Auth,
            'auth'
        );
    }

    /** 
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * validate if the requester has a citizen related to the same user that they are trying to verify
     */
    public function withValidator(Validator $validator)
    {
        $validator->after(function ($validator) {
            $citizen = Citizen::where('national_number', $this->national_number)->first();
            if ($citizen && $citizen->user) {
                $validator->errors()->add('national_number', 'This ID Card already have an account, please login using it');
            }
        });
    }
}
