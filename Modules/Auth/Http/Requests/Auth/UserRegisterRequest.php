<?php

namespace Modules\Auth\Http\Requests\Auth;

use App\Http\Requests\BaseRequest;
use App\Enums\ModuleName;
use CustomDateFormat;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Validation\Validator;
use Modules\Auth\Entities\Citizen;
use Modules\Category\Rules\GenderValidationRule;

class UserRegisterRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     * phone_number is required and must start with 09 and contain only 10 digits
     * email must be unique and from an email type
     * national_number is required
     * id_card_data is required
     * @return array
     */
    public function rules()
    {
        return [
            'phone_number' => ['required', 'regex:"(?=09)\d{10}"', 'unique:users,phone_number'],
            'name' => ['required'],
            'password' => ['required', 'regex:"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$"'],
            'national_number' => ['unique:citizens,national_number'],
            // 'birth_date' => ['required_with:national_number', 'date'],
            'birth_date' => [
                'required_with:national_number',
                'date',
                'regex:/^\d{4}-(0?[1-9]|1[0-2])-(0?[1-9]|[12]\d|3[01])$/' //YYYY-MM-DD ,YYYY-MM-D,YYYY-M-DD , YYYY-M-D
            ],
            'id_card_data' => ['required_with:national_number'],
            'region_id' => ['required_with:national_number', 'exists:regions,id'],
            'gender' => ['required_with:national_number', new GenderValidationRule()],

        ];
    }

    public function attributes()
    {
        return parent::getTranslatedAttributes(
            array_keys($this->rules()),
            ModuleName::Auth,
            'auth'
        );
    }


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /** 
     * validate if the requester has a citizen related to the same user that they are trying to create
     */
    public function withValidator(Validator $validator)
    {
        $validator->after(function ($validator) {
            if ($this->national_number) {
                $citizen = Citizen::where('national_number', $this->national_number)->first();
                if ($citizen && $citizen->user) {
                    $validator->errors()->add('national_number', 'This ID Card already have an account, please login using it');
                }
            }
        });
    }
}
