<?php

namespace Modules\Auth\Http\Requests\Auth;

use App\Http\Encryption;
use App\Http\Requests\BaseRequest;
use App\Enums\ModuleName;
use Modules\Auth\Rules\ValidQrCode;

class UserValidateQrCodeRequest extends BaseRequest
{

    protected function prepareForValidation()
    {
        $qr_code = $this->input('qr_code');

        if ($qr_code === null) {
            return null;
        }

        $encryptor = new Encryption();
        try {
            $data = $encryptor->decrypt($qr_code);
        } catch (\Exception $e) {
            return null;
        }

        $data = json_decode($data, true);

        $this->merge([
            'user_id' => $data['user_id'] ?? null,
            'created_at' => $data['created_at'] ?? null,
        ]);
    }

    public function rules()
    {
        return [
            'qr_code' => ['required', 'string', new ValidQrCode],
            'user_id' => ['required'],
            'created_at' => ['required', 'after:' . now()->subDays(3), 'before:' . now()->addDays(3)],
        ];
    }

    public function attributes()
    {
        return parent::getTranslatedAttributes(
            array_keys($this->rules()),
            ModuleName::Auth,
            'auth'
        );
    }

    public function authorize()
    {
        return true;
    }
}
