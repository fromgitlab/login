<?php

namespace Modules\Auth\Http\Requests\Auth;

use App\Http\Requests\BaseRequest;
use App\Enums\ModuleName;
use Crypt;
use Illuminate\Support\Facades\Hash;
use Termwind\Components\Dd;

class DeviceAuthenticateRequest extends BaseRequest
{

    //override validationData() method
    public function validationData()
    {
        try {
            $data = aes_128_decrypt($this->qr_code);
        } catch (\Exception $e) {
            $data = null;
        }
        $data = json_decode(json_decode($data, true), true);

        return [
            'device_id' => $data['device_id'] ?? null,
            'created_at' =>  $data['created_at'] ?? null,
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'device_id' => ['required', 'string'],
            //TODO - change this to 1 minute
            'created_at' => ['required', 'after:' . now()->subMinutes(10000)],
        ];
    }

    public function attributes()
    {
        return parent::getTranslatedAttributes(
            array_keys($this->rules()),
            ModuleName::Auth,
            'auth'
        );
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
