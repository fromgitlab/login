<?php

namespace Modules\Interaction\Interfaces\V1;

use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Interaction\Entities\Alert;

interface AlertRepositoryInterface
{
    function myAlerts(): JsonResource;
     function dismiss(Alert $alert) :void;
}
