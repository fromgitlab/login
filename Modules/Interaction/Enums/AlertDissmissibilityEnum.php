<?php

namespace Modules\Interaction\Enums;

enum AlertDissmissibilityEnum : string{
    case DISMISSIBLE = 'dismissibile';
    case NON_DISMISSIBLE = 'non_dismissible';
    case LOCALLY_DISMISSIBLE = 'locally_dismissible';
}
