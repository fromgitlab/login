<?php

namespace Modules\Interaction\Enums;

enum AlertTypesEnum: string
{
    case SUCCESS = "success";
    case DANGER = "danger";
    case WARNING = "warning";
    case INFO = "info";
    case BANNED = "banned";
}
