<?php

namespace Modules\Interaction\Enums;


enum AlertAppearanceTypeEnum:string {
    
    case TOAST = 'toast';
    case POPUP = 'popup';
}
