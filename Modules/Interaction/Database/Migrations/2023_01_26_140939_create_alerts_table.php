<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Interaction\Enums\AlertAppearanceTypeEnum;
use Modules\Interaction\Enums\AlertDissmissibilityEnum;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alerts', function (Blueprint $table) {
            $table->id();
            $table->string('type', 20);
            $table->string('appearance_type', 20)->default(AlertAppearanceTypeEnum::TOAST->name);
            $table->string('dismissibility', 20)->default(AlertDissmissibilityEnum::DISMISSIBLE->name);
            $table->string('reason_key');

            $table->integer('alertable_id')->nullable();
            $table->string('alertable_type')->nullable();

            // $table->foreignId('profile_id')->constrained('profiles')->onDelete('cascade');

            $table->timestamp('expired_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alerts');
    }
};
