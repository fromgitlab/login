<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alert_translations', function (Blueprint $table) {
            if (Schema::hasColumn('alert_translations', 'message'))
                $table->renameColumn('message', 'text');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alert_translations', function (Blueprint $table) {
            if (Schema::hasColumn('alert_translations', 'text'))
                $table->renameColumn('text', 'message');
        });
    }
};
