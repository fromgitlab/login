<?php

namespace Modules\Interaction\Providers;

use Illuminate\Support\ServiceProvider;

use Modules\Interaction\Interfaces\V1\AlertRepositoryInterface;
use Modules\Interaction\Repositories\V1\AlertRepository;

class RepositoryServiceProvider extends ServiceProvider
{

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
 
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        if(config('app.api_latest') == '1'){
            $this->app->bind(AlertRepositoryInterface::class, AlertRepository::class);
        }
      
    }

  
}
