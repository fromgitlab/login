<?php

namespace Modules\Interaction\Repositories\V1;

use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Interaction\Http\Resources\AlertResource;
use Modules\Interaction\Interfaces\V1\AlertRepositoryInterface;
use Modules\Interaction\Entities\Alert;


class AlertRepository implements AlertRepositoryInterface
{
      function myAlerts() :JsonResource
    {
        return AlertResource::collection(auth()->user()->profiles->where('id', authProfile()->id)->first()->alerts); 
    }

    function dismiss(Alert $alert) :void
    {
        $alert->delete();
    }
}
