<?php

namespace Modules\Interaction\Policies;

use Modules\Interaction\Entities\Alert;
use Illuminate\Auth\Access\HandlesAuthorization;

class AlertPolicy
{
 use HandlesAuthorization;

 public function browse(User $user)
 {
 return $user->hasPermission('Interaction.Alert.browse');
 }

 public function read(User $user, Alert $Alert)
 {
 return $user->hasPermission('Interaction.Alert.read');
 }

 public function add(User $user)
 {
 return $user->hasPermission('Interaction.Alert.add');
 }

 public function edit(User $user, Alert $Alert)
 {
 return $user->hasPermission('Interaction.Alert.edit');
 }

 public function delete(User $user, Alert $Alert)
 {
 return $user->hasPermission('Interaction.Alert.delete');
 }
}