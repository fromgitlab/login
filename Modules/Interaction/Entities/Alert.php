<?php

namespace Modules\Interaction\Entities;

use App\Models\BaseModel;
use Astrotomic\Translatable\Translatable;

class Alert  extends BaseModel
{
    use Translatable;

    public $translatedAttributes = [
        'text'
    ];

    protected $fillable = [
        'type',
        'appearance_type',
        'dismissibility',
        'reason_key',
        'profile_id',
        'expired_at',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
