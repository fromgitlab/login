<?php

use Illuminate\Support\Facades\Route;
use Modules\Interaction\Http\Controllers\API\V1\AlertController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//ANCHOR - Alerts

Route::group(['middleware' => 'auth:sanctum'], function () {
    //my Alerts (All User Alerts)
    Route::get('alerts', [AlertController::class, 'myAlerts']);
    //delete a dismissible alert (All Users Types)
    Route::delete('alerts/{alert}', [AlertController::class, 'dismiss']);
});
