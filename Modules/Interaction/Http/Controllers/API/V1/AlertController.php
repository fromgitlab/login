<?php

namespace Modules\Interaction\Http\Controllers\API\V1;

use App\Http\Controllers\BaseController;
use Modules\Interaction\Interfaces\V1\AlertRepositoryInterface;
use Modules\Interaction\Entities\Alert;


class AlertController extends BaseController
{

    public function __construct(private AlertRepositoryInterface $alertRepository)
    {
        parent::__construct();
    }

    /**
     * dismiss a dissmissable alert
     * @param Alert $alert
     */ 
    public function myAlerts()
    {
        return apiResponse(
            __("Alert.myAlerts"),
            transferData($this->alertRepository->myAlerts()),
            200
        );
    }

    /**
     * dismiss a dissmissable alert
     * @param Alert $alert
     */
    public function dismiss(Alert $alert)
    {
        if (!in_array($alert->profile_id, auth()->user()->profiles()->pluck('id')->toArray())) {
            return apiResponse(
                __("Alert.dismiss.case1"),
                null,
                401
            );
        } 
        else {
         return   $this->alertRepository->dismiss($alert);
            return apiResponse(
                __("Alert.dismiss.case2"),
                null,
                200
            );
        }
    }

}
