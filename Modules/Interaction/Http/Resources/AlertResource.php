<?php
namespace Modules\Interaction\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Interaction\Enums\AlertDissmissibilityEnum;

class AlertResource extends JsonResource
{
  /**
 * Transform the resource into an array.
 *
 * @return array<string, mixed>
 */
public function toArray($request)
{
     $is_dismissable_arr = [
        true,
        false,
        true,
    ];

     $should_notify_arr = [
        true,
        false,
        false,
    ];
     //NOTE - Should Be removed if isn't necessary in the future
    // $model = explode('\\', $this->alertable_type);
    //NOTE - Should Be removed if isn't necessary in the future
    // $model_name = $model[count($model) - 1];
    // $page_name = $model_name ? strtolower($model_name) . 's' : null;
    
    $is_dismissable = array_combine(array_column(AlertDissmissibilityEnum::cases(), 'name'), $is_dismissable_arr);
    $should_notify = array_combine(array_column(AlertDissmissibilityEnum::cases(), 'name'), $should_notify_arr);

    return [
        'id' => $this->id,
        'type' => $this->type,
        'appearance_type' => $this->appearance_type,
        'is_dismissable' => $is_dismissable[$this->dismissibility],
        'should_notify' => $should_notify[$this->dismissibility],
        'text' => $this->translate()?->text,
        // 'model_id' => $this->alertable_id,
        //  'model_page' => $page_name ,
        'created_at' => $this->created_at
    ];
}

}
