<?php

namespace Database\Seeders;

use App\Enums\LogEnum;
use Illuminate\Database\Seeder;
use App\Models\LogEvent;

class LogDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
  
        $log_events = [
            [
                'template' => "ADD"
            ],
            [
                'template' => "EDIT"
            ],
            [
                'template' => "DELETE"
            ],
            [
                'template' =>"GET"
            ],
        ];

        foreach ($log_events as $log_event) {
                $curr_log_event = LogEvent::where('template', $log_event['template'])->first();
                $log_event = $curr_log_event ? $curr_log_event : LogEvent::create($log_event);
        }
    }
}
