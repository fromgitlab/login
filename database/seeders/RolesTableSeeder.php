<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Auth\Entities\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     */
    public function run()
    {

        $roles = [
            [
                'ar' => [
                    'text' => 'مدير',
                ], 'en' => [
                    'text' => 'Administrator',
                ],
                'name' => 'admin',
            ],
            [
                'ar' => [
                    'text' => 'مستخدم',
                ], 'en' => [
                    'text' => 'Normal User',
                ],
                'name' => 'user',
            ],
            [
                'ar' => [
                    'text' => 'طبيب',
                ], 'en' => [
                    'text' => 'Doctor',
                ],
                'name' => 'doctor',
            ],
            [
                'ar' => [
                    'text' => 'مشغل العلامة التجارية',
                ],
                'en' => [
                    'text' => 'Brand Operator',
                ],
                'name' => 'brand_operator',
            ],
        ];

        foreach ($roles as $role) {
            $curr_role = Role::where('name', $role['name'])->first();
            $role = $curr_role ? $curr_role : Role::create($role);
        }
    }
}
