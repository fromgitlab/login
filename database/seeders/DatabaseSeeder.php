<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Announcement\Database\Seeders\AnnouncementDatabaseSeeder;
use Modules\Auth\Database\Seeders\AuthDatabaseSeeder;
use Modules\Auth\Helpers\AuthHelper;
use Modules\Auth\Database\Seeders\LocalAuthDatabaseSeeder;
use Modules\Catalog\Database\Seeders\BrandTableSeeder;
use Modules\Catalog\Database\Seeders\CatalogCategorySeeder;
use Modules\Catalog\Database\Seeders\CatalogDatabaseSeeder;
use Modules\Catalog\Database\Seeders\FeatureTableSeeder;
use Modules\Catalog\Database\Seeders\IndicationTableSeeder;
use Modules\Catalog\Database\Seeders\IngredientTableSeeder;
use Modules\Catalog\Database\Seeders\ItemTableSeeder;
use Modules\Catalog\Database\Seeders\LicenserTableSeeder;
use Modules\Catalog\Database\Seeders\OptionsTableSeeder;
use Modules\Catalog\Database\Seeders\UnitTableSeeder;
use Modules\Category\Database\Seeders\AvatarTableSeeder;
use Modules\Category\Database\Seeders\CategoryDatabaseSeeder;
use Modules\Category\Database\Seeders\ImportCitiesSeeder;
use Modules\Category\Database\Seeders\TypeTableSeeder;
use Modules\Loyalty\Database\Seeders\SystemWalletTableSeeder;
use Modules\Operation\Database\Seeders\OperationDatabaseSeeder;
use Modules\Profile\Database\Seeders\InsuranceTableSeeder;
use Modules\Profile\Database\Seeders\ProfileDatabaseSeeder;
use Modules\Profile\Database\Seeders\ServicesTableSeeder;
use Modules\Profile\Database\Seeders\SpecializationTableSeeder;
use Modules\Profile\Database\Seeders\SubSpecializationTableSeeder;
use Modules\Profile\Database\Seeders\WorkplaceTableSeeder;
use Modules\Profile\Database\Seeders\WorkplaceTypeTableSeeder;
use Modules\Setting\Database\Seeders\SettingDatabaseSeeder;
use Illuminate\Support\Facades\Auth;
use Modules\Auth\Entities\User;
use Illuminate\Support\Str;
use Modules\Loyalty\Database\Seeders\StageDesignTableSeeder;
use Modules\Setting\Database\Seeders\SettingTableSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if (config('app.env')  == 'local' || config('app.env')  == 'development') {
            $this->call([TruncateAllSeeder::class,]);
        }

        request()->headers->set('Current-Page', 'seeder');
        request()->headers->set('ulid', Str::ulid());

        // $user = AuthHelper::getUserOrCreate('0000000000', 'seeder@miroworld.com', 'seeder user');

        // $user = User::find($user->id);
        // Auth::setUser($user);

        if (config('app.env')  != 'local') {
            $this->call(
                [
                    LogDatabaseSeeder::class,
                    RolesTableSeeder::class,

                    // CategoryDatabaseSeeder::class,

                    // ProfileDatabaseSeeder::class,
                    // SettingDatabaseSeeder::class,
                    // CatalogDatabaseSeeder::class,
                    // OperationDatabaseSeeder::class,
                    AuthDatabaseSeeder::class,
                    // StageDesignTableSeeder::class,
                    // SystemWalletTableSeeder::class,
                    // AnnouncementDatabaseSeeder::class
                ]
            );
        } else {

            $this->call(
                [
                    LogDatabaseSeeder::class,
                    RolesTableSeeder::class,

                    // ImportCitiesSeeder::class,
                    // TypeTableSeeder::class,
                    // AvatarTableSeeder::class,

                    // ServicesTableSeeder::class,
                    // WorkplaceTypeTableSeeder::class,
                    // SpecializationTableSeeder::class,
                    // SubSpecializationTableSeeder::class,
                    // WorkplaceTableSeeder::class,
                    // InsuranceTableSeeder::class,

                    // SettingTableSeeder::class,

                    // FeatureTableSeeder::class,
                    // LicenserTableSeeder::class,
                    // IngredientTableSeeder::class,
                    // IndicationTableSeeder::class,
                    // CatalogCategorySeeder::class,
                    // UnitTableSeeder::class,
                    // OptionsTableSeeder::class,
                    // ItemTableSeeder::class,
                    // BrandTableSeeder::class,

                    LocalAuthDatabaseSeeder::class,
                    // StageDesignTableSeeder::class,
                    // SystemWalletTableSeeder::class,

                    // AnnouncementDatabaseSeeder::class
                ]
            );
        }
        // FIXME - Auth::revoke($user); 

    }
}
