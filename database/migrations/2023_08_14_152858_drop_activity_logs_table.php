<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('activity_log');
    }

    /**8
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Artisan::call("migrate --path=/database/migrations/2023_05_11_111301_create_activity_log_table");
    }
};
